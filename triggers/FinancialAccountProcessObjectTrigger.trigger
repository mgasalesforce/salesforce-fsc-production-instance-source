trigger FinancialAccountProcessObjectTrigger on FinServ__FinancialAccount__c (after insert, before insert, before update, after update, before delete) {
    FSTR.COTriggerHandler.handleProcessObjectTrigger();
}