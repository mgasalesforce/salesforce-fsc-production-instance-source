/**
 * @File Name          : Client_Service_Unit_Trigger.apxt
 * @Description        : Trigger for Client Service Unit object to create relavant CSU public group/queue
 * @Author             : soobin.kittredge@merceradvisors.com
 * @Group              : Mercer Advisors
 * @Last Modified By   : soobin.kittredge@merceradvisors.com
 * @Last Modified On   : 8/2/2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0  	  8/2/2019	  soobin.kittredge@merceradvisors.com     Initial draft
**/

trigger Client_Service_Unit_Trigger on Client_Service_Unit__c (after insert, after update, after delete, after undelete) {
    Client_Service_Unit_Trigger_Handler csuHandler = new Client_Service_Unit_Trigger_Handler();

	if (Trigger.isAfter){
		if(Trigger.isInsert){
			csuHandler.afterInsert(Trigger.New);
		} else if(Trigger.isUpdate){
			csuHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
		} else if(Trigger.isDelete){
			csuHandler.afterDelete(Trigger.oldMap);
		} else if(Trigger.isUndelete){
			csuHandler.afterUndelete(Trigger.newMap);
		}
	}

}