trigger FeedItemTriggerHandler on FeedItem (before insert) {
    if (trigger.isBefore){
		if( trigger.isInsert || trigger.isUpdate) 
		{
            system.debug('In FeedItemTriggerHandler');
			chatterFileRestrictions.preventFileTypes(trigger.new);
		}
	}    
}