trigger BoxFRUPTriggerMercer on box__FRUP__c (before insert, after insert, before update, after update) {
    
    System.debug('FRUPTriggerMercer on box__FRUP__c ' + Trigger.isInsert);
    System.debug(LoggingLevel.Info, 'FRUPTriggerMercer on box__FRUP__c ' + Trigger.isInsert);
    If (Trigger.isInsert && Trigger.isBefore) {
            
        System.debug('FRUPTriggerMercer on box__FRUP__c');
        System.debug(LoggingLevel.Info, 'FRUPTriggerMercer on box__FRUP__c');
        BoxFRUPTriggerHandler.updateInsertFileIDField(trigger.new);
        
    }

}