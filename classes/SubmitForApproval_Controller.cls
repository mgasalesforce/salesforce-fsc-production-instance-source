public class SubmitForApproval_Controller {
    @auraEnabled
    public static void submitForApproval(Id recId, String comment){
        Approval.ProcessSubmitRequest appReq = new Approval.ProcessSubmitRequest();
        appReq.setComments(comment);
        appReq.setObjectId(recId);
        Approval.ProcessResult appResult;
        System.debug(recId+' '+comment);
        try{
            appResult = Approval.process(appReq);
        } catch(Exception e){
            System.debug('No approval process has been setup yet.');
        }
    }
}