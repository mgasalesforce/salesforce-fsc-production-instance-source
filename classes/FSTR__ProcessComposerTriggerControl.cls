/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessComposerTriggerControl {
    global static Boolean EnableEventTrigger;
    global static Boolean EnableInitiators;
    global static Boolean EnableProcessObjectTriggers;
    global static Boolean EnableProcessTaskTrigger;
    global static Boolean EnableTaskTrigger;
    global ProcessComposerTriggerControl() {

    }
}
