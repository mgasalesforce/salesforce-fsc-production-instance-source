public class LightningQuery {
	@AuraEnabled
    public static List<sObject> executeSoql(String soql) {
    List<sObject> result = Database.query(soql);
    return result;
  }
}