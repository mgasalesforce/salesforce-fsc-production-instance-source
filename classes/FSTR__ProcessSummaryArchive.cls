/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessSummaryArchive implements Database.Batchable<SObject>, Database.Stateful {
    global ProcessSummaryArchive(String archiveType, Boolean isGroup, String accountLookup) {

    }
    global ProcessSummaryArchive(String archiveType, String accountLookup, Set<String> objectIds, Boolean excludeCompleted) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext ctx) {

    }
    global void queryForSingleID(String id) {

    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return null;
    }
}
