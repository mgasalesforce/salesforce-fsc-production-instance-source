public class ClientUpdateCallout_Inv {
    @InvocableMethod(label='Update Client via Callouts' description='Make callouts to update client data in portfolio database')
    public static void makeClientUpdateCallouts(List<Account> accounts){
        System.debug('Update Client via Callouts-INV START ------------');
        //Orion client - person account?
        Set<Id> memberAcctIds = (new Map<Id, Account>(accounts)).keyset();
        Set<String> memberAcctIds15 = new Set<String>();
        for(Id acctid : memberAcctIds){
            memberAcctIds15.add(String.valueOf(acctid).left(15));
        }
        List<AccountContactRelation> memberAcr = [SELECT Id, AccountId, Orion_Household_ID__c, PersonAccountid__c
                                                  FROM AccountContactRelation
                                                  WHERE Orion_Household_ID__c <> null AND PersonAccountid__c IN : memberAcctIds15];
        System.debug('memberAcr size for Orion update:' + memberAcr.size());
        Map<String, String> memberHouseholdIdMap = new Map<String, String>();
        Map<String, Integer> memberOrionIdMap = new Map<String, Integer>();
        for(AccountContactRelation acr : memberAcr){
            memberHouseholdIdMap.put(acr.PersonAccountid__c, acr.AccountId);
            memberOrionIdMap.put(acr.PersonAccountid__c, Integer.valueOf(acr.Orion_Household_ID__c));
        }
        if(memberOrionIdMap.size() > 0){
            for(String accId: memberOrionIdMap.keyset()){
                orionHouseholdUpdateCallout(memberHouseholdIdMap.get(accId), memberOrionIdMap.get(accId));
            }
        }
        
        //Env client - person account??
        Map<String, String> envPersonAcc = new Map<String, String>();
        for(Account member: accounts){
            //has Env member code
            if(member.Envestnet_Member_Code__c <> null) {
                envPersonAcc.put(member.id, member.Envestnet_Member_Code__c);
            }
        }
        if(envPersonAcc.size() > 0){
            for(String accId: envPersonAcc.keyset()){
                envMemberUpdateCallout(accId, envPersonAcc.get(accId));
            }
        }
        
        //Household?
        Map<String, String> envHouseholdAcc = new Map<String, String>();
        Map<String, String> OrionHouseholdAcc = new Map<String, String>();
        for(Account household: accounts){
            if(household.Envestnet_Customer_Code__c <> null && household.Envestnet_Advisor_Code__c <> ''){
                envHouseholdAcc.put(household.Id, household.Envestnet_Customer_Code__c);
            }
            if(household.Orion_Household_ID__c <> null && household.Orion_Rep_ID__c <> '' ){
                OrionHouseholdAcc.put(household.id, household.Orion_Household_ID__c);
            }
        }
        if(envHouseholdAcc.size()>0){
            for(String accId: envHouseholdAcc.keyset()){
                envHouseholdUpdateCallout(accId, envHouseholdAcc.get(accId));
            }
        }
        if(OrionHouseholdAcc.size()>0){
            for(String accId: OrionHouseholdAcc.keyset()){
                orionHouseholdUpdateCallout(accId, Integer.valueOf(OrionHouseholdAcc.get(accId)));
            }
        }
    }
    
    @Future(callout=true)
    public static void envHouseholdUpdateCallout(Id householdId, String customerCode){
        System.debug('** Update ENV household Disabled------ ');
        //EnvApiCollection.EnvUpdateClient(householdId,customerCode);
    }
    
    @Future(callout=true)
    public static void envMemberUpdateCallout(Id personId, String memberCode){
        System.debug('** Update ENV member Disabled------');
        //EnvApiCollection.EnvUpdateMember(personId,memberCode);
    }
    
    @Future(callout=true)
    public static void orionHouseholdUpdateCallout(Id householdId, Integer OrionHouseholdId){
        System.debug('** Update orion HH ------');
        OrionApiCollection oac = new OrionApiCollection();
        oac.updateClient(householdId, OrionHouseholdId);
    }
}