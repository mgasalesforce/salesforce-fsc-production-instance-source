/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
@RestResource(urlMapping='/ProcessSummary/*')
global class ProcessSummaryService {
    global ProcessSummaryService() {

    }
    @HttpGet
    global static String getProcessSummary() {
        return null;
    }
global class Data {
}
global class Progress {
}
global class ReturnWrapper {
}
global class SummaryTask implements System.Comparable {
}
global class SummaryTaskWrapper {
}
}
