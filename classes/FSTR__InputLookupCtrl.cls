/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class InputLookupCtrl {
    @RemoteAction
    global static FSTR.InputLookupCtrl.AuraObject getSObject(String sObjectType, String objId) {
        return null;
    }
    @RemoteAction
    global static List<FSTR.InputLookupCtrl.AuraObject> getSObjectList(String sObjectType, String searchTerm) {
        return null;
    }
global class AuraObject {
}
}
