// @description Invocable Apex class from Flow (/flow/Search_All_Person_Accounts_Leads_Referrals_Flow) to return All Person Accounts meeting search criteria bypassign sharing model 
// @author Praveen Kanumuri
// @date 7-25-2019

global without sharing class searchAllPersonAccounts {
   @InvocableMethod(label='searchAllPersonAccounts' description='searchAllPersonAccounts')
    public static List<List<Account>> getPersonAccounts(List<String> lstSearchJsonString) {
    string strName = '';
    string strCity = '';
    string strPhone = '';
    string strState = '';
    string strStatus = 'All';

    system.debug('jsonstring:'+lstSearchJsonString[0]);
    system.JSONParser jp = JSON.createParser(lstSearchJsonString[0]);
        while(jp.nextToken()!=null)
        {
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='name')
            {
              jp.nextToken();
                strName = jp.getText();    
            }   
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='city')
            {
              jp.nextToken();                 
                strCity = jp.getText();                
            }   
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='phone')
            {
              jp.nextToken();                 
                strPhone = jp.getText();                
            }   
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='status')
            {
              jp.nextToken();                 
                strStatus = jp.getText();                
            }
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='state')
            {
              jp.nextToken();                 
                strState = jp.getText();                
            } 
        }    
    
      //system.debug('strName:'+strName);
      //system.debug('strCity:'+strCity);
      //system.debug('strStatus:'+strStatus);

       string  strQueryPersonAccounts = 'SELECT Id, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'name, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'BillingCity, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'BillingState, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'PersonMailingCity, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'PersonMailingState, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'Phone, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'OwnerId, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'PersonContactId, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'FinServ__Status__c, ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'CSU_Branch_Name__c ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'FROM Account ';
       strQueryPersonAccounts = strQueryPersonAccounts + 'WHERE recordtype.name = \'person account\' and Name like \'%' + strName + '%\'';
       if(strState != null && strState.trim() <> '')
       {
           strQueryPersonAccounts += ' AND BillingState LIKE \'%' + strState + '%\'';
       }
       if(strPhone!=null && strPhone.trim() <> '')
       {
           strQueryPersonAccounts = strQueryPersonAccounts + ' and Phone like \'%' + strPhone + '%\'';
       }
    
      if(strStatus!='All' && strStatus.trim() <> '' && strStatus!=null)
         {
             strQueryPersonAccounts = strQueryPersonAccounts + ' and FinServ__Status__c = \'' + strStatus + '\'';
         }       

      //system.debug('strQueryPersonAccounts:'+strQueryPersonAccounts);

      List<Account> Accounts = database.query(strQueryPersonAccounts);
      List<List<Account>> lstAccounts = new List<List<Account>>();
      lstAccounts.add(Accounts);
      return lstAccounts;

    }
}