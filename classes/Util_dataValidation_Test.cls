@isTest
public class Util_dataValidation_Test {
    static testMethod void testUtilMethods(){
        List<String> stateFullList = new List<String>();
        stateFullList.add('Alabama');
        stateFullList.add('Alaska');
        stateFullList.add('Arizona');
        stateFullList.add('Arkansas');
        stateFullList.add('California');
        stateFullList.add('Colorado');
        stateFullList.add('Connecticut');
        stateFullList.add('Delaware');
        stateFullList.add('Florida');
        stateFullList.add('Georgia');
        stateFullList.add('Hawaii');
        stateFullList.add('Idaho');
        stateFullList.add('Illinois');
        stateFullList.add('Indiana');
        stateFullList.add('Iowa');
        stateFullList.add('Kansas');
        stateFullList.add('Kentucky');
        stateFullList.add('Louisiana');
        stateFullList.add('Maine');
        stateFullList.add('Maryland');
        stateFullList.add('Massachusetts');
        stateFullList.add('Michigan');
        stateFullList.add('Minnesota');
        stateFullList.add('Mississippi');
        stateFullList.add('Missouri');
        stateFullList.add('Montana');
        stateFullList.add('Nebraska');
        stateFullList.add('Nevada');
        stateFullList.add('New Hampshire');
        stateFullList.add('New Jersey');
        stateFullList.add('New Mexico');
        stateFullList.add('New York');
        stateFullList.add('North Carolina');
        stateFullList.add('North Dakota');
        stateFullList.add('Ohio');
        stateFullList.add('Oklahoma');
        stateFullList.add('Oregon');
        stateFullList.add('Pennsylvania');
        stateFullList.add('Rhode Island');
        stateFullList.add('South Carolina');
        stateFullList.add('South Dakota');
        stateFullList.add('Tennessee');
        stateFullList.add('Texas');
        stateFullList.add('Utah');
        stateFullList.add('Vermont');
        stateFullList.add('Virginia');
        stateFullList.add('Washington');
        stateFullList.add('West Virginia');
        stateFullList.add('Wisconsin');
        stateFullList.add('Wyoming');
        stateFullList.add('Ontario');
        stateFullList.add('Quebec');
        stateFullList.add('QuéBec');
        stateFullList.add('British Columbia');
        stateFullList.add('Alberta');
        stateFullList.add('Manitoba');
        stateFullList.add('Saskatchewan');
        stateFullList.add('Nova Scotia');
        stateFullList.add('New Brunswick');
        stateFullList.add('Newfoundland And Labrador');
        stateFullList.add('Prince Edward Island');
        stateFullList.add('Northwest Territories');
        stateFullList.add('Yukon');
        stateFullList.add('Nunavut');
        stateFullList.add('random');
        stateFullList.add('NY');
        
        List<String> genderlist = new List<String>();
        genderlist.add('Male');
        genderlist.add('Female');
        genderlist.add('Rabbit');
        genderlist.add('');
        
        for(String statename: stateFullList){
            String stateAbb = Util_dataValidation.State2Abbr(statename);
            if(statename == 'Colorado'){
                System.assertEquals('CO', stateAbb);
            }
        }
        for(String gender: genderlist){
            String genderChar = Util_dataValidation.Gender2Char(gender);
            if(gender == 'Male'){
                System.assertEquals('M', genderChar);
            }
        }
    }
}