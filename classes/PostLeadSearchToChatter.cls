// @description Invocable Apex class from Flow (/flow/Search_All_Person_Accounts_Leads_Referrals_Flow) 
// to post to chatter of person account, household account and @mention personaccount owner bypassign sharing model 
// @author Praveen Kanumuri
// @date 7-25-2019
global without sharing class PostLeadSearchToChatter {
   @InvocableMethod(label='PostLeadSearchToChatter' description='PostLeadSearchToChatter')
   public static List<string> PostPersonAccountSearchToChatter(List<id> lstLeadId) {
    id strId = lstLeadId[0];    
       system.debug('strId:'+strId);
       List<FeedItem> feedItemList = new List<FeedItem>();
        List<id> personContactId = new list<id>();
    
         if(!lstLeadId.isEmpty()){

            //Post to Lead 
            FeedItem postPersonAccount = new FeedItem();
            postPersonAccount.createdById = UserInfo.getUserId();
            postPersonAccount.ParentId = strId;
            postPersonAccount.Type = 'TextPost';        
            postPersonAccount.Body = UserInfo.getName() + ' searched for and opened the Lead record.';
            feedItemList.add(postPersonAccount);

             string strQuery = 'Select ID,Lead_Search_Date__c, ownerId FROM Lead where ID = \'' + strId + '\'';
            //This update first process builder that posts @mention to Account Owner 
            List<Lead> lstLead = database.query(strQuery);
            List<Lead> lstUpdateLead = new List<Lead>();
            system.debug('lstLead Count:'+ lstLead.size()); 
            for(Lead l : lstLead){
                system.debug('lead owner id:'+l.OwnerId);
                if(string.valueOf(l.OwnerId).startsWith('005'))
                {
                    l.Lead_Search_Date__c = system.now();
                    lstUpdateLead.add(l);
                }    
            }
            if(!lstUpdateLead.isEmpty()){
                update lstUpdateLead;
            }

 
            
          //Post to Chatter - insert Feed Item        
          if(feedItemList.size()>0)
          {
            insert feedItemList;
          }      

    }        
       
    return lstLeadId;
  }
}