/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class XMLSerializer {
    global XMLSerializer() {

    }
    global static SObject DeSerialize(String XmlToObject) {
        return null;
    }
    global static List<SObject> DeSerializeList(String XmlToObject, List<SObject> starterList) {
        return null;
    }
    global static String Serialize(SObject objectToXml) {
        return null;
    }
    global static String SerializeList(List<SObject> objectToXml) {
        return null;
    }
}
