/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Test Methods for Non Primary Person Owner Update Invocable class
 * 
 * Provides test coverage for Non Primary Person Owner Update Invocable Class methods.
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author: John Crapuchettes - john@zennify.com
 * @created: 2019-09-17
 * @modifiedBy: John Crapuchettes - john@zennify.com
 * @modifiedDate: 2019-09-17
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
@isTest
public class NonPrimaryPersonOwnerUpdateInvTest {
	@isTest
    public static void testOwnerUpdate()
    {
        // Get Starting Points
        RecordType rtHousehold = [SELECT ID FROM RecordType WHERE DeveloperName = 'IndustriesHousehold' LIMIT 1];
        RecordType rtPerson = [SELECT ID FROM RecordType WHERE DeveloperName = 'PersonAccount' LIMIT 1];
        User uTemp = [SELECT ID, Name, ProfileId FROM User WHERE Profile.Name = 'System Administrator' AND isActive = TRUE LIMIT 1];
        //User uAdmin2 = [SELECT ID, Name FROM User WHERE Profile.Name = 'System Administrator' AND isActive = TRUE AND ID != :uAdmin.Id LIMIT 1];
        
        User uAdmin = new User(Alias = 'tuser1', Email='testuser1@notanemail.com', 
            EmailEncodingKey='UTF-8', LastName='User 1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = uTemp.ProfileId, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testuser1@notanemail.com');
        insert uAdmin;
        User uAdmin2 = new User(Alias = 'tuser2', Email='testuser2@notanemail.com', 
            EmailEncodingKey='UTF-8', LastName='User 2', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = uTemp.ProfileId, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testuser2@notanemail.com');
        insert uAdmin2;
        
        system.debug('User 1: ' + uAdmin);
        system.debug('User 2: ' + uAdmin2);
        
        Branch__c branch = new Branch__c(Name = 'Test', Managing_Director__c = uAdmin.id);
        insert branch;
        
        Client_Service_Unit__c csu = new Client_Service_Unit__c();
        csu.Name = 'Test';
        csu.Branch__c = branch.id;
        csu.Client_Advisor__c = uAdmin.id;
        insert csu;
        
        Client_Service_Unit__c csu2 = new Client_Service_Unit__c();
        csu2.Name = 'Test 2';
        csu2.Branch__c = branch.id;
        csu2.Client_Advisor__c = uAdmin2.id;
        insert csu2;
        
        // Create Household and Person Account
        
        // Create Account
        Account aHousehold = new Account();
        aHousehold.Name = 'Test Account';
        aHousehold.RecordTypeId = rtHousehold.id;
        aHousehold.Client_Service_Unit__c = csu.id;
        aHousehold.FinServ__Status__c = 'Active';
        insert aHousehold;
        
        // Create Two Person Accounts
        Account aPerson1 = new Account();
        aPerson1.FirstName = 'Test';
        aPerson1.LastName = 'Person1';
        aPerson1.RecordTypeId = rtPerson.id;
        aPerson1.BillingStreet = '123';
        insert aPerson1;
        
        Account aPerson2 = new Account();
        aPerson2.FirstName = 'Test';
        aPerson2.LastName = 'Person2';
        aPerson2.RecordTypeId = rtPerson.id;
        aPerson2.BillingStreet = '123';
        insert aPerson2;
        
        Account aPContact1 = [SELECT ID, PersonContactId FROM Account WHERE ID =: aPerson1.id LIMIT 1];
        
        // Create ACRs
        AccountContactRelation acr1 = new AccountContactRelation();
        acr1.AccountId = aHousehold.id;
        acr1.ContactId = aPContact1.PersonContactId;
        acr1.FinServ__PrimaryGroup__c = true;
        acr1.FinServ__Primary__c = true;
        insert acr1;
        
        Account aPContact2 = [SELECT ID, PersonContactId FROM Account WHERE ID =: aPerson2.id LIMIT 1];
        
        AccountContactRelation acr2 = new AccountContactRelation();
        acr2.AccountId = aHousehold.id;
        acr2.ContactId = aPContact2.PersonContactId;
        acr2.FinServ__PrimaryGroup__c = true;
        acr2.FinServ__Primary__c = false;
        insert acr2;
        
        // Actual testing of switching of IDs
        test.startTest();
        
        // Change the CSU on the Household;
        aHousehold.Client_Service_Unit__c = csu2.id;
        update aHousehold;
        List<Id> HouseIDList = new List<Id>();
        HouseIDList.add(aHousehold.id);
        NonPrimaryPersonOwnerUpdateInvocable.updatePersonOwnerByHousehold(HouseIDList);
        
        test.stopTest();
        
        // This is the Primary Member - just checking it is working.
        Account aGetPA1 = [SELECT ID, OwnerID FROM Account WHERE ID = :aPerson1.id];
        
        // This is the Non-Primary Member - the test.
        Account aGetPA2 = [SELECT ID, OwnerID FROM Account WHERE ID = :aPerson2.id];
        
        // Do we get expected IDs?
        system.assertEquals(uAdmin2.id, aGetPA1.OwnerId);
        system.assertEquals(uAdmin2.id, aGetPA2.OwnerId);
    }
}