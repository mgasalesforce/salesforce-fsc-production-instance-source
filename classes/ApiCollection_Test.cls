@isTest

public class ApiCollection_Test {
    @testSetup static void setup(){
        Profile csuProfile = [SELECT Id FROM Profile WHERE Name='MA CSU']; 
        User advUser = new User(Alias = 'adv', Email='adv@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName='Testx', LastName='Advisor', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = csuProfile.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='adv@testorg.com');
        insert advUser;
        Branch__c branch = new Branch__c(
            Name = 'Denver',
            Phone_Number__c = '303-555-7845',
            Fax_Number__c = '303-555-7809',
            Street_Address__c = '1700 Grant Street',
            City__c = 'Denver',
            State__c = 'CO',
            Zip_Code__c = '80238',
            Country__c = 'United States',
            Status__c = 'Active'
        );
        insert branch;
        
        Client_Service_Unit__c adv = new Client_Service_Unit__c(
            Name = 'Advisor 64e664e64',
            Client_Advisor__c = advUser.id,
            Branch__c = branch.id,
            City__c = 'Denver',
            Status__c = 'Active',
            Type__c = 'Branch',
            Envestnet_Advisor_Code__c = 'en002239294',
            Orion_Rep_ID__c = '1',
            Envestnet_Advisor_Code_Acq_Legacy__c = 'en002239509'
        );
        insert adv;
        Account householdAcc = new Account(
            Name = 'John Cena Household',
            RecordTypeId = '0121U0000012n6fQAA',
            FinServ__IndividualType__c = 'Group',
            FinServ__Status__c = 'Active',
            Client_Service_Unit__c = adv.id,
            Orion_Household_ID__c = '25897',
            Envestnet_Customer_Code__c = ''
        );
        insert householdAcc;
        RecordType personAccountRecordType =  [SELECT Id FROM RecordType WHERE Name = 'Person Account' and SObjectType = 'Account'];
        Account personAcc = new Account(
            LastName = 'Cena2',
            FirstName = 'John',
            Salutation = 'Mr.',
            RecordType = personAccountRecordType,
            BillingStreet = '123 nowhere 34',
            BillingCity = 'here',
            BillingState = 'there',
            BillingPostalCode = '10000',
            BillingCountry = 'USA',
            Customer_Type__c = 'Customer',
            Envestnet_Member_Code__c = 'en003xxxxx',
            personbirthdate=Date.today(),
            FinServ__Gender__pc = 'Male',
            FinServ__MaritalStatus__pc = 'Married',
            FinServ__Occupation__pc = 'Testing'
        );
        insert personAcc;
        Account personAcc2 = new Account(
            LastName = 'Cena2',
            FirstName = 'James',
            Salutation = 'Mr.',
            RecordType = personAccountRecordType,
            BillingStreet = '123 nowhere 34',
            BillingCity = 'here',
            BillingState = 'there',
            BillingPostalCode = '10000',
            BillingCountry = 'USA',
            Customer_Type__c = 'Customer',
            Envestnet_Member_Code__c = 'en003xxxxxxxxx',
            personbirthdate=Date.today(),
            FinServ__Gender__pc = 'Male',
            FinServ__MaritalStatus__pc = 'Married',
            FinServ__Occupation__pc = 'Testing'
        );
        insert personAcc2;
        personAcc = [SELECT Id, PersonContactId from account where id =: personAcc.id];
        personAcc2 = [SELECT Id, PersonContactId from account where id =: personAcc2.id];
        AccountContactRelation acr = new AccountContactRelation (AccountId = householdAcc.id, ContactId=personAcc.PersonContactId, FinServ__PrimaryGroup__c = true, FinServ__Primary__c = true, FinServ__Rollups__c = 'All');
        insert acr;
        AccountContactRelation acr2 = new AccountContactRelation (AccountId = householdAcc.id, ContactId=personAcc2.PersonContactId, FinServ__PrimaryGroup__c = true, FinServ__Primary__c = false, FinServ__Rollups__c = 'All');
        insert acr2;
        OrionAuthCredential__c orionAuthCred = new OrionAuthCredential__c(usnm__c ='xx', pw__c ='xx', clientId__c ='xx', clientSecret__c ='xx', expiration__c = Datetime.now());
        insert orionAuthCred;
        EnvAuthCredential__c EnvAuthCred = new EnvAuthCredential__c(usnm__c='xx', pw__c='xx',endpoint__c='api/auth/login', expiration__c=Datetime.now(), token__c='eyJhPH8qpyJo-e7lQm41ilpKssC1v7zXUodriqHeXJDBS4G5ygeWSrVWiE6u51AFgHSeTo9zaqBjWChau7yZvrORw', channel__c='xx');
        insert EnvAuthCred;
        
        FinServ__FinancialAccount__c finacc = new FinServ__FinancialAccount__c(
            Name = 'test fin acc',
            RecordTypeId='0121U0000012n6xQAA',
            FinServ__PrimaryOwner__c = personAcc.id,
            Capital_Gains_Reinvestment__c = true,
            Dividend_Reinvestment__c = false,
			Orion_Financial_Account_ID__c = '1000000');
        FinServ__FinancialAccount__c finacc2 = new FinServ__FinancialAccount__c(
            Name = 'test fin acc2',
            RecordTypeId='0121U0000012n6xQAA',
            FinServ__PrimaryOwner__c = personAcc.id,
            Capital_Gains_Reinvestment__c = true,
            Dividend_Reinvestment__c = false,
			Orion_Financial_Account_ID__c = '');
        insert finacc;
        insert finacc2;

    }
    @isTest static void testFinaccUpdate(){
        Test.setMock(HttpCalloutMock.class, new Orion_MockHttPCallout_Auth());
        Test.startTest();
        OrionApiCollection.updateAccountVerbose(1000000);
        Test.stopTest();
    }
    @isTest static void testFinaccUpdateInv(){
        Test.setMock(HttpCalloutMock.class, new Orion_MockHttPCallout_Auth());
        Set<Id> finAccIdSet = (new Map<Id, sObject>([SELECT Id FROM FinServ__FinancialAccount__c])).keySet();
        List<Id> finAccIdList = new List<Id>(finAccIdSet);
        Test.startTest();
        OrionApiCollection.updateAccountVerbose_Inv(finAccIdList);
        Test.stopTest();
    }
    @isTest static void testAuthOrion(){
        Test.setMock(HttpCalloutMock.class, new Orion_MockHttPCallout_Auth());
        Test.startTest();
        OrionApiCollection oac = new OrionApiCollection();
        OrionApiCollection.orion_token = 'xx';
        oac.OrionApiCollection_Init();
        Test.stopTest();
    }
    @isTest static void testAuthEnv(){
        Test.setMock(HttpCalloutMock.class, new Env_MockHttpCallout_Auth());
        Test.startTest();
        EnvApiCollection eac = new EnvApiCollection();
        eac.EnvApiCollection_init();
        Test.stopTest();
    }
    @isTest static void testupdateclient(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Orion_MockHttpCallout_Auth());
        OrionApiCollection oac = new OrionApiCollection();
        Account hh = [SELECT Id, Orion_Household_ID__c from Account where Orion_Household_ID__c<>null][0];
        Test.setMock(HttpCalloutMock.class, new Orion_MockHttpCallout());
        oac.updateClient(hh.id, Integer.valueOf(hh.Orion_Household_ID__c));
        Test.stopTest();
    }
    @isTest static void testvalidateclient(){
        Test.startTest();
        Account hh = [SELECT Id from Account where ispersonaccount = false][0];
        EnvApiCollection.validateClientInfo(hh.id);
        Test.stopTest();
    }
    @isTest static void testcreateclient(){
        Test.setMock(HttpCalloutMock.class, new Env_MockHttpCallout());
        Test.startTest();
        Account hh = [SELECT Id, Envestnet_Member_Code__c, Envestnet_Customer_Code__c, Envestnet_Advisor_Code__c, Orion_Household_ID__c from Account where ispersonaccount = false][0];
        EnvApiCollection.EnvCreateClient(hh.id,hh.Envestnet_Advisor_Code__c );
        Test.stopTest();
    }
    @isTest static void testupdateclient_env(){
        Test.setMock(HttpCalloutMock.class, new Env_MockHttpCallout());
        Test.startTest();
        Account hh = [SELECT Id, Envestnet_Member_Code__c, Envestnet_Customer_Code__c, Envestnet_Advisor_Code__c, Orion_Household_ID__c from Account where ispersonaccount = false][0];
        hh.Envestnet_Customer_Code__c = 'en0010000000';
        EnvApiCollection.EnvUpdateClient(hh.id,'en0010000000' );
        Test.stopTest();
    }
    @isTest static void testmember_env(){
        Test.setMock(HttpCalloutMock.class, new Env_MockHttpCallout());
        Test.startTest();
        Account ps = [SELECT Id, Envestnet_Member_Code__c, Envestnet_Customer_Code__c, Envestnet_Advisor_Code__c, Orion_Household_ID__c from Account where ispersonaccount = true][0];
        EnvApiCollection.EnvUpdateMember(ps.id,'en0090000000' );
        EnvApiCollection.SalesforceValueToCode('Spouse','relation');
        Test.stopTest();
    }
    @isTest static void testdeletemember_env(){
        Test.setMock(HttpCalloutMock.class, new Env_MockHttpCallout());
        Test.startTest();
        EnvApiCollection.EnvRemoveMember('en0090000000');
        Test.stopTest();
    }
    @isTest static void testaddmember_env(){
        Test.setMock(HttpCalloutMock.class, new Env_MockHttpCallout());
        Test.startTest();
        Account personAccountRecord = [SELECT Id,Name,Salutation,FirstName,LastName,MiddleName,PersonBirthdate,Phone,PersonEmail,BillingStreet,BillingCity,BillingState,BillingPostalCode,BillingCountry,FinServ__Gender__pc,FinServ__MaritalStatus__pc,FinServ__Occupation__pc,FinServ__CurrentEmployer__pc,Fax,IsPersonAccount,Social_Security_Number__pc,Envestnet_Customer_Code__c,Envestnet_Member_Code__c,Envestnet_Advisor_Code__c 
                                       FROM Account 
                                       WHERE ispersonaccount = true][0];
        String x = EnvApiCollection.SfPersonToEnvMemberJSON(personAccountRecord.id);
        EnvApiCollection.EnvAddNewMember(x, '1~1~1111111');
        Test.stopTest();
    }
    @isTest static void testInvocMethod(){
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new Orion_MockHttpCallout_Auth());
        List<Account> acct = [SELECT Id, Orion_Rep_ID__c, Envestnet_Member_Code__c, Envestnet_Customer_Code__c, Envestnet_Advisor_Code__c, Orion_Household_ID__c from Account];
        ClientUpdateCallout_Inv.makeClientUpdateCallouts(acct);
        Test.stopTest();
    }
}