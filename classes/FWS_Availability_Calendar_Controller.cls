/**
 * @File Name          : FWS_Availability_Calendar_Controller.apxc
 * @Description        : Custom controller for visualforce page EPA_Availability. Uses O365 Oauth2.0 to grant access to MS Graph and pulls  event data from shared calendar 
 * @Author             : soobin.kittredge@merceradivosr.com
 * @Group              : Mercer Advisors
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 7/26/2019, 1:33:12 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0       09/06/2018       soobin.kittredge@merceradivosr.com     Initial Version
 * 1.1       09/07/2018       soobin.kittredge@merceradivosr.com     connected app update (App now in Azure)
 * 1.2       09/10/2018       soobin.kittredge@merceradivosr.com     Update in creating pending meeting - ending time is 2 hours from starting time (both xx:00 and xx:30)
 * 1.3       09/12/2018       soobin.kittredge@merceradivosr.com     Updated 'totalWeeklyMeeting' counter so it is counted when there is 'EP-IM Available' block
 * 1.4       10/08/2018       soobin.kittredge@merceradivosr.com     Updated email notification template to include timezone
 * 1.5       10/25/2018       soobin.kittredge@merceradivosr.com     Added calendarYear variable to save requested date's year
 *
 * 2.0       07/25/2019       soobin.kittredge@merceradivosr.com     Initial draft for V2.0 with improvements - ignore event when not available / extend functionality for Income Tax calendar / Pending event creation
 **/
public class FWS_Availability_Calendar_Controller {
    public String targetdate_selected 	{get; set;}
    public String resp					{get; set;}
    public String tokenResp				{get; set;}
    public String currenturl 			{get; set;}
    public String authcode 				{get; set;}
    public String authsessionstate		{get; set;}
    public String authtoken				{get; set;}
    public String authtokenType			{get; set;}
    public String graphResp				{get; set;}
    public String calSearchResp			{get; set;}
    
    public Boolean authorized 			{get; set;}
    public Boolean gotToken				{get; set;}
    public Boolean tokenUsed			{get; set;}
    public Boolean gotGraph				{get; set;}
    public Boolean hasSearchResult		{get; set;}
    public static String CLINET_ID		= '3fe03c85-98e9-43aa-b833-c546aeed1b20';
    public static String SCOPE			= 'Calendars.ReadWrite.shared';
    public static String PASSWORD 		= 'UF3=Xty_0[eYnRqgAy:O8ol2*eVD-MFk';
    
    
    public String timezone			{get; set;}
    public Date today_d				{get; set;}
    public Date monday				{get; set;}
    public Date tuesday				{get; set;}
    public Date wednesday			{get; set;}
    public Date thursday			{get; set;}
    public Date friday				{get; set;}
    public Date saturday			{get; set;} // to be used in search for event end date
    public String monday_str		{get; set;}
    public String tuesday_str		{get; set;}
    public String wednesday_str		{get; set;}
    public String thursday_str		{get; set;}
    public String friday_str		{get; set;}
    public String saturday_str		{get; set;}
    public String targetWeek		{get; set;}
    public String monday_urlstr		{get; set;}
    public String saturday_urlstr	{get; set;}
    public String calendarYear		{get; set;}
    
    
    public Integer mon_available	{get; set;}
    public Integer tue_available	{get; set;}
    public Integer wed_available	{get; set;}
    public Integer thu_available	{get; set;}
    public Integer fri_available	{get; set;}
    
    public List<object> timeblocks						 {get; set;}
    public List<String> timeblocks_liststr 				 {get; set;}
    public Boolean MeetingAvailableThisweek				 {get; set;}
    
    public set<String> meetingTimeSet_mon					 {get; set;} //key sets for vf iteration
    public set<String> meetingTimeSet_tue					 {get; set;}
    public set<String> meetingTimeSet_wed					 {get; set;}
    public set<String> meetingTimeSet_thu					 {get; set;}
    public set<String> meetingTimeSet_fri					 {get; set;}
    public Map<String, Integer> timeblock_list_mon			 {get; set;} //monday-{'time',(#Available*10^2+#tentative*10^0)}
    public Map<String, Integer> timeblock_list_tue			 {get; set;}
    public Map<String, Integer> timeblock_list_wed			 {get; set;}
    public Map<String, Integer> timeblock_list_thu			 {get; set;}
    public Map<String, Integer> timeblock_list_fri			 {get; set;}
    public Map<String, String> timeblock_availability_mon	 {get; set;} //monday-{'time',availability literal}
    public Map<String, String> timeblock_availability_tue	 {get; set;}
    public Map<String, String> timeblock_availability_wed	 {get; set;}
    public Map<String, String> timeblock_availability_thu	 {get; set;}
    public Map<String, String> timeblock_availability_fri	 {get; set;}
    
    public List<String> vf_datatable				 {get; set;} 				//for table styling
    public Boolean displayConfirmation				 {get; set;}
    public String selectedCalName                    {get; set;}
    public Boolean currentlyEpCalendar				 {get; set;}
    
    public String request_email_to		    {get; set;} //public group name for recieving request email
    public String calendar_email	        {get; set;} //O365 email that has the calendar

    public static String REQUEST_EMAIL_TO_EP 	= 'EP-IM Request'; //public group name for recieving request email : FWS EP
    public static String CALENDAR_EMAIL_EP		= 'epmeetings@merceradvisors.com'; //O365 email that has the calendar : FWS EP

    public static String REQUEST_EMAIL_TO_TAX 	= 'TAX-Meeting Request'; //public group name for recieving request email : FWS Tax
    public static String CALENDAR_EMAIL_TAX		= 'IncomeTax@merceradvisors.com'; //O365 email that has the calendar : FWS Tax

    
    public String finalMeetingReqInfo		{get; set;}
    public String finalMeetingReqInfo_date	{get; set;}
    public String finalMeetingReqInfo_time	{get; set;}
    
    public Boolean MeetingRequest_reqInfo	{get; set;}
    public String MeetingRequest_caseLink	{get; set;}
    public String MeetingRequest_oppLink	{get; set;}
    public String MeetingRequest_clientName	{get; set;}
    
    
    
    
    
    public FWS_Availability_Calendar_Controller(){
        
        if(authorized == NULL && gotToken == NULL && gotGraph == NULL){
            gotGraph = false;
            gotToken = false;
            authorized = false;
            tokenUsed = false;
            hasSearchResult = false;
            today_d = Date.today();
            calendarYear = String.valueOf(Date.today().year());
            monday = today_d.toStartOfWeek() + 1;
            setWeekDates(monday);
            getTargetWeekSet();
            vf_datatable = new List<String>{''};								//for table styling
                }
        
        timezone = String.valueOf(UserInfo.getTimeZone().getID());
        
        targetdate_selected = Date.today().format();
        currenturl = ApexPages.currentPage().getUrl();
        
        authorized = currenturl.contains('session_state');

        selectedCalName = 'FWS Meeting Availability Calendar';
        
        if(authorized&&!tokenUsed) {
            authcode = currenturl.substringBetween('code=', '&');
            authsessionstate = currenturl.substringAfter('&session_state=');
            tokenUsed = true;
            requestToken();
        }
        If(gotGraph){
            getIMAvailableItems();
        }
        if(calendar_email==NULL){
            selectEpIm();
            getIMAvailableItems();
        }
    }

    //-- Choose Calendar & get authorized
    public void selectEpIm(){
        request_email_to = REQUEST_EMAIL_TO_EP;
        calendar_email  = CALENDAR_EMAIL_EP;
        selectedCalName = 'Estate Planning - Initial Meeting Calendar';
        currentlyEpCalendar = true;
    }

    public void selectTax(){
        request_email_to = REQUEST_EMAIL_TO_TAX;
        calendar_email = CALENDAR_EMAIL_TAX;
        selectedCalName = 'Income Tax - Meeting Calendar';
        currentlyEpCalendar = false;
    }
    
    
    
    //-- Count Available Time blocks : START ------------------------------------------
    
    public Integer countAvailableTB(String targetDate){
        Integer countTB = 0;
        String substringTerm = 'start={dateTime='+targetDate;
        Integer substringTerm_length = substringTerm.length();
        String timeblocks_str = '';
        for(String s : timeblocks_liststr){
            timeblocks_str += s;
        }
        Integer index = timeblocks_str.indexOf(substringTerm);
        while(index >=0){
            countTB++;
            timeblocks_str = timeblocks_str.substring(index+substringTerm_length);
            index = timeblocks_str.indexOf(substringTerm);
        }
        return countTB;
    }
    
    public void countDailyAvailability(){
        mon_available = countAvailableTB(monday_Str);
        tue_available = countAvailableTB(tuesday_Str);
        wed_available = countAvailableTB(wednesday_Str);
        thu_available = countAvailableTB(thursday_Str);
        fri_available = countAvailableTB(friday_Str);
    }
    
    public void timeblocks2timeblock(){
        MeetingAvailableThisweek = false;
        
        String timeblock_end = '';
        String timeblock_start = '';
        String timeblock_subject = '';
        String timeblock_day = '';
        Integer totalWeeklyMeeting = 0;
        //testing purpose----------
        String timeblocks_str = '';
        for(String s : timeblocks_liststr){ //timeblocks_liststr = list string of events
            timeblocks_str += s;
        }
        //--------------------------
        //add day string to list<string>... might be a better way?
        List<String> day_str = new List<String>();
        day_str.add(monday_Str);
        day_str.add(tuesday_Str);
        day_str.add(wednesday_Str);
        day_str.add(thursday_Str);
        day_str.add(friday_Str);
        
        for(Integer dayofweek = 0; dayofweek<5; dayofweek++){ //iterate through mon-fri
            Map<String, Integer> meetingmap_day = new Map<String, Integer>();  // Map<string, ingeter> --- string: meeting time (ex:08:00:00) , integer -- 10^0 = tentative, 10^2 = available
            Map<String, String> meetingmap_literal = new Map<String, String>();
            Set<String> meetingtime_keyset = new Set<String>();
            
            for(Integer event = 0; event < timeblocks_liststr.size(); event++){ //iterate through events
                //this date = day_str[dayofweek]
                if(timeblocks_liststr[event].indexOf(day_str[dayofweek]) > 0){
                    //this event contains the this date's date format (ex: 2018-09-17)
                    String meetingTime = timeblocks_liststr[event].substringBetween('start={dateTime='+day_str[dayofweek]+'T', ':00.0000000,');
                    if(timeblocks_liststr[event].indexOf(' Available')>0 || timeblocks_liststr[event].indexOf(' available')>0){
                        //if this event is 'xxxx available'
                    	totalWeeklyMeeting++;
                        //add meetingTime to Map key and keyset
                        if(meetingtime_keyset.contains(meetingTime)){ 	//already added - modify value
                            Integer meetingvalue = meetingmap_day.remove(meetingTime);
                            meetingvalue = meetingvalue + 100;
                            meetingmap_day.put(meetingTime, meetingvalue);
                        } else {										//new meeting time - new key-value map
                            meetingtime_keyset.add(meetingTime); //new keyset
                            meetingmap_day.put(meetingTime, 100); //new map
                        }
                        
                    } else if(timeblocks_liststr[event].indexOf('tentative')>0){
                        //if this event is tentative
                        if(meetingtime_keyset.contains(meetingTime)){ 	//already added - modify value
                            Integer meetingvalue = meetingmap_day.remove(meetingTime);
                            meetingvalue = meetingvalue - 99;
                            if(meetingvalue/100 >0) meetingmap_day.put(meetingTime, meetingvalue);
                            else meetingtime_keyset.remove(meetingTime);
                        } else {										//new meeting time - new key-value map
                            meetingtime_keyset.add(meetingTime); //new keyset
                            meetingmap_day.put(meetingTime, -99); //new map
                        }
                    }
                }
            }
            
            If (totalWeeklyMeeting > 0){
                MeetingAvailableThisweek = true;
            }
            
            switch on dayofweek {
                when 0 {		// monday
                    timeblock_list_mon 			= meetingmap_day;
                    meetingTimeSet_mon 			= meetingtime_keyset;
                    System.debug(LoggingLevel.INFO, 'MON Map-------------'+timeblock_list_mon);
                    System.debug(LoggingLevel.INFO, 'Mon Key-------------'+meetingtime_keyset);
                }	
                when 1 {		// tuesday
                    timeblock_list_tue = meetingmap_day;
                    meetingTimeSet_tue = meetingtime_keyset;
                    System.debug(LoggingLevel.INFO, 'tue Map-------------'+timeblock_list_tue);
                    System.debug(LoggingLevel.INFO, 'tue Key-------------'+meetingtime_keyset);
                }	
                when 2 {		// wednesday
                    timeblock_list_wed = meetingmap_day;
                    meetingTimeSet_wed = meetingtime_keyset;
                    System.debug(LoggingLevel.INFO, 'wed Map-------------'+timeblock_list_wed);
                    System.debug(LoggingLevel.INFO, 'wed Key-------------'+meetingtime_keyset);
                }	
                when 3 {		// thurssday
                    timeblock_list_thu = meetingmap_day;
                    meetingTimeSet_thu = meetingtime_keyset;
                    System.debug(LoggingLevel.INFO, 'thu Map-------------'+timeblock_list_thu);
                    System.debug(LoggingLevel.INFO, 'thu Key-------------'+meetingtime_keyset);
                }	
                when 4 {		// friday
                    timeblock_list_fri = meetingmap_day;
                    meetingTimeSet_fri = meetingtime_keyset;
                    System.debug(LoggingLevel.INFO, 'fri Map-------------'+timeblock_list_fri);
                    System.debug(LoggingLevel.INFO, 'fri Key-------------'+meetingtime_keyset);
                }	
            } 
        }//end of for loop that iterates day of the week
        
        //set literal value of map
        timeblock_value2literal();
    }
    
    public void timeblock_value2literal(){
        timeblock_availability_mon = new Map<String, String>();
        timeblock_availability_tue = new Map<String, String>();
        timeblock_availability_wed = new Map<String, String>();
        timeblock_availability_thu = new Map<String, String>();
        timeblock_availability_fri = new Map<String, String>();
        //mon
        for(String eventTime : timeblock_list_mon.keySet()){
            String event_literal = eventCode2literal(timeblock_list_mon.get(eventTime));
            timeblock_availability_mon.put(eventTime, event_literal);
        }
        //tue
        for(String eventTime : timeblock_list_tue.keySet()){
            String event_literal = eventCode2literal(timeblock_list_tue.get(eventTime));
            timeblock_availability_tue.put(eventTime, event_literal);
        }
        //wed
        for(String eventTime : timeblock_list_wed.keySet()){
            String event_literal = eventCode2literal(timeblock_list_wed.get(eventTime));
            timeblock_availability_wed.put(eventTime, event_literal);
        }
        //thu
        for(String eventTime : timeblock_list_thu.keySet()){
            String event_literal = eventCode2literal(timeblock_list_thu.get(eventTime));
            timeblock_availability_thu.put(eventTime, event_literal);
        }
        //fri
        for(String eventTime : timeblock_list_fri.keySet()){
            String event_literal = eventCode2literal(timeblock_list_fri.get(eventTime));
            timeblock_availability_fri.put(eventTime, event_literal);
        }
    }
    
    public String eventCode2literal(Integer eventCode){
        String eventcode_literal = '';
        
        //10^2 = # of available time block
        Integer available_timeblock = eventCode/100;
        switch on available_timeblock{
            when 1{ //1 att. available
                eventcode_literal += available_timeblock + ' meeting session available ';
            }
            when 0{ //no att. available
                eventcode_literal += 'No meeting session available '; //technically shouldn't be shows as if there is no meeting time is available it won't show a block
            }
            when else{ //1+ atts. available
                eventcode_literal += available_timeblock + ' meeting sessions available ';
            }
        }
        
        if(available_timeblock > 0){ 
        // ignore when no meeting session is available
        
        //10^0 = # of tentative time block
        //Integer tentative_timeblock = eventCode - available_timeblock*100;
        Integer tentative_timeblock = Math.mod(eventCode, 100);
        if(tentative_timeblock >= 0){
            switch on tentative_timeblock{
                when 1{
                    eventcode_literal += '<br/>'+ tentative_timeblock +' pending meeting request';
                }
                when 0{
                    //do nothing
                }
                when else{
                    eventcode_literal += '<br/>'+ tentative_timeblock +' pending meeting requests';
                }
            }
        }
        } 
        return eventcode_literal;
    }
    
    
    //-- Count Available Time blocks : END ------------------------------------------
    
    
    
    
    
    //-- Set target week and timezone : START ------------------------------------------
    
    public void setWeekDates(date monday_param){
        tuesday 	= monday + 1;
        wednesday 	= monday + 2;
        thursday 	= monday + 3;
        friday 		= monday + 4;
        saturday 	= monday + 5;
        //set string format for user
        monday_str		= date2String(monday);
        tuesday_str 	= date2String(tuesday);
        wednesday_str 	= date2String(wednesday);
        thursday_str 	= date2String(thursday);
        friday_str 		= date2String(friday);
        saturday_str 	= date2String(saturday);
    }
    
    public String date2String(Date date_param){
        //String date_str = DateTime.newInstance(date_param.year(), date_param.month(), date_param.day()).format('MM/DD/YYYY');
        String date_str = String.valueOf(date_param);
        return date_str;
    }
    
    public List<SelectOption> getTargetWeekSet(){
        List<SelectOption> targetWeekSet = new List<SelectOption>();
        String targetWeek_option = '';
        Date monday_thisweek =  today_d.toStartOfWeek() + 1;
        Date monday_option = monday_thisweek;
        Date friday_option = monday_option + 5;
        String monday_option_str = date2String(monday_option);
        String friday_option_str = date2String(friday_option);
        
        for(Integer i = 0; i<16 ; i++){
            monday_option = monday_thisweek + (i * 7);
            friday_option = monday_option + 4;
            monday_option_str = date2String(monday_option);
            friday_option_str = date2String(friday_option);
            targetWeek_option = monday_option_str + ' ~ ' + friday_option_str;
            targetWeekSet.add(new SelectOption(String.valueOf(i), targetWeek_option));
        }
        return targetWeekSet;
    }
    
    public List<SelectOption> getTimezoneSet(){
        List<SelectOption> timezoneSet = new List<SelectOption>();
        
        timezoneSet.add(new SelectOption('America/New_York','America/New_York'));
        timezoneSet.add(new SelectOption('America/Chicago','America/Chicago'));
        timezoneSet.add(new SelectOption('America/Denver','America/Denver'));
        timezoneSet.add(new SelectOption('America/Phoenix','America/Phoenix'));
        timezoneSet.add(new SelectOption('America/Los_Angeles','America/Los_Angeles'));
        timezoneSet.add(new SelectOption('America/Anchorage','America/Anchorage'));
        timezoneSet.add(new SelectOption('Pacific/Honolulu','Pacific/Honolulu'));
        
        return timezoneSet;
    }
    
    public void updateTargetWeek(){
        String updatedTargetWeek = '0';
        if(targetWeek != NULL){
            updatedTargetWeek = targetWeek;
        }
        
        System.debug(LoggingLevel.INFO, '**** target week:' + updatedTargetWeek);
        monday = today_d.toStartOfWeek() + 1 + (Integer.valueOf(updatedTargetWeek) *7 );
        setWeekDates(monday);
        monday_urlstr = monday_str + 'T00:00:00';
        saturday_urlstr = saturday_str + 'T00:00:00';
    }
    
    //-- Set target week and timezone : END ------------------------------------------
    
    
    
    
    
    //--Office 365 OAuth2.0 process : START --------------------------------------------------
    public PageReference redirectToOauth(){
        String authurl = 'https://login.microsoftonline.com/6e43c250-90fd-4d15-857c-c97a68e5ba6a/oauth2/v2.0/authorize?response_type=code&client_id='+CLINET_ID+'&scope='+SCOPE+'&grant_type=authorization_code?prompt=admin_consent';
        PageReference pageRef = new PageReference(authurl);
System.debug(LoggingLevel.INFO, '**** auth url: ' + authurl);
        return PageRef;
    }
    
    public void requestToken(){
        String tokenurl = 'https://login.microsoftonline.com/6e43c250-90fd-4d15-857c-c97a68e5ba6a/oauth2/v2.0/token';
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(tokenurl);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        String reqBody = 'grant_type=authorization_code&client_id='+ CLINET_ID + '&scope=' + SCOPE + '&code=' + authcode + '&client_secret=' + PASSWORD;
        
        Blob requestBlob = Blob.valueOf(request.toString());
        EncodingUtil.base64Encode(requestBlob);
        request.setBodyAsBlob(requestBlob);
        request.setBody(reqBody);
        
        HttpResponse response = Http.send(request);
        System.debug(LoggingLevel.INFO, '**** token body: ' + response.getBody());
        System.debug(LoggingLevel.INFO, '**** token status code: ' + response.getStatusCode());
        if(response.getStatusCode() == 200){
            tokenResp = response.getBody();
            Map<String, Object> tokenRespDeserial = (Map<String, Object>)Json.deserializeUntyped(tokenResp);
            authtoken = (String)tokenRespDeserial.get('access_token');
            authtokenType = (String)tokenRespDeserial.get('token_type');
            gotToken = true;
            requestGraph();
        }
    }
    
    public void requestGraph(){
        String requestGraphUrl = 'https://graph.microsoft.com/v1.0/me';
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        request.setEndpoint(requestGraphUrl);
        request.setMethod('GET');
        String authHeader =authtokenType +' '+ authtoken;
        request.setHeader('Authorization', authHeader);
        request.setHeader('Accept', 'application/json');
        
        HttpResponse response = Http.send(request);
        System.debug(LoggingLevel.INFO, '**** graph body: ' + response.getBody());
        System.debug(LoggingLevel.INFO, '**** graph status code:' + response.getStatusCode());
        if(response.getStatusCode() == 200){
            graphResp = response.toString();
            gotGraph = true;
        }
    }
    
    //--Office 365 OAuth2.0 process : END --------------------------
    
    
    
    
    //--Office 365 REST API Calls : START --------------------------    
    /* Only used for API testing
public void getCalInfo(){
String endpointurl = 'https://graph.microsoft.com/v1.0/Users/epmeetings@merceradvisors.com/Calendars';
callO365api(endpointurl);
}

public void callO365api(String endpointurl){        
Http http = new Http();
HttpRequest request = new HttpRequest();
request.setEndpoint(endpointurl);
request.setMethod('GET');
String authHeader = authtokenType +' '+ authtoken;
request.setHeader('Authorization', authHeader);
request.setHeader('Accept', 'text/plain, application/json, text/html');
HttpResponse response = Http.send(request);
System.debug(LoggingLevel.INFO, '****' + response.getBody());
System.debug(LoggingLevel.INFO, '****' + response.getStatusCode());
if(response.getStatusCode() == 200){
System.debug(LoggingLevel.INFO, '****' + response.getBody());
resp = response.getBody();
}     
}
*/
    
    public void getIMAvailableItems(){  
        updateTargetWeek();
        String endpointurl = 'https://graph.microsoft.com/v1.0/Users/'+ calendar_email +'/calendarview?startDateTime=' + monday_urlstr +'&endDateTime='+ saturday_urlstr +'&$Select=Subject,start,end,ShowAs&$top=100';
        callCalView(endpointurl);
    }
    
    public void callCalView(String endpointurl){ 
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointurl);
        request.setMethod('GET');
        String authHeader = authtokenType +' '+ authtoken;
        request.setHeader('Authorization', authHeader);
        request.setHeader('Prefer','outlook.timezone="'+timezone+'"');
        request.setHeader('Accept', 'text/plain, application/json, text/html');
        HttpResponse response = Http.send(request);
System.debug(LoggingLevel.INFO, '**** Cal View status code: ' + response.getStatusCode());
System.debug(LoggingLevel.INFO, '**** Cal View Response: ' + response.getBody());
        if(response.getStatusCode() == 200){
            hasSearchResult = true;
            calSearchResp = response.getBody();
            Map<String, Object> calTimeblockDeserial = (Map<String, Object>)Json.deserializeUntyped(calSearchResp);
            timeblocks = (List<object>)calTimeblockDeserial.get('value');
            System.debug(LoggingLevel.INFO, '**** timeblocks: ' + timeblocks);
            obj2liststr(timeblocks);
            countDailyAvailability();
            timeblocks2timeblock();
        }     
    }
    
    public void obj2liststr(List<object> obj){
        String str = '';
        List<String> liststr = new List<String>();
        for(object o : obj){
            str = String.valueOf(o);
            liststr.add(str);
        }
        timeblocks_liststr = liststr;
    }
    
    
    
    public void createPendingIMevent(){  
        updateTargetWeek();
        String endpointurl = 'https://graph.microsoft.com/v1.0/Users/'+ calendar_email +'/calendar/events';
        callEventCreate(endpointurl);
    }
    
    public void callEventCreate(String endpointurl){
        
        String meetingInfoRequest = finalMeetingReqInfo;
        calendarYear = '20' + meetingInfoRequest.substringAfter('20');
        calendarYear = calendarYear.substringBefore('-') + '-';
        meetingInfoRequest= meetingInfoRequest.substringAfter(calendarYear);
        String meetingInfo_date = calendarYear + meetingInfoRequest.substring(0,5);
        String meetingInfo_time = meetingInfoRequest.substring(5,10);
        Integer meetingInfo_startingTime = Integer.valueOf(meetingInfo_time.substring(0,2));
        String meetingInfo_endingTime = String.valueOf(meetingInfo_startingTime + 2) + meetingInfo_time.substring(2, 5);
        
        String authHeader = authtokenType +' '+ authtoken;
        String reqBody = '';
        
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(endpointurl);
        request.setMethod('POST');
        
        request.setHeader('Authorization', authHeader);
        request.setHeader('Content-Type', 'application/json');
        //request.setHeader('Prefer','outlook.timezone="'+timezone+'"');
        request.setHeader('Accept', 'text/plain, application/json, text/html');
        
        reqBody = '{ "Subject": "[New EP-IM Request] '+ MeetingRequest_clientName +'", "Body": { "ContentType": "HTML", "Content": "Client Name: <strong> '+ MeetingRequest_clientName +'</strong><br/>Handoff Case Link: '+ MeetingRequest_caseLink +'<br/>Related Opportunity Link: '+ MeetingRequest_oppLink +'<br/><br/>Request submitted on '+ System.now() +'(UTC) by '+ UserInfo.getName() +'" }, ';
        reqBody +='"Start": { "DateTime": "'+ meetingInfo_date +'T'+meetingInfo_time+':00", "TimeZone": "'+timezone+'" }, "End": { "DateTime": "'+ meetingInfo_date +'T'+meetingInfo_endingTime+':00", "TimeZone": "'+timezone+'" }, "Attendees": [ { "EmailAddress": { "Address": "'+ UserInfo.getUserEmail() +'", "Name": "'+ UserInfo.getName() +'" }, "Type": "Required" } ], "ShowAs":"Tentative" }';
        
        //Blob requestBlob = Blob.valueOf(request.toString());
        //EncodingUtil.base64Encode(requestBlob);
        //request.setBodyAsBlob(requestBlob);
        request.setBody(reqBody);
        System.debug(LoggingLevel.INFO, '****' + reqBody);
        
        HttpResponse response = Http.send(request);
        System.debug(LoggingLevel.INFO, '****' + response.getBody());
        System.debug(LoggingLevel.INFO, '****' + response.getStatusCode());
        
        if(response.getStatusCode() == 200){
            System.debug(LoggingLevel.INFO, '****-------------------POSTED');
        }     
    }
    
    
    //--Office 365 REST API Calls : END --------------------------    
    
    
    
    
    
    
    //-- Send request email : START ------------------------------------------
    
    public PageReference sendReqEmail(){
        //find public group: EP-IM Request
        //set ID so we don't have duplicate id
        if(finalMeetingReqInfo != NULL){
            displayConfirmation = false;
            String meetingInfoRequest = finalMeetingReqInfo;
        	calendarYear = '20' + meetingInfoRequest.substringAfter('20');
            calendarYear = calendarYear.substringBefore('-') + '-';
            meetingInfoRequest= meetingInfoRequest.substringAfter(calendarYear);
            String meetingInfo_date = calendarYear + meetingInfoRequest.substring(0,5);
            String meetingInfo_time = meetingInfoRequest.substring(5,10) + '(' + timezone +')';
            String meetingInfo_availability = meetingInfoRequest.substring(10);
            
            List<String> requestEmailSentToEmail = setEmailRecipient(request_email_to);
            requestEmailSentToEmail.add(calendar_email);
            String emailSubject = '[New EP-IM Request] ' + MeetingRequest_clientName +' ('+ + meetingInfo_date + ', ' + meetingInfo_time +')';
            String emailBody = '<strong>New EP-IM Request</strong> <br/>&nbsp;<br/>' + 'Date: <strong>' + meetingInfo_date + '</strong> <br/>Time: <strong>'+ meetingInfo_time + '</strong> <br/>Availability: <strong>'+ meetingInfo_availability+'</strong><br/><br/>';
            emailBody += 'Client Name: <strong>' + MeetingRequest_clientName + '</strong><br/>';    
            emailBody += 'Handoff Case Link: ' + MeetingRequest_caseLink + '<br/>';
            emailBody += 'Related Opportunity Link: ' + MeetingRequest_oppLink + '<br/><br/>';
            
            Messaging.SingleEmailMessage requestEmail = new Messaging.SingleEmailMessage();
            requestEmail.setToAddresses(requestEmailSentToEmail);
            requestEmail.setSubject(emailSubject);
            requestEmail.setHtmlBody(emailBody);
            Messaging.sendEmail(New Messaging.SingleEmailMessage[]{requestEmail});
            //email sent
            System.debug(LoggingLevel.INFO, '****--------------------Email sent');
            closeMeetingInfoReq();
            ApexPages.addmessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'Request for EP-Initial Meeting ('+finalMeetingReqInfo_date+', '+finalMeetingReqInfo_time+') has been sent.'));
            
            
        }
        
        return null;
    }
    
    public List<String> setEmailRecipient(String groupName){
        
        createPendingIMevent();
        
        List<String> emailSet = new List<String>();
        List<Id> userIds = new List<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();
        
        //find groupId
        Id EPIMgroupId = [SELECT Id, name FROM Group WHERE name=:groupName limit 1].id;
        if(EPIMgroupId != NULL){
            //loop through group members
            for(GroupMember m : [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: EPIMgroupId]){
                if(((String)m.UserOrGroupId).startsWith(userType)){
                    userIds.add(m.UserOrGroupId);
                }
            }
            
            if(userIds.size()>0){
                for(User u : [SELECT Id, email FROM User WHERE Id IN :userIds]){
                    emailSet.add(u.email);
                }
            }
        }
        
        return emailSet;
    }
    
    public void showConfirmation(){
        String meetingInfoRequest = finalMeetingReqInfo;
        calendarYear = '20' + meetingInfoRequest.substringAfter('20');
        calendarYear = calendarYear.substringBefore('-') + '-';
        
System.debug(LoggingLevel.INFO, '****calendarYear' + calendarYear);
        
        meetingInfoRequest= meetingInfoRequest.substringAfter(calendarYear);
        String meetingInfo_date = calendarYear + meetingInfoRequest.substring(0,5);
        String meetingInfo_time = meetingInfoRequest.substring(5,10);
System.debug(LoggingLevel.INFO, '****meetingInfoRequest' + meetingInfoRequest);
System.debug(LoggingLevel.INFO, '****meetingInfo_date' + meetingInfo_date);
System.debug(LoggingLevel.INFO, '****meetingInfo_time' + meetingInfo_time);
        finalMeetingReqInfo_date = meetingInfo_date;
        finalMeetingReqInfo_time = meetingInfo_time;
        displayConfirmation = true;
    }
    
    public void closeConfirmation(){
        displayConfirmation = false;
    }
    
    public void showMeetingInfoReq(){
        closeConfirmation();
        MeetingRequest_reqInfo = true;
    }
    
    public void closeMeetingInfoReq(){
        MeetingRequest_reqInfo = false;
    }
    //-- Send request email : END ------------------------------------------
    
    
}