/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessReassignmentRules implements Database.AllowsCallouts, Database.Batchable<SObject>, Database.Stateful, System.Schedulable {
    global ProcessReassignmentRules() {

    }
    global void execute(System.SchedulableContext SC) {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global List<FSTR__PCE_Step_Assignment__c> start(Database.BatchableContext bc) {
        return null;
    }
}
