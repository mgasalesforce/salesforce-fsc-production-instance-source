public class ChatterUtils {
    @future
    static public void addFollower(Id userId, Id objectToFollowId) {
        EntitySubscription e = new EntitySubscription();
        e.subscriberId = userId;
        e.parentId = objectToFollowId;
        Database.insert(e,false);
    }

}