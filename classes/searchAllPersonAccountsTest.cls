// @description Test Class for Code Coverage for searchAllPersonAccounts 
// @author Praveen Kanumuri
// @date 7-30-2019

@IsTest(SeeAllData=true)
public class searchAllPersonAccountsTest 
{
	static testMethod void testSearchPersonAccounts1() 
	{
	
                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

                Account a = new Account();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                insert a;
                
                
                //searchAllPersonAccounts cntrl = new searchAllPersonAccounts(); 

                string strJsonAll = '{"name": "Test", "phone": "222", "city": "Test", "state": "MI", "status": "All"}';
                List<string> lstJson = new List<string>();
                lstJson.add(strJsonAll);

                List<Account> lstAcc = new List<Account>();   
                List<List<Account>> lstlstAcc = new List<List<Account>>();           

                lstlstAcc = searchAllPersonAccounts.getPersonAccounts(lstJson);

                System.assertEquals(1, lstlstAcc.size());

                string strJsonActive = '{"name": "Test", "phone": "222", "city": "Test", "state": "MI", "status": "Active"}';
                List<string> lstJsonActive = new List<string>();
                lstJsonActive.add(strJsonActive);
                lstlstAcc = searchAllPersonAccounts.getPersonAccounts(lstJsonActive);

 
	}

        
}