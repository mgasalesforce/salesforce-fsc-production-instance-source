/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Invocable methods and supporting class structure for cloning action plan templates.
 * 
 * Extends the stock Salesforce Action Plans Templates and associated objects to allow for editing
 * of Published templates.  Includes Action Plan Template, Action Plan Template Version, Action Plan
 * Template Item, Action Plan Template Value objects as of Summer '19 release.
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author: Praveen Kanumari - praveen@zennify.com
 * @created: 2019-08-16
 * @modifiedBy: John Crapuchettes - john@zennify.com
 * @modifiedDate: 2019-08-28
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public class CloneActionPlanTemplateInvocable 
{
    //Declare Invocable
    @InvocableMethod(label='Clone Action Plan Template' description='Returns the ID of a new Action Plan Template and all child objects')
    public static List<List<Id>> CloneRecord(List<ActionPlanTemplate> actionPlanTemplateCollection)
    {
       	//Declare Source ID for Action Plan Template and check to make sure the Lists contain values.
       	ID actionPlanTemplateId = null;
       	
        if(actionPlanTemplateCollection.size()>0)
        {
            actionPlanTemplateId = actionPlanTemplateCollection[0].Id;
        }
        if(actionPlanTemplateId != null)
        {
            //Declare Source and Target Action Plan Template
            ActionPlanTemplate sourceApt = [select Id,OwnerId,IsDeleted,Name,LastViewedDate,LastReferencedDate,ActionPlanType,TargetEntityType,Description from ActionPlanTemplate Where Id = :actionPlanTemplateId];
            ActionPlanTemplate targetApt = new ActionPlanTemplate();
    
    
            //Copy values from Source to Target
            targetApt.OwnerId = sourceApt.OwnerId;
            targetApt.Name = sourceApt.Name;
            targetApt.ActionPlanType = sourceApt.ActionPlanType;
            targetApt.TargetEntityType = sourceApt.TargetEntityType;
            targetApt.Description = sourceApt.Description;
            //Create Target Action Plan Template to get an ID.
            insert targetApt;
            system.debug('targetApt Id: '+targetApt.id);
    
            //Declare Source and Target Action Plan Template Version and query for values
            //Action Plan Template Versions are generated automatically with a matching ID value as the Action Plan Template ID.
            ActionPlanTemplateVersion sourceAptv = [Select Id,IsDeleted,Name,LastViewedDate,LastReferencedDate,ActionPlanTemplateId,Version,Status,ActivationDateTime,InactivationDateTime from ActionPlanTemplateVersion where actionPlanTemplateId = :actionPlanTemplateId];
            ActionPlanTemplateVersion targetAptv = [Select Id,IsDeleted,Name,LastViewedDate,LastReferencedDate,ActionPlanTemplateId,Version,Status,ActivationDateTime,InactivationDateTime from ActionPlanTemplateVersion where actionPlanTemplateId = :targetApt.id];
            //No Insert required due to automatic creation of the Version.
            
            //Declare Source Action Plan Template Items List
            List<ActionPlanTemplateItem> sourceApti = [Select Id,IsDeleted,Name,LastViewedDate,LastReferencedDate,ActionPlanTemplateVersionId,DisplayOrder,ItemEntityType,IsRequired from ActionPlanTemplateItem where ActionPlanTemplateVersionId = :sourceAptv.id];		
            Set<Id> sourceAptiIds = (new Map<Id,SObject>(sourceApti)).keySet();
            
            //Declare Target Action Plan Template Items List
            List<ActionPlanTemplateItem> targetApti = new List<ActionPlanTemplateItem>();
            
            //Provide map of <oldid,newid> to find the parent object in the next loops.
            map<id,id> matchAptiIds = new map<id,id>();
            
            //Loop through Source Action Plan Template Items to assign values to the Target Action Plan Template Items List
            for(ActionPlanTemplateItem sapti : sourceApti)
            {    
                ActionPlanTemplateItem newAptiRec = new ActionPlanTemplateItem();
                newAptiRec.Name = sapti.Name+sapti.id;
                newAptiRec.ActionPlanTemplateVersionId = targetAptv.id;
                newAptiRec.DisplayOrder = sapti.DisplayOrder;
                newAptiRec.ItemEntityType = sapti.ItemEntityType;
                newAptiRec.IsRequired = sapti.IsRequired;
                targetApti.add(newAptiRec);
            }
            if(targetApti.size()>0)		
            {
                // Create Target Action Plan Template Items for IDs.
                insert targetApti; 		
            }
    
            //Populate Map from old to new.
            List<ActionPlanTemplateItem> updAptiName = new List<ActionPlanTemplateItem>();
            for(ActionPlanTemplateItem tApti : targetApti)		
            {
                string name = tApti.name;
                string oldIdString = name.right(18);
                id oldId = id.valueof(oldIdString);
                matchAptiIds.put(oldId,tApti.id);
                system.debug('oldId:'+oldId);
                system.debug('newId tApti.id:'+tApti.id);
                ActionPlanTemplateItem uApti = new ActionPlanTemplateItem();
                uapti.id = tApti.Id;
                uapti.name = tApti.name.substring(0,tApti.name.length()-18);
                system.debug('uapti.name:'+uapti.name);
                updAptiName.add(uapti);
            }    
            if(updAptiName.size()>0)    
            {
                update updAptiName;
            }    
            system.debug('matchAptiIds:'+matchAptiIds);
        
            system.debug('sourceAptiIds:'+sourceAptiIds);
    
            //Declare Source Action Plan Template Item Values List
            List<ActionPlanTemplateItemValue> sourceAptiv = [Select Id,IsDeleted,Name,LastViewedDate,LastReferencedDate,ActionPlanTemplateItemId,ItemEntityFieldName,ValueLiteral,ValueFormula,ItemEntityType from ActionPlanTemplateItemValue where ActionPlanTemplateItemId in :sourceAptiIds];
            system.debug('sourceAptiv.size:'+sourceAptiv.size());
            
            //Declare Target Action Plan Template Item Values List 
            List<ActionPlanTemplateItemValue> targetAptiv = new List<ActionPlanTemplateItemValue>();
            
            //Loop through the Source Action Plan Template Item Values to assign values to the Target Action Plan Template Items Values List 
            for(ActionPlanTemplateItemValue saptiv : sourceAptiv)
            {    
                system.debug('here in loop');
                ActionPlanTemplateItemValue newAptivRec = new ActionPlanTemplateItemValue();
                newAptivRec.Name = saptiv.Name;
                newAptivRec.ActionPlanTemplateItemId = matchAptiIds.get(saptiv.ActionPlanTemplateItemId);
                newAptivRec.ItemEntityFieldName = saptiv.ItemEntityFieldName;
                newAptivRec.ValueLiteral = saptiv.ValueLiteral;
                newAptivRec.ValueFormula = saptiv.ValueFormula;
                //newAptivRec.ItemEntityType = saptiv.ItemEntityType;
                targetAptiv.add(newAptivRec);
                system.debug('matchAptiIds.get(saptiv.id):'+matchAptiIds.get(saptiv.id));
            }
            if(targetAptiv.size()>0)
            {
                //Insert Action Plan Template Item Values List
                insert targetAptiv;
            }
            
            //Build returnable List of primatives for invocable requirements.
            List<List<Id>> returnList = new List<List<Id>>();
            List<Id> targetAptIDList = new List<Id>();
            if (targetApt != null)
            {
                //Include the Target Action Plan Template's ID.
                targetAptIDList.add(targetApt.Id);
                returnList.add(targetAptIDList);
            }
            return returnList;
        }
        else
        {
            return null;
        }
    }
}