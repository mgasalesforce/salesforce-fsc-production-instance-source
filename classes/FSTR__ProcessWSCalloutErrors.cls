/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessWSCalloutErrors implements Database.AllowsCallouts, Database.Batchable<SObject> {
    global ProcessWSCalloutErrors() {

    }
    global void execute(Database.BatchableContext BC, List<SObject> scope) {

    }
    global void finish(Database.BatchableContext BC) {

    }
    global void sendErrorEmail(List<FSTR__PCE_Callout_Error_Queue__c> errorQueues) {

    }
    global List<FSTR__PCE_Callout_Error_Queue__c> start(Database.BatchableContext bc) {
        return null;
    }
}
