// @description Test Class for BoxCreateFolders
// @author Praveen Kanumuri
// @date 8-5-2019
@isTest
private class BoxCreateFoldersTest {
     
    private static final String FAKE_TOKEN = 'fakeToken';
     
    private class MockFolder implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
             
            res.setStatusCode(200);
            res.setBody('{"errorCode":"200","message":"SUCCESS","type":"folder","id":"83835684224","name":"NewFolder","access_token":"xxxxxxxx"}');
 
            return res;
        }
    }
 
     private class MockAddTags implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
             
            res.setStatusCode(200);
            res.setBody('{"errorCode":"200","message":"SUCCESS","type":"folder","id":"83835684224","name":"NewFolder","access_token":"xxxxxxxx"}');
 
            return res;
        }
    }

 
     private class MockCopyFolder implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
             
            res.setStatusCode(200);
            res.setBody('{"errorCode":"200","message":"SUCCESS","type":"folder","id":"83835684224","name":"NewFolder","access_token":"xxxxxxxx"}');
 
            return res;
        }
    }

  private class MockCreateFolderAssociation implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
             
            res.setStatusCode(200);
            res.setBody('{"errorCode":"200","message":"SUCCESS","type":"folder","id":"83835684224","name":"NewFolder","access_token":"xxxxxxxx"}');
 
            return res;
        }
    }


    private class MockGetFolderInfo implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('GET', req.getMethod());
            string strBody =  '{"type": "folder","id": "85883204380","sequence_id": "0","etag": "0","name": "Ryan Household","created_at": "2019-08-28T22:16:00-07:00","modified_at": "2019-08-28T22:23:07-07:00","description": "",';
            strBody = strBody + '"size": 0,"path_collection": {"total_count": 3,"entries": [{"type": "folder","id": "0","sequence_id": null,"etag": null,"name": "All Files"},{"type": "folder","id": "82368317415","sequence_id": "1",';
            strBody = strBody + '"etag": "1","name": "Salesforce-00D1U000000ucig"},{"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"}]},"created_by": {"type": "user","id": "9373892167","name": ';
            strBody = strBody + '"Test User","login": "testuser@merceradvisors.com"},"modified_by": {"type": "user","id": "9573931418","name": "MercerFscJwt","login": "AutomationUser@boxdevedition.com"},';
            strBody = strBody + '"trashed_at": null,"purged_at": null,"content_created_at": "2019-08-28T22:16:00-07:00","content_modified_at": "2019-08-28T22:23:07-07:00","owned_by": {"type": "user","id": "9373892167","name": "Test User",';
            strBody = strBody + '"login": "testuser@merceradvisors.com"},"shared_link": null,"folder_upload_email": null,"parent": {"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"},"item_status": "active",';
            strBody = strBody + '"item_collection": {"total_count": 7,"entries": [{"type": "folder","id": "85883344282","sequence_id": "0","etag": "0","name": "Archive"},{"type": "folder","id": "85882692747","sequence_id": "0","etag": "0","name":'; 
            strBody = strBody + '"Client Collaboration"},{"type": "folder","id": "85882525037","sequence_id": "0","etag": "0","name": "Client Profile"},{"type": "folder","id": "85884889091","sequence_id": "0","etag": "0","name": "Compliance"},';
            strBody = strBody + '{"type": "folder","id": "85884841486","sequence_id": "0","etag": "0","name": "Financial Planning"},{"type": "folder","id": "85882910256","sequence_id": "0","etag": "0","name": "Paperwork"},{"type": "folder","id":'; 
            strBody = strBody + '"85881778485","sequence_id": "0","etag": "0","name": "Unique Investments"}],"offset": 0,"limit": 100,"order": [{"by": "type","direction": "ASC"},{"by": "name","direction": "ASC"}]}';

            res.setStatusCode(200);
            res.setBody(strBody);
 
            return res;
        }
    }
 
 private class MockFolderFail implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            System.assertEquals('POST', req.getMethod());
             
            res.setStatusCode(200);
            res.setBody('{"errorCode":"200","message":"SUCCESS","type":"error","id":"83835684224","name":"NewFolder","access_token":"xxxxxxxx"}');
 
            return res;
        }
    }
    
 private class MockToken implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            //System.assertEquals('POST', req.getMethod());             
            res.setStatusCode(200);
            res.setBody('{"access_token":"xxxxxxxx","message":"SUCCESS","errorCode":"200", "grant_type", "urn:ietf:params:oauth:grant-type:jwt-bearer"}');
 
            return res;
        }
    }

@isTest
    static void testcreateFolders(){
                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

                Account a = new Account();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                insert a;

                Branch__c b = new Branch__c();
                b.Name = 'TestBranch';
                b.Branch_Manager__c = UserInfo.getUserId();
                insert b;

                Client_Service_Unit__c csu = new Client_Service_Unit__c();
                csu.branch__c = b.id;
                csu.Client_Advisor__c = UserInfo.getUserId();
                csu.Client_Associate__c = UserInfo.getUserId();
                csu.Name = 'CSU';
                insert csu;

                Id HhRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();

                Account ah = new Account();
                ah.Name = 'TestHousehold';
                ah.Phone = '1111';
                ah.BillingCity = '1111xxx';
                ah.BillingState = 'GA';
                ah.recordTypeId = HhRecordTypeId;
                ah.FinServ__Status__c = 'Active';
                ah.Client_Service_Unit__c = csu.id;
                insert ah;
                

        Test.setMock(HttpCalloutMock.class, new MockFolder());
        Test.startTest();
        String strRes = BoxCreateFolders.createFolders(ah.id); 
        Test.stopTest();
        //System.assertEquals('xxxxxxxx', accessToken);

            
    }

@isTest
    static void testcreateSubFolders(){
                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

                Account a = new Account();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                insert a;

                Branch__c b = new Branch__c();
                b.Name = 'TestBranch';
                b.Branch_Manager__c = UserInfo.getUserId();
                insert b;

                Client_Service_Unit__c csu = new Client_Service_Unit__c();
                csu.branch__c = b.id;
                csu.Client_Advisor__c = UserInfo.getUserId();
                csu.Client_Associate__c = UserInfo.getUserId();
                csu.Name = 'CSU';
                insert csu;

                Id HhRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();

                Account ah = new Account();
                ah.Name = 'TestHousehold';
                ah.Phone = '1111';
                ah.BillingCity = '1111xxx';
                ah.BillingState = 'GA';
                ah.recordTypeId = HhRecordTypeId;
                ah.FinServ__Status__c = 'Active';
                ah.Client_Service_Unit__c = csu.id;
                insert ah;
                

        Test.setMock(HttpCalloutMock.class, new MockFolder());
        Test.startTest();
        String strRes = BoxCreateFolders.createSubFolders(ah.id); 
        Test.stopTest();
        //System.assertEquals('xxxxxxxx', accessToken);

            
    }

@isTest
    static void testgetFolderInfo(){

        Test.setMock(HttpCalloutMock.class, new MockGetFolderInfo());
        Test.startTest();
        String strRes = BoxCreateFolders.getFolderInfo('85884889091',FAKE_TOKEN); 
        Test.stopTest();
        //System.assertEquals('xxxxxxxx', accessToken);

            
    }

@isTest
    static void testCopyFoldersAddTags(){

        Test.setMock(HttpCalloutMock.class, new MockCopyFolder());
        Test.startTest();
        String accname = 'AccountName';
        String strFolderIdByRecordId = '001';
        String strRes = BoxCreateFolders.copyFoldersAddTags(accname,strFolderIdByRecordId);
        Test.stopTest();
        //System.assertEquals('xxxxxxxx', accessToken);

            
    }

@isTest
    static void testCreateFolderAssociation() {
        box.Toolkit boxToolkit = new box.Toolkit();

                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();
                Account a = new Account();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                insert a;

        Test.setMock(HttpCalloutMock.class, new MockCreateFolderAssociation());
        Test.startTest();
        String strRes = BoxCreateFolders.createFolderAssociation(a, boxToolkit);
        Test.stopTest();

    }

@isTest
    static void testextractFolderIdFromJson(){

        string strBody =  '{"type": "folder","id": "85883204380","sequence_id": "0","etag": "0","name": "Ryan Household","created_at": "2019-08-28T22:16:00-07:00","modified_at": "2019-08-28T22:23:07-07:00","description": "",';
        strBody = strBody + '"size": 0,"path_collection": {"total_count": 3,"entries": [{"type": "folder","id": "0","sequence_id": null,"etag": null,"name": "All Files"},{"type": "folder","id": "82368317415","sequence_id": "1",';
        strBody = strBody + '"etag": "1","name": "Salesforce-00D1U000000ucig"},{"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"}]},"created_by": {"type": "user","id": "9373892167","name": ';
        strBody = strBody + '"Test User","login": "testuser@merceradvisors.com"},"modified_by": {"type": "user","id": "9573931418","name": "MercerFscJwt","login": "AutomationUser@boxdevedition.com"},';
        strBody = strBody + '"trashed_at": null,"purged_at": null,"content_created_at": "2019-08-28T22:16:00-07:00","content_modified_at": "2019-08-28T22:23:07-07:00","owned_by": {"type": "user","id": "9373892167","name": "Test User",';
        strBody = strBody + '"login": "testuser@merceradvisors.com"},"shared_link": null,"folder_upload_email": null,"parent": {"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"},"item_status": "active",';
        strBody = strBody + '"item_collection": {"total_count": 7,"entries": [{"type": "folder","id": "85883344282","sequence_id": "0","etag": "0","name": "Archive"},{"type": "folder","id": "85882692747","sequence_id": "0","etag": "0","name":'; 
        strBody = strBody + '"Client Collaboration"},{"type": "folder","id": "85882525037","sequence_id": "0","etag": "0","name": "Client Profile"},{"type": "folder","id": "85884889091","sequence_id": "0","etag": "0","name": "Compliance"},';
        strBody = strBody + '{"type": "folder","id": "85884841486","sequence_id": "0","etag": "0","name": "Financial Planning"},{"type": "folder","id": "85882910256","sequence_id": "0","etag": "0","name": "Paperwork"},{"type": "folder","id":'; 
        strBody = strBody + '"85881778485","sequence_id": "0","etag": "0","name": "Unique Investments"}],"offset": 0,"limit": 100,"order": [{"by": "type","direction": "ASC"},{"by": "name","direction": "ASC"}]}';


        String strFolderId = BoxCreateFolders.extractFolderIdFromJson(strBody, 'Compliance');
        System.assertEquals('85884889091', strFolderId);
    }

@isTest
    static void testExtractSubFolderCountFromJson(){

        string strBody =  '{"type": "folder","id": "85883204380","sequence_id": "0","etag": "0","name": "Ryan Household","created_at": "2019-08-28T22:16:00-07:00","modified_at": "2019-08-28T22:23:07-07:00","description": "",';
        strBody = strBody + '"size": 0,"path_collection": {"total_count": 3,"entries": [{"type": "folder","id": "0","sequence_id": null,"etag": null,"name": "All Files"},{"type": "folder","id": "82368317415","sequence_id": "1",';
        strBody = strBody + '"etag": "1","name": "Salesforce-00D1U000000ucig"},{"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"}]},"created_by": {"type": "user","id": "9373892167","name": ';
        strBody = strBody + '"Test User","login": "testuser@merceradvisors.com"},"modified_by": {"type": "user","id": "9573931418","name": "MercerFscJwt","login": "AutomationUser@boxdevedition.com"},';
        strBody = strBody + '"trashed_at": null,"purged_at": null,"content_created_at": "2019-08-28T22:16:00-07:00","content_modified_at": "2019-08-28T22:23:07-07:00","owned_by": {"type": "user","id": "9373892167","name": "Test User",';
        strBody = strBody + '"login": "testuser@merceradvisors.com"},"shared_link": null,"folder_upload_email": null,"parent": {"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"},"item_status": "active",';
        strBody = strBody + '"item_collection": {"total_count": 7,"entries": [{"type": "folder","id": "85883344282","sequence_id": "0","etag": "0","name": "Archive"},{"type": "folder","id": "85882692747","sequence_id": "0","etag": "0","name":'; 
        strBody = strBody + '"Client Collaboration"},{"type": "folder","id": "85882525037","sequence_id": "0","etag": "0","name": "Client Profile"},{"type": "folder","id": "85884889091","sequence_id": "0","etag": "0","name": "Compliance"},';
        strBody = strBody + '{"type": "folder","id": "85884841486","sequence_id": "0","etag": "0","name": "Financial Planning"},{"type": "folder","id": "85882910256","sequence_id": "0","etag": "0","name": "Paperwork"},{"type": "folder","id":'; 
        strBody = strBody + '"85881778485","sequence_id": "0","etag": "0","name": "Unique Investments"}],"offset": 0,"limit": 100,"order": [{"by": "type","direction": "ASC"},{"by": "name","direction": "ASC"}]}';

        String strSubFolderCount = BoxCreateFolders.ExtractSubFolderCountFromJson(strBody);
        System.assertEquals('7', strSubFolderCount);
    }

@isTest
    static void testAddTags(){

        string strBody =  '{"type": "folder","id": "85883204380","sequence_id": "0","etag": "0","name": "Ryan Household","created_at": "2019-08-28T22:16:00-07:00","modified_at": "2019-08-28T22:23:07-07:00","description": "",';
        strBody = strBody + '"size": 0,"path_collection": {"total_count": 3,"entries": [{"type": "folder","id": "0","sequence_id": null,"etag": null,"name": "All Files"},{"type": "folder","id": "82368317415","sequence_id": "1",';
        strBody = strBody + '"etag": "1","name": "Salesforce-00D1U000000ucig"},{"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"}]},"created_by": {"type": "user","id": "9373892167","name": ';
        strBody = strBody + '"Test User","login": "testuser@merceradvisors.com"},"modified_by": {"type": "user","id": "9573931418","name": "MercerFscJwt","login": "AutomationUser@boxdevedition.com"},';
        strBody = strBody + '"trashed_at": null,"purged_at": null,"content_created_at": "2019-08-28T22:16:00-07:00","content_modified_at": "2019-08-28T22:23:07-07:00","owned_by": {"type": "user","id": "9373892167","name": "Test User",';
        strBody = strBody + '"login": "testuser@merceradvisors.com"},"shared_link": null,"folder_upload_email": null,"parent": {"type": "folder","id": "82365809711","sequence_id": "1","etag": "1","name": "Accounts"},"item_status": "active",';
        strBody = strBody + '"item_collection": {"total_count": 7,"entries": [{"type": "folder","id": "85883344282","sequence_id": "0","etag": "0","name": "Archive"},{"type": "folder","id": "85882692747","sequence_id": "0","etag": "0","name":'; 
        strBody = strBody + '"Client Collaboration"},{"type": "folder","id": "85882525037","sequence_id": "0","etag": "0","name": "Client Profile"},{"type": "folder","id": "85884889091","sequence_id": "0","etag": "0","name": "Compliance"},';
        strBody = strBody + '{"type": "folder","id": "85884841486","sequence_id": "0","etag": "0","name": "Financial Planning"},{"type": "folder","id": "85882910256","sequence_id": "0","etag": "0","name": "Paperwork"},{"type": "folder","id":'; 
        strBody = strBody + '"85881778485","sequence_id": "0","etag": "0","name": "Unique Investments"}],"offset": 0,"limit": 100,"order": [{"by": "type","direction": "ASC"},{"by": "name","direction": "ASC"}]}';

        Test.setMock(HttpCalloutMock.class, new MockAddTags());
        Test.startTest();
        String strSubFolderCount = BoxCreateFolders.addTags(strBody, 'Compliance');
        Test.stopTest();

        System.assertEquals('7', strSubFolderCount);
    }

@isTest
    static void testcreateFoldersFail(){
                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

                Account a = new Account();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                insert a;

                Branch__c b = new Branch__c();
                b.Name = 'TestBranch';
                b.Branch_Manager__c = UserInfo.getUserId();
                insert b;

                Client_Service_Unit__c csu = new Client_Service_Unit__c();
                csu.branch__c = b.id;
                csu.Client_Advisor__c = UserInfo.getUserId();
                csu.Client_Associate__c = UserInfo.getUserId();
                csu.Name = 'CSU';
                insert csu;

                Id HhRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();

                Account ah = new Account();
                ah.Name = 'TestHousehold';
                ah.Phone = '1111';
                ah.BillingCity = '1111xxx';
                ah.BillingState = 'GA';
                ah.recordTypeId = HhRecordTypeId;
                ah.FinServ__Status__c = 'Active';
                ah.Client_Service_Unit__c = csu.id;
                insert ah;
                

        Test.setMock(HttpCalloutMock.class, new MockFolderFail());
        Test.startTest();
        String strRes = BoxCreateFolders.createFolders(ah.id); 
        Test.stopTest();
        //System.assertEquals('xxxxxxxx', accessToken);

            
    }

@isTest
    static void testcreateFolder(){
                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

                Account a = new Account();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                insert a;

                Branch__c b = new Branch__c();
                b.Name = 'TestBranch';
                b.Branch_Manager__c = UserInfo.getUserId();
                insert b;

                Client_Service_Unit__c csu = new Client_Service_Unit__c();
                csu.branch__c = b.id;
                csu.Client_Advisor__c = UserInfo.getUserId();
                csu.Client_Associate__c = UserInfo.getUserId();
                csu.Name = 'CSU';
                insert csu;

                Id HhRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();

                Account ah = new Account();
                ah.Name = 'TestHousehold';
                ah.Phone = '1111';
                ah.BillingCity = '1111xxx';
                ah.BillingState = 'GA';
                ah.recordTypeId = HhRecordTypeId;
                ah.FinServ__Status__c = 'Active';
                ah.Client_Service_Unit__c = csu.id;
                insert ah;
                

        Test.setMock(HttpCalloutMock.class, new MockFolder());
        Test.startTest();
        String strRes = BoxCreateFolders.createFolder(ah.name,'11111','fakeToken'); 
        Test.stopTest();
        //System.assertEquals('xxxxxxxx', accessToken);

            
    }

}