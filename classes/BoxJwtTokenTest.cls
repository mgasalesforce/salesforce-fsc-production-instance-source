@isTest
private class BoxJwtTokenTest {
     
    private static final String FAKE_TOKEN = 'fakeToken';
     
    private class Mock implements HttpCalloutMock {
 
        public HTTPResponse respond(HTTPRequest req) {
             
            HTTPResponse res = new HTTPResponse();
            //System.assertEquals('POST', req.getMethod());
            System.assert(req.getBody().contains('grant_type'), req.getBody());
            System.assert(req.getBody().contains('assertion'), req.getBody());
             
            res.setStatusCode(200);
            res.setBody('{"scope":"api","expires_in":4278,"restricted_to":[],"token_type":"bearer","access_token":"' + FAKE_TOKEN + '"}');
 
            return res;
        }
    }
 
 
    @isTest
    static void testRequestAccessToken() {


        List<Box_JWT_Authentication__mdt> boxAuthMdt  = [select jwtUsername__c,jwtPrivateKey__c,jwtHostname__c,jwtPublicKeyId__c,
                                                 jwtConnectedAppConsumerKey__c,boxSubType__c,jwtJti__c, HouseholdMasterFolderStructure_FolderId__c 
                                                 from Box_JWT_Authentication__mdt
                                                where label = 'BoxJwtAuthFinal'];
                                                                                                                            

        BoxJwtToken.Configuration config = new BoxJwtToken.Configuration();
        config.jwtUsername = boxAuthMdt[0].jwtUsername__c; 
        config.privateKey = boxAuthMdt[0].jwtPrivateKey__c; 
        config.publicKeyId = boxAuthMdt[0].jwtPublicKeyId__c; 
        config.jwtHostname = boxAuthMdt[0].jwtHostname__c; 
        config.jwtJti = boxAuthMdt[0].jwtJti__c; 
        config.box_sub_type = boxAuthMdt[0].boxSubType__c; 
        config.jwtConnectedAppConsumerKey = boxAuthMdt[0].jwtConnectedAppConsumerKey__c; 
    
         
        Test.setMock(HttpCalloutMock.class, new Mock());
        Test.startTest();
        String accessToken = new BoxJwtToken(config).requestAccessToken();
        Test.stopTest();
        system.debug('accessToken:'+accessToken);
        System.assertEquals(FAKE_TOKEN, accessToken);
        //System.assertEquals(grant_type, 'urn:ietf:params:oauth:grant-type:jwt-bearer');
    }

    @isTest
    static void testFormEncode(){
        
        List<Box_JWT_Authentication__mdt> boxAuthMdt  = [select jwtUsername__c,jwtPrivateKey__c,jwtHostname__c,jwtPublicKeyId__c,
                                                 jwtConnectedAppConsumerKey__c,boxSubType__c,jwtJti__c, HouseholdMasterFolderStructure_FolderId__c 
                                                 from Box_JWT_Authentication__mdt
                                                where label = 'BoxJwtAuthFinal'];
                                                                                                                            

        BoxJwtToken.Configuration config = new BoxJwtToken.Configuration();
        config.jwtUsername = boxAuthMdt[0].jwtUsername__c; 
        config.privateKey = boxAuthMdt[0].jwtPrivateKey__c; 
        config.publicKeyId = boxAuthMdt[0].jwtPublicKeyId__c; 
        config.jwtHostname = boxAuthMdt[0].jwtHostname__c; 
        config.jwtJti = boxAuthMdt[0].jwtJti__c; 
        config.box_sub_type = boxAuthMdt[0].boxSubType__c; 
        config.jwtConnectedAppConsumerKey = boxAuthMdt[0].jwtConnectedAppConsumerKey__c; 

        Map<String, String> m = new Map<String, String>();
         m.put('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer');
         m.put('assertion', 'xxxxxyyyyzzzzzz');
        string body = new BoxJwtToken(config).formEncode(m); 
    }

    @isTest
    static void testmDebugUntruncated()
    {
        BoxJwtToken.Configuration config = new BoxJwtToken.Configuration();
        string str = 'asdasdasdasdasdasdasdasdasd';
        new BoxJwtToken(config).mDebugUntruncated(str);
    }

    @isTest
    static void testgenerateRandomString()
    {
        BoxJwtToken.Configuration config = new BoxJwtToken.Configuration();
        new BoxJwtToken(config).generateRandomString(16);        
    }


    
    
}