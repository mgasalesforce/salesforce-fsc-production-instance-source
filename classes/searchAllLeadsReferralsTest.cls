// @description Test Class for Code Coverage for searchAllLeadsReferrals 
// @author Praveen Kanumuri
// @date 7-30-2019

@IsTest(SeeAllData=true)
public class searchAllLeadsReferralsTest 
{
	static testMethod void testsearchAllLeadsReferralsTest1() 
	{
	
                Id RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Marketing Lead').getRecordTypeId();

                Lead a = new Lead();
                a.FirstName = 'TestName';
                a.LastName = 'TestName';
                a.Phone = '2222';
                a.City = 'TestCity';
                a.State = 'MI';
                a.LeadSource = 'Call-in';
                a.RecordTypeId = RecordTypeId;
                //a.Status = 'Closed: Not Converted';
                insert a;
                
                
                //searchAllPersonAccounts cntrl = new searchAllPersonAccounts(); 

                string strJsonAll = '{"name": "Test", "phone": "222", "city": "Test", "state": "MI", "status": "All"}';
                List<string> lstJson = new List<string>();
                lstJson.add(strJsonAll);

                List<Lead> lstLead = new List<Lead>();   
                List<List<Lead>> lstlstLead = new List<List<Lead>>();           

                lstlstLead = searchAllLeadsReferrals.getLeadsReferrals(lstJson);

                System.assertEquals(1, lstlstLead.size());

                string strJsonActive = '{"name": "Test", "phone": "", "city": "Test", "state": "MI", "status": "Active"}';
                List<string> lstJsonActive = new List<string>();
                lstJsonActive.add(strJsonActive);
                lstlstLead = searchAllLeadsReferrals.getLeadsReferrals(lstJsonActive);

 
	}

        
}