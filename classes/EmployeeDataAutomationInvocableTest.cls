/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Test Methods for Employee Data Automation Invocable class
 * 
 * Provides test coverage for Employee Data Automation Class methods.
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author: John Crapuchettes - john@zennify.com
 * @created: 2019-09-17
 * @modifiedBy: John Crapuchettes - john@zennify.com
 * @modifiedDate: 2019-09-17
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */
@isTest(SeeAllData=true)
public class EmployeeDataAutomationInvocableTest {    
    @isTest
    public static void testRelatedCases()
    {
        // Get Starting Points
        RecordType rtHousehold = [SELECT ID FROM RecordType WHERE DeveloperName = 'IndustriesHousehold' LIMIT 1];
        RecordType rtBranchOps = [SELECT ID FROM RecordType WHERE DeveloperName = 'Branch_Operations' LIMIT 1];
        User uAdmin = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND isActive = TRUE LIMIT 1];
        
        Branch__c branch = new Branch__c(Name = 'Test', Managing_Director__c = uAdmin.id);
        insert branch;
        
        Client_Service_Unit__c csu = new Client_Service_Unit__c();
        csu.Name = 'Test';
        csu.Branch__c = branch.id;
        csu.Client_Advisor__c = uAdmin.id;
        insert csu;
        
        // Create Account
        Account aHousehold = new Account();
        aHousehold.Name = 'Test Account';
        aHousehold.RecordTypeId = rtHousehold.id;
        aHousehold.Client_Service_Unit__c = csu.id;
        aHousehold.FinServ__Status__c = 'Active';
        aHousehold.Customer_Type__c = 'Employee';
        aHousehold.Employee__c = uAdmin.id;
        insert aHousehold;
              
        // Create Case to Run On
        Case cBranchOps = new Case();
        cBranchOps.AccountId = aHousehold.id;
        cBranchOps.RecordTypeId = rtBranchOps.id;
        cBranchOps.Customer_Type__c = 'Customer';        
        cBranchOps.Requested_Completion_Date__c = system.today();
        insert cBranchOps;
        
        Case cGood = [SELECT ID, Customer_Type__c, Employee__c FROM Case WHERE ID =: cBranchOps.Id];
        
        // Test for Process Builder normalizing employee data on creation.
        //system.assertEquals(cGood.Customer_Type__c, aHousehold.Customer_Type__c);
        //system.assertEquals(cGood.Employee__c, aHousehold.Employee__c);
        
        // Change the Household's values
        Account aChange = [SELECT ID, Customer_Type__c, Employee__c FROM Account WHERE ID =: aHousehold.Id];
        aChange.Customer_Type__c = 'Customer';
        aChange.Employee__c = null;
        update aChange;
        
        // Call Invocable
        Test.startTest();
        EmployeeDataAutomationInvocable.updateRelatedByAccount(new List<ID>{aChange.Id});
        Test.stopTest();
        
        Case cChange = [SELECT ID, Customer_Type__c, Employee__c FROM Case WHERE ID =: cBranchOps.Id];
        
        // Test for changes after the Invocable method.
        //system.assertEquals(cChange.Customer_Type__c, aChange.Customer_Type__c);
        //system.assertEquals(cChange.Employee__c, aChange.Employee__c);
    }
    @isTest
    public static void testRelatedOpportunities()
    {
        // Get Starting Points
        RecordType rtHousehold = [SELECT ID FROM RecordType WHERE DeveloperName = 'IndustriesHousehold' LIMIT 1];
        RecordType rtEstate = [SELECT ID FROM RecordType WHERE DeveloperName = 'Estate_Planning' LIMIT 1];
        User uAdmin = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND IsActive=True LIMIT 1];
        
        Branch__c branch = new Branch__c(Name = 'Test', Managing_Director__c = uAdmin.id);
        insert branch;
        
        Client_Service_Unit__c csu = new Client_Service_Unit__c();
        csu.Name = 'Test';
        csu.Branch__c = branch.id;
        csu.Client_Advisor__c = uAdmin.id;
        insert csu;
        
        // Create Account
        Account aHousehold = new Account();
        aHousehold.Name = 'Test Account';
        aHousehold.RecordTypeId = rtHousehold.id;
        aHousehold.Client_Service_Unit__c = csu.id;
        aHousehold.FinServ__Status__c = 'Active';
        aHousehold.Customer_Type__c = 'Employee';
        aHousehold.Employee__c = uAdmin.id;
        insert aHousehold;
        
        //Create Opportunity to Run On
        Opportunity oEstate = new Opportunity();
        oEstate.RecordTypeId = rtEstate.id;
        oEstate.Name = 'asdf';
        oEstate.AccountId = aHousehold.Id;
        oEstate.StageName = 'New';
        oEstate.CloseDate = system.today();
        insert oEstate;
        
        Opportunity oGood = [SELECT ID, Customer_Type__c, Employee__c FROM Opportunity WHERE ID =: oEstate.Id];
        
        // Test for Process Builder normalizing employee data on creation.
        //system.assertEquals(oGood.Customer_Type__c, aHousehold.Customer_Type__c);
        //system.assertEquals(oGood.Employee__c, aHousehold.Employee__c);
        
        // Change the Household's values
        Account aChange = [SELECT ID, Customer_Type__c, Employee__c FROM Account WHERE ID =: aHousehold.Id];
        aChange.Customer_Type__c = 'Customer';
        aChange.Employee__c = null;
        update aChange;
        
        // Call Invocable
        Test.startTest();
        EmployeeDataAutomationInvocable.updateRelatedByAccount(new List<ID>{aChange.Id});
        Test.stopTest();
        
        Opportunity oChange = [SELECT ID, Customer_Type__c, Employee__c FROM Opportunity WHERE ID =: oEstate.Id];
        
        //system.assertEquals(oChange.Customer_Type__c, aChange.Customer_Type__c);
        //system.assertEquals(oChange.Employee__c, aChange.Employee__c);
    }
    
    /*
    @isTest
    public static void testRelatedFinancialAccounts()
    {
        // Get Starting Points
        RecordType rtHousehold = [SELECT ID FROM RecordType WHERE DeveloperName = 'IndustriesHousehold' LIMIT 1];
        RecordType rtPersonAccount = [SELECT ID FROM RecordType WHERE DeveloperName = 'PersonAccount' LIMIT 1];
        RecordType rtEnvestnet = [SELECT ID FROM RecordType WHERE DeveloperName = 'Envestnet_Account' LIMIT 1];
        User uAdmin = [SELECT ID FROM User WHERE Profile.Name = 'System Administrator' AND IsActive=True LIMIT 1];
        Client_Service_Unit__c csu = [SELECT ID FROM Client_Service_Unit__c LIMIT 1];
        
        // Create Account
        Account aHousehold = new Account();
        aHousehold.Name = 'Test Account';
        aHousehold.RecordTypeId = rtHousehold.id;
        aHousehold.Client_Service_Unit__c = csu.id;
        aHousehold.FinServ__Status__c = 'Active';
        aHousehold.Customer_Type__c = 'Employee';
        aHousehold.Employee__c = uAdmin.id;
        insert aHousehold;
        
        // Create Person Account
        Account aPerson = new Account();
        aPerson.LastName = 'Test';
        aPerson.FirstName = 'Test';
        aPerson.RecordTypeId = rtPersonAccount.id;
        aPerson.FinServ__Status__c = 'Active';
        //aPerson.Client_Service_Unit__c = csu.id;
        aPerson.BillingStreet = '123';
        aPerson.Customer_Type__c = 'Employee';
        aPerson.Employee__c = uAdmin.id;
        insert aPerson;
        
        // Get Person Account's Contact ID
        Account aPersonContact = [SELECT ID, PersonContactId FROM Account WHERE ID =: aPerson.id LIMIT 1];
        
        // Create ACR
        AccountContactRelation acr = new AccountContactRelation();
        acr.AccountId = aHousehold.id;
        acr.ContactId = aPersonContact.PersonContactId;
        acr.Roles = 'Client';
        acr.FinServ__PrimaryGroup__c = True;
        acr.FinServ__Primary__c = True;
        acr.FinServ__Rollups__c = 'All';
        insert acr;
        
        //Create Financial Account
        FinServ__FinancialAccount__c fEnv = new FinServ__FinancialAccount__c();
        fEnv.RecordTypeId = rtEnvestnet.Id;
        fEnv.Name = 'Test Env 1';
        fEnv.FinServ__PrimaryOwner__c = aPerson.id;
        fEnv.FinServ__Ownership__c = 'Individual';
        fEnv.FinServ__Balance__c = 200000;
        fEnv.FinServ__Managed__c = true;
        insert fEnv;
        
        FinServ__FinancialAccount__c fGood = [SELECT ID, Customer_Type__c, Employee__c FROM FinServ__FinancialAccount__c WHERE ID =: fEnv.Id];
        
        system.assertEquals(fGood.Customer_Type__c, aHousehold.Customer_Type__c);
        system.assertEquals(fGood.Employee__c, aHousehold.Employee__c);
        
        // Change the Household's values
        Account aHChange = [SELECT ID, Customer_Type__c, Employee__c FROM Account WHERE ID =: aHousehold.Id];
        aHChange.Customer_Type__c = 'Customer';
        aHChange.Employee__c = null;
        update aHChange;
        
        Account aPChange = [SELECT ID, Customer_Type__c, Employee__c FROM Account WHERE ID =: aPerson.Id];
        aPChange.Customer_Type__c = 'Customer';
        aPChange.Employee__c = null;
        update aPChange;
        
        // Call Invocable
        Test.startTest();
        EmployeeDataAutomationInvocable.updateRelatedByAccount(new List<ID>{aPChange.Id});
        Test.stopTest();
        
         FinServ__FinancialAccount__c fChange = [SELECT ID, Customer_Type__c, Employee__c FROM FinServ__FinancialAccount__c WHERE ID =: fEnv.Id];
        
        // Test for changes after the Invocable method.
        system.assertEquals(fChange.Customer_Type__c, aPChange.Customer_Type__c);
        system.assertEquals(fChange.Employee__c, aPChange.Employee__c);
    }*/
}