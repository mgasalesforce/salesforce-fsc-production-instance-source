// @description Lightning Component Action to Clone Opportunity and Child Records based on FieldSet 
// @author Praveen Kanumuri
// @date 7-10-2019

public without sharing class CloneOpportunityObject {
    
    @AuraEnabled
    public static String cloneRecord(String recordId){
        
            List<Opportunity> lstOppRecordType = [Select RecordType.Name,RecordType.DeveloperName FROM Opportunity where id = :recordId];
            string recordType = lstOppRecordType[0].RecordType.Name;
            string recordTypeDevName = lstOppRecordType[0].RecordType.DeveloperName;

            //check if a fieldset with recordtype name exists. If not default to 'cloneOpportunity' fieldset
       
             //system.debug('recordType1:' + recordType);

            //if(!fieldSetExists(recordType,'Opportunity'))
            //{
                       
            //}

            if(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordType).getRecordTypeId() == NULL)
            {
	            recordType = 'cloneOpportunity'; 
            }


            //system.debug('recordType2:' + recordType);


        //Schema.FieldSet fs1 = Schema.SObjectType.Opportunity.fieldSets.getMap().get('CloneOpportunity');        

        //List<Schema.FieldSetMember> fieldList = SObjectType.Opportunity.FieldSets.CloneOpportunity.getFields();

        //system.debug('recordType:' + recordType);

        string soqlQuery = 'SELECT ';

        List<Schema.FieldSetMember> fieldListOpp = readFieldSet(recordTypeDevName,'Opportunity');

        for(Schema.FieldSetMember f : fieldListOpp) {
            if(schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().isAccessible()){
                soqlQuery = soqlQuery+f.getFieldPath() +','; 
            }
         }


        List<Schema.FieldSetMember> fieldListOppFinAcc = readFieldSet('Clone_Opportunity_Financial_Account','Opportunity_Financial_Account__c');
        string soqlQueryOppFinAcc = ' (SELECT ';
        for(Schema.FieldSetMember f : fieldListOppFinAcc) {
            if(schema.getGlobalDescribe().get('Opportunity_Financial_Account__c').getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().isAccessible()){
                soqlQueryOppFinAcc = soqlQueryOppFinAcc+f.getFieldPath() +','; 
            }
         }
        soqlQueryOppFinAcc = soqlQueryOppFinAcc.removeEnd(',');
        soqlQueryOppFinAcc += ' FROM Opportunity_Financial_Accounts__r)';


        //OpportunityLineItems
        /*
        List<Schema.FieldSetMember> fieldListOppLineItems = readFieldSet('Clone_Opportunity_Product','OpportunityLineItem');
        string soqlQueryOppLineItems = ' (SELECT ';
        for(Schema.FieldSetMember f : fieldListOppLineItems) {
            if(schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().isAccessible()){
                soqlQueryOppLineItems = soqlQueryOppLineItems+f.getFieldPath() +','; 
            }
         }
        soqlQueryOppLineItems = soqlQueryOppLineItems.removeEnd(',');
        soqlQueryOppLineItems += ' FROM OpportunityLineItems)';
        */

        Map<String, Schema.SObjectType> oppSchemaMap = Schema.getGlobalDescribe();    
        Set <String> oppConRoleFieldMap = oppSchemaMap.get('OpportunityContactRole').getDescribe().fields.getMap().keySet();

        string soqlQueryOppConRole = ' (SELECT  ' ; 
        
        for (String s :oppConRoleFieldMap ){
            if(schema.getGlobalDescribe().get('OpportunityContactRole').getDescribe().fields.getMap().get(s).getDescribe().isAccessible()){
                soqlQueryOppConRole +=  + s+',';
            }
        }

        soqlQueryOppConRole =  soqlQueryOppConRole.removeEnd(',');

        soqlQueryOppConRole = soqlQueryOppConRole + ' FROM OpportunityContactRoles) ';

        //soqlQuery = soqlQuery + soqlQueryOppFinAcc + ' , ' + soqlQueryOppLineItems + ' , ' + soqlQueryOppConRole;
        soqlQuery = soqlQuery + soqlQueryOppFinAcc + ' , '  + soqlQueryOppConRole;
        soqlQuery += ' FROM Opportunity WHERE ID = \'' + recordId +'\'' ;
        //System.debug('soqlQuery opportunity'+soqlQuery);

        //Query for the Opportunity record types
        List<RecordType> oppRtypes = [Select Name, Id From RecordType 
                  where sObjectType='Opportunity' and isActive=true];
     
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> oppRecordTypes = new Map<String,String>{};
        for(RecordType rt: OppRtypes)
            oppRecordTypes.put(rt.Name,rt.Id);

        Opportunity origOpp = Database.query(soqlQuery);
        Opportunity clondedOpp = origOpp.clone(false, false, false, false);
        
        // Setting the Opportunity's IsAClone flag for automation/validation - John C 7/18/19
        clondedOpp.IsAClone__c = true;
        
        try{
            //if(origOpp.RecordTypeId == oppRecordTypes.get('Advisory Sales'))
            //{
                //clondedOpp.StageName = 'Closed'; --Make changes to this after clarification from Client.
                //clondedOpp.CloseDate = NULL; //Next Meeting/Action Item Date - This is calculated from Activities
                //system.debug('');
            //}
            if (origOpp.RecordTypeId == oppRecordTypes.get('Tax Preparation') || origOpp.RecordTypeId == oppRecordTypes.get('Estate Planning') || origOpp.RecordTypeId == oppRecordTypes.get('Advisory Sales'))
            {
                clondedOpp.StageName = 'New';
            }
            //System.debug('origOpp:' +origOpp);
            insert clondedOpp ;

            id oppClonedId = clondedOpp.id;

            /* //OpportunityLineItems 
            Map<Opportunity,List<OpportunityLineItem>> OpportunityLineItemsMapping = new Map<Opportunity,List<OpportunityLineItem>>();
            OpportunityLineItemsMapping.put(clondedOpp,origOpp.OpportunityLineItems.deepClone(false,false,false));

            for(Opportunity opp :OpportunityLineItemsMapping.keySet()){
                for(OpportunityLineItem oppLineItem : OpportunityLineItemsMapping.get(opp)){
                    oppLineItem.OpportunityId = opp.Id;
                }
            }

            if(OpportunityLineItemsMapping.size() >0)
            {
                list<OpportunityLineItem> newOppLineItems = OpportunityLineItemsMapping.get(clondedOpp);
                insert newOppLineItems; //OpportunityLineItemsMapping.values();
            }
            */

            Map<Opportunity,List<Opportunity_Financial_Account__c>> OpportunityFinAccMapping = new Map<Opportunity,List<Opportunity_Financial_Account__c>>();
            OpportunityFinAccMapping.put(clondedOpp,origOpp.Opportunity_Financial_Accounts__r.deepClone(false,false,false));

            for(Opportunity opp :OpportunityFinAccMapping.keySet()){
                for(Opportunity_Financial_Account__c oppFinAcc : OpportunityFinAccMapping.get(opp)){
                    oppFinAcc.Opportunity__c = opp.Id;
                }
            }

            if(OpportunityFinAccMapping.size() >0)
            {
                list<Opportunity_Financial_Account__c> newOppFinAcc = OpportunityFinAccMapping.get(clondedOpp);
                insert newOppFinAcc; //OpportunityLineItemsMapping.values();
            }


            Map<Opportunity,List<OpportunityContactRole>> OpportunityConRoleMapping = new Map<Opportunity,List<OpportunityContactRole>>();
            OpportunityConRoleMapping.put(clondedOpp,origOpp.OpportunityContactRoles.deepClone(false,false,false));

            for(Opportunity opp :OpportunityConRoleMapping.keySet()){
                for(OpportunityContactRole oppConRole : OpportunityConRoleMapping.get(opp)){
                    oppConRole.OpportunityId = opp.Id;
                }
            }

            if(OpportunityConRoleMapping.size() >0)
            {
                list<OpportunityContactRole> newOppConRole = OpportunityConRoleMapping.get(clondedOpp);
                insert newOppConRole; //OpportunityLineItemsMapping.values();
            }


            return clondedOpp.id ;
        }catch(Exception e){
            //system.debug(e.getMessage());
            return recordId; //e.getMessage() ;
        }
       
        
    }



    @AuraEnabled
    public static String cloneMultipleRecords (string selRecordIds){ //(List<string> recordIds){

        
        //system.debug('selRecordIds:'+selRecordIds);

        List<String> recordIds = new List<String>();
        Object[] values = (Object[])System.JSON.deserializeUntyped(selRecordIds);
        if(values.size()>0){         
            for (Object id : values) {
                recordIds.add(string.valueof(id) );
            }
        }

        
        //system.debug('recordIds:'+recordIds);
        

        list<OpportunityLineItem> newOppLineItems = new list<OpportunityLineItem>();
        list<Opportunity_Financial_Account__c> newOppFinAcc = new list<Opportunity_Financial_Account__c>();
        list<OpportunityContactRole> newOppConRole = new list<OpportunityContactRole>();
        string retOppId;

        for(string recordId:recordIds)
        {
            List<Opportunity> lstOppRecordType = [Select RecordType.Name,RecordType.DeveloperName FROM Opportunity where id = :recordId];
            string recordType = lstOppRecordType[0].RecordType.Name;
            string recordTypeDevName = lstOppRecordType[0].RecordType.DeveloperName;

            //check if a fieldset with recordtype name exists. If not default to 'cloneOpportunity' fieldset
       
             //system.debug('recordType1:' + recordType);

            //if(!fieldSetExists(recordType,'Opportunity'))
            //{
                       
            //}

            if(Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get(recordType).getRecordTypeId() == NULL)
            {
	            recordType = 'cloneOpportunity'; 
            }


            //system.debug('recordType2:' + recordType);

            string soqlQuery = 'SELECT ';

            List<Schema.FieldSetMember> fieldListOpp = readFieldSet(recordTypeDevName,'Opportunity');

            for(Schema.FieldSetMember f : fieldListOpp) {
                if(schema.getGlobalDescribe().get('Opportunity').getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().isAccessible()){
                    soqlQuery = soqlQuery+f.getFieldPath() +','; 
                }
            } 


            List<Schema.FieldSetMember> fieldListOppFinAcc = readFieldSet('Clone_Opportunity_Financial_Account','Opportunity_Financial_Account__c');
            string soqlQueryOppFinAcc = ' (SELECT ';
            for(Schema.FieldSetMember f : fieldListOppFinAcc) {
                if(schema.getGlobalDescribe().get('Opportunity_Financial_Account__c').getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().isAccessible()){
                    soqlQueryOppFinAcc = soqlQueryOppFinAcc+f.getFieldPath() +','; 
                }
            }
            soqlQueryOppFinAcc = soqlQueryOppFinAcc.removeEnd(',');
            soqlQueryOppFinAcc += ' FROM Opportunity_Financial_Accounts__r)';

            //OpportunityLineItems
            /*
            List<Schema.FieldSetMember> fieldListOppLineItems = readFieldSet('Clone_Opportunity_Product','OpportunityLineItem');
            string soqlQueryOppLineItems = ' (SELECT ';
            for(Schema.FieldSetMember f : fieldListOppLineItems) {
                if(schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap().get(f.getFieldPath()).getDescribe().isAccessible()){
                    soqlQueryOppLineItems = soqlQueryOppLineItems+f.getFieldPath() +','; 
                }
            }
            soqlQueryOppLineItems = soqlQueryOppLineItems.removeEnd(',');
            soqlQueryOppLineItems += ' FROM OpportunityLineItems)';
            */

            Map<String, Schema.SObjectType> oppSchemaMap = Schema.getGlobalDescribe();    
            Set <String> oppConRoleFieldMap = oppSchemaMap.get('OpportunityContactRole').getDescribe().fields.getMap().keySet();

            string soqlQueryOppConRole = ' (SELECT  ' ; 
        
            for (String s :oppConRoleFieldMap ){
                if(schema.getGlobalDescribe().get('OpportunityContactRole').getDescribe().fields.getMap().get(s).getDescribe().isAccessible()){
                    soqlQueryOppConRole +=  + s+',';
                }
            }

            soqlQueryOppConRole =  soqlQueryOppConRole.removeEnd(',');

            soqlQueryOppConRole = soqlQueryOppConRole + ' FROM OpportunityContactRoles) ';

            //soqlQuery = soqlQuery + soqlQueryOppFinAcc + ' , ' + soqlQueryOppLineItems + ' , ' + soqlQueryOppConRole;
            soqlQuery = soqlQuery + soqlQueryOppFinAcc + ' , '  + soqlQueryOppConRole;
            soqlQuery += ' FROM Opportunity WHERE ID = \'' + recordId +'\'' ;

            //system.debug('soqlQuery: ' +soqlQuery);

            //Query for the Opportunity record types
            List<RecordType> oppRtypes = [Select Name, Id From RecordType 
                      where sObjectType='Opportunity' and isActive=true];
     
            //Create a map between the Record Type Name and Id for easy retrieval
            Map<String,String> oppRecordTypes = new Map<String,String>{};
            for(RecordType rt: OppRtypes)
                oppRecordTypes.put(rt.Name,rt.Id);

            Opportunity origOpp = Database.query(soqlQuery);
            Opportunity clondedOpp = origOpp.clone(false, false, false, false);
            
            // Setting the Opportunity's IsAClone flag for automation/validation - John C 7/18/19
            clondedOpp.IsAClone__c = true;
                    
            //if(origOpp.RecordTypeId == oppRecordTypes.get('Advisory Sales'))
            //{
                //clondedOpp.StageName = 'Closed'; //Make changes to this after clarification from Client.
                //clondedOpp.CloseDate = ''; //Next Meeting/Action Item Date - This is calculated from Activities
                //clondedOpp.StageName = origOpp.StageName;
            //    system.debug('here:');
            //}
            if (origOpp.RecordTypeId == oppRecordTypes.get('Tax Preparation') || origOpp.RecordTypeId == oppRecordTypes.get('Estate Planning') || origOpp.RecordTypeId == oppRecordTypes.get('Advisory Sales'))
            {
                clondedOpp.StageName = 'New';
            }

                insert clondedOpp ;

                retOppId = clondedOpp.id;

                id oppClonedId = clondedOpp.id;

                //OpportunityLineItems
                /*
                Map<Opportunity,List<OpportunityLineItem>> OpportunityLineItemsMapping = new Map<Opportunity,List<OpportunityLineItem>>();
                OpportunityLineItemsMapping.put(clondedOpp,origOpp.OpportunityLineItems.deepClone(false,false,false));

                for(Opportunity opp :OpportunityLineItemsMapping.keySet()){
                    for(OpportunityLineItem oppLineItem : OpportunityLineItemsMapping.get(opp)){
                        oppLineItem.OpportunityId = opp.Id;
                    }
                }

                if(OpportunityLineItemsMapping.size() >0)
                {
                    newOppLineItems.addall(OpportunityLineItemsMapping.get(clondedOpp));
                    //insert newOppLineItems; //OpportunityLineItemsMapping.values();
                }
                */

                Map<Opportunity,List<Opportunity_Financial_Account__c>> OpportunityFinAccMapping = new Map<Opportunity,List<Opportunity_Financial_Account__c>>();
                OpportunityFinAccMapping.put(clondedOpp,origOpp.Opportunity_Financial_Accounts__r.deepClone(false,false,false));

                for(Opportunity opp :OpportunityFinAccMapping.keySet()){
                    for(Opportunity_Financial_Account__c oppFinAcc : OpportunityFinAccMapping.get(opp)){
                        oppFinAcc.Opportunity__c = opp.Id;
                    }
                }

                if(OpportunityFinAccMapping.size() >0)
                {
                    newOppFinAcc.addall(OpportunityFinAccMapping.get(clondedOpp));
                    //insert newOppFinAcc; //OpportunityLineItemsMapping.values();
                }


                Map<Opportunity,List<OpportunityContactRole>> OpportunityConRoleMapping = new Map<Opportunity,List<OpportunityContactRole>>();
                OpportunityConRoleMapping.put(clondedOpp,origOpp.OpportunityContactRoles.deepClone(false,false,false));

                for(Opportunity opp :OpportunityConRoleMapping.keySet()){
                    for(OpportunityContactRole oppConRole : OpportunityConRoleMapping.get(opp)){
                        oppConRole.OpportunityId = opp.Id;
                    }
                }

                if(OpportunityConRoleMapping.size() >0)
                {
                    newOppConRole.addall(OpportunityConRoleMapping.get(clondedOpp));
                    //insert newOppConRole; //OpportunityLineItemsMapping.values();
                }


                //return clondedOpp.id ;
        }
        
        try{

                if(newOppLineItems.size()>0)
                {
                    //system.debug('newOppLineItems:'+newOppLineItems);
	                insert newOppLineItems;
                } 

                if(newOppFinAcc.size()>0)
                {
                    //system.debug('newOppFinAcc:'+newOppFinAcc);
	                insert newOppFinAcc;
                } 

                if(newOppConRole.size()>0)
                {
                    //system.debug('newOppConRole:'+newOppConRole);
	                insert newOppConRole;
                } 

                return retOppId;

        }catch(Exception e)
        {
            return e.getMessage();
        }

    }


    public static List<Schema.FieldSetMember> readFieldSet(String fieldSetName, String ObjectName)
    {
        //system.debug('fieldSetName:'+fieldSetName);
        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();

        //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));

        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);

        //List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        //system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
        return fieldSetObj.getFields(); 
    }  

    public static boolean fieldSetExists(String fieldSetName, String ObjectName)
    {

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe();
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(ObjectName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
	    Map<string,Schema.FieldSet> mapFieldSetNames = DescribeSObjectResultObj.FieldSets.getMap();
	    boolean exists = mapFieldSetNames.containskey(fieldSetName);
        return exists;
    }
}