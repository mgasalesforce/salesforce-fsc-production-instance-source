// @description Invocable Apex class from Flow (/flow/Search_All_Person_Accounts_Leads_Referrals_Flow) 
// to return All ACR records of selected person account meeting search criteria bypassign sharing model 
// @author Praveen Kanumuri
// @date 7-25-2019

public without sharing class searchAllAccountContactRelations {

    @InvocableMethod(label='searchAllAccountContactRelations' description='searchAllAccountContactRelations')
    public static List<List<AccountContactRelation>> searchAllAccountContactRelations(List<String> lstPersonContactIds) {

       string PersonContactId = lstPersonContactIds[0];
       system.debug('PersonContactId:'+PersonContactId);
       List<FeedItem> feedItemList = new List<FeedItem>();
       
       string  strQueryAcr = 'Select id, ';
       strQueryAcr = strQueryAcr + 'ContactId, ';
       strQueryAcr = strQueryAcr + 'Roles, FinServ__Primary__c, ';
       strQueryAcr = strQueryAcr + 'FinServ__PrimaryGroup__c, ';
       strQueryAcr = strQueryAcr + 'Account.Client_Service_Unit__c, ';
       strQueryAcr = strQueryAcr + 'Account.Client_Associate__c, ';
       strQueryAcr = strQueryAcr + 'Account.Client_Advisor__c, ';
       strQueryAcr = strQueryAcr + 'Contact.Name, ';
       strQueryAcr = strQueryAcr + 'Contact.Phone, ';
       strQueryAcr = strQueryAcr + 'Contact.Account.BillingCity, ';
       strQueryAcr = strQueryAcr + 'Contact.Account.BillingState, ';
       strQueryAcr = strQueryAcr + 'Contact.Account.id, ';
       strQueryAcr = strQueryAcr + 'Account.Name, ';
       strQueryAcr = strQueryAcr + 'Account.id, ';
       strQueryAcr = strQueryAcr + 'Account.Client_Service_Unit__r.Name, ';
       strQueryAcr = strQueryAcr + 'Account.Client_Service_Unit__r.Client_Advisor__r.Name, ';
       strQueryAcr = strQueryAcr + 'Account.Client_Service_Unit__r.Client_Associate__r.Name,';
       strQueryAcr = strQueryAcr + 'HouseholdName__c, ';
       strQueryAcr = strQueryAcr + 'ContactName__c, ';
       strQueryAcr = strQueryAcr + 'ContactPhone__c, ';
       strQueryAcr = strQueryAcr + 'AccountBillingCity__C, ';
       strQueryAcr = strQueryAcr + 'AccountBillingState__C, ';
       strQueryAcr = strQueryAcr + 'ClientAdvisorName__c, ';
       strQueryAcr = strQueryAcr + 'ClientAssociateName__c, ';
       strQueryAcr = strQueryAcr + 'ClientServiceUnitName__c, ';
       strQueryAcr = strQueryAcr + 'PersonAccountId__c, ';
       strQueryAcr = strQueryAcr + 'HouseholdAccountId__c, ';
       strQueryAcr = strQueryAcr + 'CsuBranchName__c, ';
       strQueryAcr = strQueryAcr + 'PersonAccountStatus__c ';               
       strQueryAcr = strQueryAcr + 'FROM AccountContactRelation ';
       system.debug('strQueryAcr:'+strQueryAcr);

       string strQueryWhere = 'where isActive = True and ContactId = \'' + PersonContactId + '\'';

    //system.debug('strQueryAcr:'+strQueryAcr);
    //system.debug('strQueryWhere:'+strQueryWhere);


      List<AccountContactRelation> accountContactDetails = database.query(strQueryAcr+strQueryWhere);

      //insert to Person_Acount_Search_History
      List<Person_Account_Search_History__c> searchHistoryList = new List<Person_Account_Search_History__c>();

        for(AccountContactRelation ad:accountContactDetails) 
        {
            
            Person_Account_Search_History__c personSearchHistory = new Person_Account_Search_History__c();

            personSearchHistory.Searched_by_User__c = UserInfo.getUserId();
            personSearchHistory.Accessed_Details__c = true;
            personSearchHistory.Person_Account_Accessed__c = ad.Contact.Account.id; 
            personSearchHistory.Client_Advisor__c = ad.Account.Client_Advisor__c;
            personSearchHistory.Client_Associate__c = ad.Account.Client_Associate__c;
            personSearchHistory.Client_Service_Unit__c = ad.Account.Client_Service_Unit__c;
            personSearchHistory.Household_Account_Accessed__c = ad.Account.id;
            personSearchHistory.City__c = ad.Contact.Account.BillingCity;
            personSearchHistory.State__c = ad.Contact.Account.BillingState;
            personSearchHistory.Account_Phone__c = ad.Contact.Phone;            
            searchHistoryList.add(personSearchHistory);
        }

        //Insert Account Search History
        if(searchHistoryList.size() > 0)
        {    
            insert searchHistoryList;
        }

        /*for(AccountContactRelation acr : accountContactDetails){
            FeedItem postHousehold = new FeedItem();
                  postHousehold.createdById = UserInfo.getUserId();
                  postHousehold.ParentId = acr.Account.id;
                  postHousehold.Type = 'TextPost';              
            postHousehold.Body = UserInfo.getName() + ' searched for and opened the Account record.';
                  feedItemList.add(postHousehold);            
        }*/


      //Post to Chatter - insert Feed Item        
      /*if(feedItemList.size()>0)
            {
                insert feedItemList;
            } */        

      List<List<AccountContactRelation>> lstAcr = new List<List<AccountContactRelation>>();
      lstAcr.add(accountContactDetails);
      return lstAcr;

    }
}