// @description Lightning Component Action to Box Folder Hierarchy for Household and Institutions
// @author Praveen Kanumuri
// @date 8-05-2019

public without sharing class BoxCreateFolders {
    
    @AuraEnabled
    public static String createFolders(String recordId){
    
            system.debug('recordId:'+recordId);
            


            /*//Use this if passing list of Ids
            List<String> lstRecordIds = new List<String>();
            Object[] values = (Object[])System.JSON.deserializeUntyped(recordIds);
            if(values.size()>0){         
                for (Object id : values) {
                    lstRecordIds.add(string.valueof(id));
                }
            }*/
                    
        
            //List<Account> lstAcc = [Select Name, RecordType.Name,RecordType.DeveloperName FROM Account where id = :lstRecordIds[0]];
            List<Account> lstAcc = [Select Name, RecordType.Name,RecordType.DeveloperName FROM Account where id = :recordId];

            string accName = lstAcc[0].Name;
            

            List<Box_JWT_Authentication__mdt> boxAuthMdt  = [select jwtUsername__c,jwtPrivateKey__c,jwtHostname__c,jwtPublicKeyId__c,
                                                 jwtConnectedAppConsumerKey__c,boxSubType__c,jwtJti__c, HouseholdMasterFolderStructure_FolderId__c,HouseholdRootFolderStructure_FolderId__c 
                                                 from Box_JWT_Authentication__mdt
                                                where label = 'BoxJwtAuthFinal'];    
                                                
             string strParentId =  boxAuthMdt[0].HouseholdRootFolderStructure_FolderId__c; 
             string strMasterFolderStructureFolderId =  boxAuthMdt[0].HouseholdMasterFolderStructure_FolderId__c;                                            
             BoxJwtToken.Configuration config = new BoxJwtToken.Configuration();
             config.jwtUsername = boxAuthMdt[0].jwtUsername__c; 
             config.privateKey = boxAuthMdt[0].jwtPrivateKey__c; 
             config.publicKeyId = boxAuthMdt[0].jwtPublicKeyId__c; 
             config.jwtHostname = boxAuthMdt[0].jwtHostname__c; 
             config.jwtJti = boxAuthMdt[0].jwtJti__c; 
             config.box_sub_type = boxAuthMdt[0].boxSubType__c; 
             config.jwtConnectedAppConsumerKey = boxAuthMdt[0].jwtConnectedAppConsumerKey__c; 
             String strAccessToken = new BoxJwtToken(config).requestaccesstoken();
             system.debug('getaccesstoken:'+strAccessToken);
            

           string newAccountFolderId = ''; 
           string strOutput = '';
           
           //Copy Folder creates parent folder and also copies folder hierarchy from master. So do not need below step
           //string strResultCreateFolder = createFolder(accName,strParentId,strAccessToken);

           /*//Since response JSON has id field of existing Account or New Created Account, we can skip this check below. Grab ID from JSON and use in next step to create hierarchy
           //If folder is created,, type is folder. So Grab new Folder Id. Else type is error, status 40, code 'item_name_in_use'. So get id of existing folder
           if(extractJsonField(strResultCreateFolder,'type')=='folder')
           {
               newAccountFolderId = extractJsonField(strResultCreateFolder,'id');
           } else if(extractJsonField(strResultCreateFolder,'type')=='error') {
               if(extractJsonField(strResultCreateFolder,'code')=='item_name_in_use')
               {
                   newAccountFolderId = extractJsonField(strResultCreateFolder,'id');
               }
           }*/

           
           string strResultCreateFolder = copyFolder(accName,strParentId,strAccessToken);
           
           //If folder is created,, type is folder. So Grab new Folder Id. Else type is error, status 40, code 'item_name_in_use'. So get id of existing folder
           if(extractJsonField(strResultCreateFolder,'type')=='folder')
           {
               newAccountFolderId = extractJsonField(strResultCreateFolder,'id');
               strOutput = 'Folder ' + accName + ' with Folder id ' + newAccountFolderId + ' created'; 
           } else if(extractJsonField(strResultCreateFolder,'type')=='error') {
               if(extractJsonField(strResultCreateFolder,'code')=='item_name_in_use')
               {
                   newAccountFolderId = extractJsonField(strResultCreateFolder,'id');
                   strOutput = 'Folder ' + accName + ' with Folder id ' + newAccountFolderId + ' already exists';                   
               }
           }

            system.debug('strOutput:'+strOutput);

           //Add Tags
            //string strAccountFolderInfoJson = getFolderInfo(newAccountFolderId,strAccessToken);
            //string strSubFolderCount = addTags(strAccountFolderInfoJson, strAccessToken);


           return recordId;
           
        
    }


    @AuraEnabled
    public static String createSubFolders(String recordId){
    
            system.debug('recordId:'+recordId);

            //List<Account> listAccts = [SELECT Id, Name, RecordTypeID, Type, OwnerID, Box_Folder_ID__c, RecordType.Name  FROM Account where id = :recordId];
            List<Account> listAcctsUpdated = new List<Account>();

            Account acct = [SELECT Id, Name, RecordTypeID, Type, OwnerID, Box_Folder_ID__c, RecordType.Name, Box_SubFolders_Created__c FROM Account where id = :recordId];

            Id accountId = acct.id; 
            id ownerId = acct.Ownerid;
            string accName = acct.Name;
            Boolean BoxSubFoldersCreated = acct.Box_SubFolders_Created__c;

            box.Toolkit boxToolkit = new box.Toolkit();
            string strFolderIdByRecordId = boxToolkit.getFolderIdByRecordId(acct.id);
            
            if(Test.isRunningTest()){
                strFolderIdByRecordId = '123456';
            }
            system.debug('strFolderIdByRecordId:'+strFolderIdByRecordId);     

            if(strFolderIdByRecordId==null || strFolderIdByRecordId == '')
            {
                system.debug('No Root Folder');
                //return recordId;

            }               
            else if(BoxSubFoldersCreated==TRUE)
            {
                system.debug('Sub Folders Already Created');
                //return recordId;                    
            }
            else
            {

                string strAccessToken = getAccessToken();                     
          
                string strSubFolderCount = createFoldersAddTags(accName,strFolderIdByRecordId,boxToolkit,strAccessToken);    
         
                box.Toolkit.CollaborationType collabType = box.Toolkit.CollaborationType.EDITOR;
                String collabId = boxToolkit.createCollaborationOnRecord(ownerId, accountId, collabType, false); 
                system.debug('collabId:'+collabId);

                if(collabId == null){
                    system.debug('Box Collaboration was not created because: '+ boxToolkit.mostRecentError);
                    }
            
                boxToolkit.commitChanges();

                if(strSubFolderCount=='7')
                {
                    acct.Box_SubFolders_Created__c = TRUE;
                    update acct;
                }    
            }
        return recordId;
    }


 public static string createFolder(string strFolderName, string strParentFolderId, string strAccesstoken, box.Toolkit boxToolkit)
    {
            string strNewFolderId = boxToolkit.CreateFolder(strFolderName,strParentFolderId,strAccesstoken); 

            system.debug(strFolderName + ' NewFolderId:'+strNewFolderId);

            if(strNewFolderId == null){
                    system.debug('Box Folder ' + strFolderName + ' was not created because: '+ boxToolkit.mostRecentError);
                }
            return strNewFolderId;
               
    }

    public static string createFoldersAddTags(string accname, string strFolderIdByRecordId, box.Toolkit boxToolkit, string strAccessToken)
    {       

            string strClientProfileFolderId = createFolder('Client Profile', strFolderIdByRecordId, strAccesstoken, boxToolkit);  

            string strPaperworkFolderId = createFolder('Paperwork', strFolderIdByRecordId, strAccesstoken, boxToolkit);   
  
            if(strPaperworkFolderId != NULL && strPaperworkFolderId != '')
            {            
                string strTrustPaperworkFolderId = createFolder('Trust Paperwork', strPaperworkFolderId, strAccesstoken, boxToolkit);   
                string strCustodianPaperworkFolderId = createFolder('Custodian Paperwork', strPaperworkFolderId, strAccesstoken, boxToolkit);   

            }

            string strUniqueInvestmentsFolderId = createFolder('Unique Investments', strFolderIdByRecordId, strAccesstoken, boxToolkit);   
            if(strUniqueInvestmentsFolderId != NULL && strUniqueInvestmentsFolderId != '')
            {            
                string strOilAndGasFolderId = createFolder('Oil & Gas', strUniqueInvestmentsFolderId, strAccesstoken, boxToolkit);   
                string strHedgeFundsFolderId = createFolder('Hedge Funds', strUniqueInvestmentsFolderId, strAccesstoken, boxToolkit);   
                string strPromissoryNotesFolderId = createFolder('Paromissory Notes', strUniqueInvestmentsFolderId, strAccesstoken, boxToolkit);   
                string strRealEstateFolderId = createFolder('Real Estate', strUniqueInvestmentsFolderId, strAccesstoken, boxToolkit);   
            }

            string strArchiveFolderId = createFolder('Archive', strFolderIdByRecordId, strAccesstoken, boxToolkit);   
            if(strArchiveFolderId != NULL && strArchiveFolderId != '')
            {
                string strLaserficheFilecopiesFolderId = createFolder('Laserfiche File Copies', strArchiveFolderId, strAccesstoken, boxToolkit);   
                string strRedFileCopiesFolderId = createFolder('Red File Copies', strArchiveFolderId, strAccesstoken, boxToolkit);   
            }

            string strComplianceFolderId = createFolder('Compliance', strFolderIdByRecordId, strAccesstoken, boxToolkit);   

            string strClientCollaborationFolderId = createFolder('Client Collaboration', strFolderIdByRecordId, strAccesstoken, boxToolkit);   

            string strFinancialPlanningFolderId = createFolder('Financial Planning', strFolderIdByRecordId, strAccesstoken, boxToolkit);   
            if(strFinancialPlanningFolderId != NULL && strFinancialPlanningFolderId != '')
            {    
                string strClientDeliverablesFolderId = createFolder('Client Deliverables', strFinancialPlanningFolderId, strAccesstoken, boxToolkit);   
                string strPensionPlanFolderId = createFolder('Pension Plan', strFinancialPlanningFolderId, strAccesstoken, boxToolkit);   
                string strEstatePlanningFolderId = createFolder('Estate Planning', strFinancialPlanningFolderId, strAccesstoken, boxToolkit);   
                string strInsuranceFolderId = createFolder('Insurance', strFinancialPlanningFolderId, strAccesstoken, boxToolkit);   
                string strTaxFolderId = createFolder('Tax', strFinancialPlanningFolderId, strAccesstoken, boxToolkit);   
                string strClientMeetingDataFolderId = createFolder('Client Meeting Data', strFinancialPlanningFolderId, strAccesstoken, boxToolkit);   
            }    

            string strAccountFolderInfoJson = getFolderInfo(strFolderIdByRecordId,strAccessToken);
            string strSubFolderCount = addTags(strAccountFolderInfoJson, strAccessToken);

           //system.debug('strOutput:'+strResultAddTags);
         
         return strSubFolderCount;
      
    }



    public static string copyFoldersAddTags(string accname, string strFolderIdByRecordId)
    {
            string strAccessToken = getAccessToken();            

           string newAccountFolderId = ''; 
           string strOutput = '';           

           string strResultCreateFolder = copyFolder(accName,strFolderIdByRecordId,strAccessToken);
           system.debug('strResultCreateFolder:'+strResultCreateFolder);
           
           //If folder is created,, type is folder. So Grab new Folder Id. Else type is error, status 40, code 'item_name_in_use'. So get id of existing folder
           if(extractJsonField(strResultCreateFolder,'type')=='folder')
           {
               newAccountFolderId = extractJsonField(strResultCreateFolder,'id');
               strOutput = 'Folder ' + accName + ' with Folder id ' + newAccountFolderId + ' created'; 
           } else if(extractJsonField(strResultCreateFolder,'type')=='error') {
               if(extractJsonField(strResultCreateFolder,'code')=='item_name_in_use')
               {
                   newAccountFolderId = extractJsonField(strResultCreateFolder,'id');
                   strOutput = 'Folder ' + accName + ' with Folder id ' + newAccountFolderId + ' already exists';                   
               }
           }

            system.debug('strOutput:'+strOutput);

            string strAccountFolderInfoJson = getFolderInfo(newAccountFolderId,strAccessToken);
            string strSubFolderCount = addTags(strAccountFolderInfoJson, strAccessToken);

           //system.debug('strOutput:'+strResultAddTags);
         
         return strSubFolderCount;
      
    }

    public static string getAccessToken()
    {
                    List<Box_JWT_Authentication__mdt> boxAuthMdt  = [select jwtUsername__c,jwtPrivateKey__c,jwtHostname__c,jwtPublicKeyId__c,
                                                 jwtConnectedAppConsumerKey__c,boxSubType__c,jwtJti__c, HouseholdMasterFolderStructure_FolderId__c,HouseholdRootFolderStructure_FolderId__c 
                                                 from Box_JWT_Authentication__mdt
                                                where label = 'BoxJwtAuthFinal'];    
                                                
             string strParentId =  boxAuthMdt[0].HouseholdRootFolderStructure_FolderId__c; 
             string strMasterFolderStructureFolderId =  boxAuthMdt[0].HouseholdMasterFolderStructure_FolderId__c;                                            
             BoxJwtToken.Configuration config = new BoxJwtToken.Configuration();
             config.jwtUsername = boxAuthMdt[0].jwtUsername__c; 
             config.privateKey = boxAuthMdt[0].jwtPrivateKey__c; 
             config.publicKeyId = boxAuthMdt[0].jwtPublicKeyId__c; 
             config.jwtHostname = boxAuthMdt[0].jwtHostname__c; 
             config.jwtJti = boxAuthMdt[0].jwtJti__c; 
             config.box_sub_type = boxAuthMdt[0].boxSubType__c; 
             config.jwtConnectedAppConsumerKey = boxAuthMdt[0].jwtConnectedAppConsumerKey__c; 
             String strAccessToken = new BoxJwtToken(config).requestaccesstoken();
             system.debug('getaccesstoken:'+strAccessToken);
             return strAccessToken;

    }
    public static string createFolder(string strFolderName, String strParentId, string strAccessToken){
        Http p=new Http();
        HttpRequest request =new HttpRequest();
        request.setEndPoint('https://api.box.com/2.0/folders');
        request.setMethod('POST');
        String body='{"name":"' + strFolderName + '", "parent": {"id": "' + strParentId + '"}}';
        request.setBody(body);
        request.setHeader('Authorization','Bearer '+strAccessToken);
        HttpResponse response=p.send(request);
        string result=response.getBody();
        return result;
    }
    
      public static string copyFolder(string strFolderName,String strParentId,string strAccessToken){
        

            List<Box_JWT_Authentication__mdt> boxAuthMdt  = [select jwtUsername__c,jwtPrivateKey__c,jwtHostname__c,jwtPublicKeyId__c,
                                                 jwtConnectedAppConsumerKey__c,boxSubType__c,jwtJti__c, HouseholdMasterFolderStructure_FolderId__c,HouseholdRootFolderStructure_FolderId__c 
                                                 from Box_JWT_Authentication__mdt
                                                where label = 'BoxJwtAuthFinal'];    
                                                
             //string strParentId =  boxAuthMdt[0].HouseholdRootFolderStructure_FolderId__c; 
             string strMasterFolderStructureFolderId =  boxAuthMdt[0].HouseholdMasterFolderStructure_FolderId__c;  

        string endpointurl = 'https://api.box.com/2.0/folders/'+strMasterFolderStructureFolderId+'/copy';
        system.debug('copyFolder endpointurl:'+endpointurl);
        Http p=new Http();
        HttpRequest request =new HttpRequest();
        request.setEndPoint(endpointurl);
        request.setMethod('POST');
        String body='{"name":"' + strFolderName + '", "parent": {"id": "' + strParentId + '"}}';
        request.setBody(body);
        request.setHeader('Authorization','Bearer '+strAccessToken);
        HttpResponse response=p.send(request);
        string result=response.getBody();
        return result;
    }

      public static string addTagsToFolder(String stFolderId,string strAccessToken,list<String> lstTags){      
        string endpointurl = 'https://api.box.com/2.0/folders/'+stFolderId+'?fields=tags';
        //system.debug('addTagsToFolder endpointurl:'+endpointurl);
        Http p=new Http();
        HttpRequest request =new HttpRequest();
        request.setEndPoint(endpointurl);
        request.setMethod('PUT');
        String body='{"tags":"' + lstTags + '"}';
        request.setBody(body);
        request.setHeader('Authorization','Bearer '+strAccessToken);
        HttpResponse response=p.send(request);
        string result=response.getBody();
        return result;
    }

  public static string addTags(String strAccountFolderInfoJson,string strAccessToken)
  {      
            string archiveFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Archive');
            List<String> lstTagsArchiveFolder = new List<String>();
            lstTagsArchiveFolder.add('Old Red or Laserfiche files');
            string strResultAddTagsArchiveFolder = addTagsToFolder(archiveFolderId,strAccessToken, lstTagsArchiveFolder);

            string clientCollaboratoinFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Client Collaboration');
            List<String> lstTagsClientCollaboratoinFolder = new List<String>();
            lstTagsClientCollaboratoinFolder.add('Secure way to share data with clients');
            string strResultAddTagsClientCollaboratoinFolder = addTagsToFolder(clientCollaboratoinFolderId,strAccessToken, lstTagsClientCollaboratoinFolder);

            string clientProfileFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Client Profile');
            List<String> lstTagsClientProfileFolder = new List<String>();
	        lstTagsClientProfileFolder.add('Client Info Form');
			lstTagsClientProfileFolder.add('Personal Client Data Documents');			
			lstTagsClientProfileFolder.add('Photos');
            string strResultAddTagsClientProfileFolder = addTagsToFolder(clientProfileFolderId,strAccessToken, lstTagsClientProfileFolder);

            string complianceFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Compliance');
            List<String> lstTagsComplianceFolder = new List<String>();
			lstTagsComplianceFolder.add('IPGs');
			lstTagsComplianceFolder.add('Ominbus Agreement');
			lstTagsComplianceFolder.add('RTQ');
			lstTagsComplianceFolder.add('SIS');
            string strResultAddTagsComplianceFolder = addTagsToFolder(complianceFolderId,strAccessToken, lstTagsComplianceFolder);

            string financialPlanningFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Financial Planning');
            List<String> lstTagsFinancialPlanningFolder = new List<String>();
 			lstTagsFinancialPlanningFolder.add('Modules');
			lstTagsFinancialPlanningFolder.add('Client Deliverables');
			lstTagsFinancialPlanningFolder.add('Estate Planning');
			lstTagsFinancialPlanningFolder.add('Tax');
			lstTagsFinancialPlanningFolder.add('Pension');
			lstTagsFinancialPlanningFolder.add('Insurance');        
            string strResultAddTagsFinancialPlanningFolder = addTagsToFolder(financialPlanningFolderId,strAccessToken, lstTagsFinancialPlanningFolder);

            string paperworkFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Paperwork');
            List<String> lstTagsPaperworkFolder = new List<String>();
            lstTagsPaperworkFolder.add('Custodian');
			lstTagsPaperworkFolder.add('NATC/Trust & Other Operational Paperwork');
            string strResultAddTagsPaperworkFolder = addTagsToFolder(paperworkFolderId,strAccessToken, lstTagsPaperworkFolder);

            string uniqueInvestmentsFolderId = extractFolderIdFromJson(strAccountFolderInfoJson,'Unique Investments');
            List<String> lstTagsUniqueInvestmentsFolder = new List<String>();
			lstTagsUniqueInvestmentsFolder.add('Hedge Funds');
			lstTagsUniqueInvestmentsFolder.add('Oil & Gas');
			lstTagsUniqueInvestmentsFolder.add('Promissory Notes');
			lstTagsUniqueInvestmentsFolder.add('Real Estate');            
            string strResultAddTagsUniqueInvestmentsFolder = addTagsToFolder(uniqueInvestmentsFolderId,strAccessToken, lstTagsUniqueInvestmentsFolder);
            
            //Subfolder Tags
            string strFinancialPlanningFolderInfoJson = getFolderInfo(financialPlanningFolderId,strAccessToken);
            string clientMeetingDataFolderId = extractFolderIdFromJson(strFinancialPlanningFolderInfoJson,'Client Meeting Data');
            string clientDeliverablesFolderId = extractFolderIdFromJson(strFinancialPlanningFolderInfoJson,'Client Deliverables');

            List<String> lstTagsclientMeetingDataFolder = new List<String>();
			lstTagsclientMeetingDataFolder.add('Documents received to prepare for meetings');     
            string strResultAddTagsClientMeetingDataFolder = addTagsToFolder(clientMeetingDataFolderId,strAccessToken, lstTagsclientMeetingDataFolder);

            List<String> lstTagsclientDeliverablesFolder = new List<String>();
			lstTagsclientDeliverablesFolder.add('Documents sent after planning meetings');     
            string strResultAddTagsClientDeliverablesFolder = addTagsToFolder(clientDeliverablesFolderId,strAccessToken, lstTagsclientDeliverablesFolder);

            string strSubFolderCount = extractSubFolderCountFromJson(strAccountFolderInfoJson);

            return strSubFolderCount;

    }

      public static string getFolderInfo(String stFolderId,string strAccessToken){      
        string endpointurl = 'https://api.box.com/2.0/folders/'+stFolderId;
        //system.debug('getFolderInfo endpointurl:'+endpointurl);
        Http p=new Http();
        HttpRequest request =new HttpRequest();
        request.setEndPoint(endpointurl);
        request.setMethod('GET');
        request.setHeader('Authorization','Bearer '+strAccessToken);
        HttpResponse response=p.send(request);
        string result=response.getBody();
        return result;
    }

    //private class BoxException extends Exception {
    //}
      
    public static String extractJsonField(String body, String field) {         
        JSONParser parser = JSON.createParser(body);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == field) {
                parser.nextToken();
                return parser.getText();
            }
        }
        return 'Not Found';
        //throw new BoxException(field + ' not found in response ' + body);
    }    


 public static String extractFolderIdFromJson(String body, String field) {         
        string folderId = '';
        JSONParser parser = JSON.createParser(body);
        while (parser.nextToken() != null) {            
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == 'id') {
                parser.nextToken();
                folderId = parser.getText(); 
                //system.debug('folderId:'+folderId);               
            }
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == 'name') {
                        parser.nextToken();
                        if(parser.getText()==field) {
                            return folderId;                
                }           
            }        
        }
        return '000';
        //throw new BoxException(field + ' not found in response ' + body);
    }    

 public static String extractSubFolderCountFromJson(String body) {         
        string strSubFolderCount = '';
        JSONParser parser = JSON.createParser(body);
        while (parser.nextToken() != null) {            
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == 'item_collection') {
                parser.nextToken(); //{
                parser.nextToken(); //total_count
                parser.nextToken();
                strSubFolderCount = parser.getText(); 
                system.debug('strSubFolderCount:'+strSubFolderCount);    
                return strSubFolderCount;           
            }
        }
        return '000';
        //throw new BoxException(field + ' not found in response ' + body);
    } 

   public static string createFolderAssociation(Account acct, box.Toolkit boxkit){
        String existingSFRecordAssociated = boxKit.getRecordIdByFolderId(acct.Box_Folder_ID__c);
        String existingBoxFolderAssociated = boxKit.getFolderIdByRecordId(acct.Id);
        String collabId;

        SYSTEM.DEBUG('another folder is associated with this Salesforce Record: ' + existingSFRecordAssociated);

        SYSTEM.DEBUG('another Salesforce Object is associated with this Box Folder: ' + existingBoxFolderAssociated);

        if(existingSFRecordAssociated == null && existingBoxFolderAssociated == null)
        {
            if(!Test.isRunningTest())
            {
                box__FRUP__c frup = boxKit.createFolderAssociation(acct.Id, acct.Box_Folder_ID__c);
                if(frup == null)
                {
                    system.debug('box__FRUP__c was not created because: '+boxKit.mostRecentError);
                }
                
                box.Toolkit.CollaborationType collabType = box.Toolkit.CollaborationType.EDITOR;
                collabId = boxKit.createCollaborationOnRecord(acct.OwnerID, acct.Id, collabType, false); 
                
                if(collabId == null)
                {
                    system.debug('Box Collaboration was not created because: '+ boxKit.mostRecentError);
                }
            } 
        }
        return collabId;
    }

}