/**
 * @File Name          : Tax_Timecard_Generator_Controller.apxc
 * @Description        : 
 * @Author             : soobin.kittredge@merceradvisors.com
 * @Group              : 
 * @Last Modified By   : soobin.kittredge@merceradvisors.com
 * @Last Modified On   : 8/22/2019, 4:20:47 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/22/2019, 9:48:29 AM   soobin.kittredge@merceradvisors.com     Initial Version
**/
public class Tax_Timecard_Generator_Controller {
    public Boolean stepOpp		 			{get; set;}
    public Boolean stepTc		 			{get; set;}
    public Boolean noOppSelected	 		{get; set;}
    public Boolean timecardProcessed		{get; set;}
    public Boolean timecardProcessed_error	{get; set;}
    
    public String oppHouseholdSearch		{get; set;}
    public String oppType					{get; set;}
    public String oppTypeLabel				{get; set;}
    
    public List<Opportunity> allOpps			{get; set;}
    public Set<Opportunity> selectedOpps		{get; set;}
    public List<Opportunity> selectedOppsList	{get; set;}
    
    public List<Service_Timecard__c> insertTimecards		{get; set;}
    public List<Service_Timecard__c> generatedTimecards	{get; set;}
    public String servPerformed									{get; set;}
    public List<SelectOption> ServPerformedList 				{get; set;}
    public Decimal hours_all									{get; set;}
    public Date todaysDate										{get; set;}
    
    public String dmlResult				{get; set;}
    public Integer successDml			{get; set;}
    public Integer errorDml				{get; set;}
    
    
    
    
    
    public Tax_Timecard_Generator_Controller(){
        init();
    }
    
    public void init(){
        
        oppHouseholdSearch = '';
        selectServPerformed();
        opptypeTax();
        stepOpp = true;
        stepTc = false;
        noOppSelected = true;
        timecardProcessed = false;
        timecardProcessed_error = false;
        
        hours_all = 0.00;
        servPerformed = '--None--';
        todaysDate = System.today();
        
        selectedOpps = new Set<Opportunity>();
        
        successDml = 0;
        errorDml = 0;
    }
    
    // Actions ==================================================================
    // Step 1 ---- opp dev name
    public void opptypeTax(){
        oppType = 'Tax_Preparation';
        oppTypeLabel = 'FWS – Tax Planning & Preparation';
        getOppList();
    } 
    public void opptypeEP(){
        oppType = 'Estate_Planning'; 
        oppTypeLabel = 'FWS – Estate Planning';
        getOppList();
    } 
    
    // Step 2 ---- Select opp 
    public void getOppList(){
        String oppSoqlQuery = 'SELECT id, name, RecordType.DeveloperName, Accountid, Account.Name, Return_Type__c, Opportunity_Nickname__c FROM Opportunity WHERE isClosed = FALSE AND IsWon = FALSE AND RecordType.DeveloperName = \''+ oppType + '\' AND Account.Name Like \'%'+ oppHouseholdSearch +'%\' LIMIT 100';
        allOpps = database.query(oppSoqlQuery);
        getOpportunities();
    }
    
    public List<sOpportunity> soppList {get; set;}
    public void getOpportunities(){
        soppList = new List<sOpportunity>();
        if(allOpps != NULL){
            for (Opportunity o : allOpps){
                soppList.add(new sOpportunity(o));
            }
        }
    }
    
    public PageReference addSelectedOpp(){//selectedOpps
        for(sOpportunity sOpp : soppList){
            if(sOpp.selected == true){
                selectedOpps.add(sOpp.opp);
            }
        }
        System.debug('Selected Opportunities----');
        for(Opportunity opp: selectedOpps){
            System.debug(opp);
        }
        //soppList = null; //reset sOpportunity list
        
        selectedOppsList = new List<Opportunity>(selectedOpps);
        
        //create new IC with selected opp
        //generateTimecardRec();
        noOppSelected = false;
        return null;
    }
    
    public class sOpportunity{
        public Opportunity opp 		{get; set;}
        public Boolean selected 	{get; set;}
        
        public sOpportunity(Opportunity o){
            opp = o;
            selected = false;
        }
    }
    
    // Step 3 
    
    public List<SelectOption> selectServPerformed() {
        ServPerformedList = new List<SelectOption>();
        Schema.DescribeFieldResult describeResult = Service_Timecard__c.Service_Performed__c.getDescribe();
        List<Schema.PicklistEntry> serviceList = describeResult.getPicklistValues();
        
        ServPerformedList.add(new selectOption('','--None--'));
        for(Schema.PicklistEntry serv : serviceList){
            if(serv.isActive()){
                ServPerformedList.add(new SelectOption(serv.getValue(), serv.getLabel()));
            }
        }
        
        return ServPerformedList;
    }
    
    public PageReference applyAllTC(){
        applyHourstoAllTC();
        applyServtoAllTC();
        return null;
    }
    
    public PageReference applyHourstoAllTC(){
        for(Service_Timecard__c tc : insertTimecards){
            tc.Hours_Worked__c = hours_all;
        }
        return null;
    }
    
    public PageReference applyServtoAllTC(){
        for(Service_Timecard__c tc : insertTimecards){
            tc.Service_Performed__c = servPerformed;
        }
        return null;
    }
    
    public void initTcCreation(){
        stepOpp = false;
        stepTc = true;
        
        insertTimecards = new List<Service_Timecard__c>();
        
        for(Opportunity o : selectedOppsList){
            Service_Timecard__c newTc = new Service_Timecard__c(Opportunity__c = o.id,
                                                                          HouseholdName_text__c = o.Account.Name,
                                                                          User__c = UserInfo.getUserId(),
                                                                          Hours_Worked__c = hours_all,
                                                                          Service_Performed__c = servPerformed,
                                                                          Date_of_Service__c = todaysDate
                                                                         );
            insertTimecards.add(newTc);
        }
    }
    
    public void createTimecard(){
        dmlResult = '';
        generatedTimecards = new List<Service_Timecard__c>();
        Set<Id> generatedTcIds = new Set<Id>();
        
        Database.SaveResult[] dmlResDB = Database.insert(insertTimecards,false);   
        
        for(Database.SaveResult sr : dmlResDB){
            stepTc = false;
            timecardProcessed = true;
            
            if(sr.isSuccess()){
                dmlResult = 'Success!';
                System.debug(dmlResult);
                generatedTcIds.add(sr.getId());
                successDml ++;
            } else {
                dmlResult = 'Error: ' + sr.getErrors();
                System.debug(dmlResult);
                errorDml ++;
                timecardProcessed = false;
                timecardProcessed_error = true;
            }
            
            If(timecardProcessed){
                generatedTimecards = [SELECT Id, Name, Opportunity__c, Household_Name__c, User__c, Price_of_Service__c
                                      FROM Service_Timecard__c WHERE Id IN :generatedTcIds];
            }
        }
    }
    
    // Other methods -------
}