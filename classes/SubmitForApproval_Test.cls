@isTest
public class SubmitForApproval_Test {
    @testSetup static void setup(){
        Profile csuProfile = [SELECT Id FROM Profile WHERE Name='MA CSU']; 
        User advUser = new User(Alias = 'adv', Email='adv@testorg.com', 
                            EmailEncodingKey='UTF-8', FirstName='Testx', LastName='Advisor', LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = csuProfile.Id, 
                            TimeZoneSidKey='America/Los_Angeles', UserName='adv@testorg.com');
        insert advUser;
        Branch__c branch = new Branch__c(
            Name = 'Denver',
            Phone_Number__c = '303-555-7845',
            Fax_Number__c = '303-555-7809',
            Street_Address__c = '1700 Grant Street',
            City__c = 'Denver',
            State__c = 'CO',
            Zip_Code__c = '80238',
            Country__c = 'United States',
            Status__c = 'Active'
        );
        insert branch;
        
        Client_Service_Unit__c adv = new Client_Service_Unit__c(
            Name = 'Advisor 64e664e64',
            Client_Advisor__c = advUser.id,
            Branch__c = branch.id,
            City__c = 'Denver',
            Status__c = 'Active',
            Type__c = 'Branch'
        );
        insert adv;
        Account householdAcc = new Account(
            Name = 'Test Household',
            RecordTypeId = '0121U0000012n6fQAA',
            FinServ__IndividualType__c = 'Group',
            FinServ__Status__c = 'Active',
            Client_Service_Unit__c = adv.id
        );
        insert householdAcc;
        
        Price_Modification_Request__c price_modification_request = new Price_Modification_Request__c(
            Related_Account__c = householdAcc.id,
            RecordTypeId = '0121U000000GF7UQAW',
            All_Financial_Accounts__c = 'Yes',
            Apply_Foundation_Fee_Schedule__c = false,
            Billing_Status__c = 'Billing Not Implemented',
            Client_Status__c = 'Prospect',
            Discount_Reason__c = 'Cash account',
            Discount_Type__c = 'Alternative Schedule A: 10bps off initial tier',
            Estate_Planning_Included__c = 'No',
            IsDeleted__c = false,
            Referral_Source__c = 'Digital Lead',
            Request_Initiation__c = 'Client Request',
            Standard_Fee_Schedule__c = false,
            Status__c = 'Not Submitted',
            Submitted_Status__c = 'Not Submitted',
            Tax_Preparation_Included__c = 'No',
            Timeframe__c='Permanent',
            X0_1MM__c = 1.0,
            X10MM__c = 0.5,
            X1MM_2MM__c = 1.0,
            X2MM_3MM__c = 0.9,
            X3MM_4MM__c = 0.9,
            X4MM_5MM__c = 0.9,
            X5MM_10MM__c = 0.75);
        insert price_modification_request;
        
    }
    @isTest static void testSubmitforApproval(){
        Test.startTest();
        price_modification_request__c price_modification_request = [SELECT ID from price_modification_request__c Limit 1];
        SubmitForApproval_Controller.submitForApproval(price_modification_request.id, 'TestComment');
        Test.stopTest();
    }
    
}