/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class TwilioIterator {
    global TwilioIterator() {

    }
global class AccountIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioAccount> {
    global AccountIterator(FSTR.TwilioAccountList twlist) {

    }
    global FSTR.TwilioAccount next() {
        return null;
    }
}
global class ApplicationIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioApplication> {
    global ApplicationIterator(FSTR.TwilioApplicationList twlist) {

    }
    global FSTR.TwilioApplication next() {
        return null;
    }
}
global class AuthorizedConnectAppIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioAuthorizedConnectApp> {
    global AuthorizedConnectAppIterator(FSTR.TwilioAuthorizedConnectAppList twlist) {

    }
    global FSTR.TwilioAuthorizedConnectApp next() {
        return null;
    }
}
global class AvailablePhoneNumberIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioAvailablePhoneNumber> {
    global AvailablePhoneNumberIterator(FSTR.TwilioAvailablePhoneNumberList twlist) {

    }
    global FSTR.TwilioAvailablePhoneNumber next() {
        return null;
    }
}
global class CallIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioCall> {
    global CallIterator(FSTR.TwilioCallList twlist) {

    }
    global FSTR.TwilioCall next() {
        return null;
    }
}
global class ConferenceIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioConference> {
    global ConferenceIterator(FSTR.TwilioConferenceList twlist) {

    }
    global FSTR.TwilioConference next() {
        return null;
    }
}
global class ConnectAppIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioConnectApp> {
    global ConnectAppIterator(FSTR.TwilioConnectAppList twlist) {

    }
    global FSTR.TwilioConnectApp next() {
        return null;
    }
}
global class IncomingPhoneNumberIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioIncomingPhoneNumber> {
    global IncomingPhoneNumberIterator(FSTR.TwilioIncomingPhoneNumberList twlist) {

    }
    global FSTR.TwilioIncomingPhoneNumber next() {
        return null;
    }
}
global class MediaIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioMedia> {
    global MediaIterator(FSTR.TwilioMediaList twlist) {

    }
    global FSTR.TwilioMedia next() {
        return null;
    }
}
global class MemberIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioMember> {
    global MemberIterator(FSTR.TwilioMemberList twlist) {

    }
    global FSTR.TwilioMember next() {
        return null;
    }
}
global class MessageIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioMessage> {
    global MessageIterator(FSTR.TwilioMessageList twlist) {

    }
    global FSTR.TwilioMessage next() {
        return null;
    }
}
global class NotificationIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioNotification> {
    global NotificationIterator(FSTR.TwilioNotificationList twlist) {

    }
    global FSTR.TwilioNotification next() {
        return null;
    }
}
global abstract class ObjectIteratorSupport {
    global Boolean hasNext() {
        return null;
    }
}
global class OutgoingCallerIdIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioOutgoingCallerId> {
    global OutgoingCallerIdIterator(FSTR.TwilioOutgoingCallerIdList twlist) {

    }
    global FSTR.TwilioOutgoingCallerId next() {
        return null;
    }
}
global class ParticipantIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioParticipant> {
    global ParticipantIterator(FSTR.TwilioParticipantList twlist) {

    }
    global FSTR.TwilioParticipant next() {
        return null;
    }
}
global class QueueIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioQueue> {
    global QueueIterator(FSTR.TwilioQueueList twlist) {

    }
    global FSTR.TwilioQueue next() {
        return null;
    }
}
global class RecordingIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioRecording> {
    global RecordingIterator(FSTR.TwilioRecordingList twlist) {

    }
    global FSTR.TwilioRecording next() {
        return null;
    }
}
global class ShortCodeIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioShortCode> {
    global ShortCodeIterator(FSTR.TwilioShortCodeList twlist) {

    }
    global FSTR.TwilioShortCode next() {
        return null;
    }
}
global class SmsIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioSms> {
    global SmsIterator(FSTR.TwilioSmsList twlist) {

    }
    global FSTR.TwilioSms next() {
        return null;
    }
}
global class TranscriptionIterator extends FSTR.TwilioIterator.ObjectIteratorSupport implements System.Iterator<FSTR.TwilioTranscription> {
    global TranscriptionIterator(FSTR.TwilioTranscriptionList twlist) {

    }
    global FSTR.TwilioTranscription next() {
        return null;
    }
}
global class TwilioUnsupportedOperationException extends Exception {
}
}
