//
// Generated by JSON2Apex http://json2apex.herokuapp.com/
//

public class EnvApi_Client {

	public String clientHandle;
	public String advisorHandle;
	public List<FamilyMembers> familyMembers;
	public String clientType;
	public String familyName;
	public List<Program> program;
	public Double totalMarketValue;
	public String repCodeHandle;
	public AdvisorDTO advisorDTO;
	public EstimatedNetWorth estimatedNetWorth;
	public EstimatedNetWorth investableAssets;
	public EstimatedNetWorth annualIncome;
	public EstimatedNetWorth estimatedAnnualLiabilities;
	public EstimatedNetWorth estimatedAnnualExpenses;
	public Boolean logixClient;
	public Boolean digitalAdviceClient;
	public Boolean yodleeFastlinkClient;
	public String dateCreated;
	public String lastReviewDate;
	public String nextReviewDate;
	public String salesforcePartnerId;
	public String currencyId;
	public Boolean sampleClient;
	public String externalCustomerCode;
	public Integer marketIndexId;
	public Integer marketIndexScope;
	public String reportDeliveryMethod;
	public Boolean hideSecondaryBenchmark;
	public Boolean excludeFromStaleClientList;

	public class FamilyMembers {
		public String memberHandle;
		public String clientHandle;
		public Integer memberType;
		public Boolean isEntity;
		public String firstName;
		public String lastName;
		public String middleName;
		public String salutation;
		public String birthDate;
		public Double retirementAge;
		public Integer retirementMonth;
		public Double lifeExpectancy;
		public Integer gender;
		public String suffix;
		public Integer maritalStatus;
		public Integer dependents;
		public Integer employmentStatus;
		public String occupation;
		public String incomeSource;
		public EstimatedNetWorth totalIncome;
		public String employerName;
		public Integer yearsEmployed;
		public Integer citizenshipCountryId;
		public Integer taxResidenceCountryId;
		public String salesforcePartnerId;
		public String ssn;
		public Integer investorClassification;
		public Boolean taxationFlag;
		public Double stateTaxRate;
		public Double federalTaxRate;
		public String userName;
		public Boolean onlineAccess;
		public Integer identificationType;
		public String otherIdentificationType;
		public String idNumber;
		public Integer idIssueState;
		public String idIssueDate;
		public String idExpiryDate;
		public String trustEstablishedDate;
		public Integer eDeliveryEnabled;
		public Integer eDeliveryOptedByClient;
		public Integer llcTaxClassification;
		public String comment;
		public List<MemberAddress> memberAddress;
		public Integer ssntype;
	}

	public class Program {
	}

	public class AdvisorDTO {
		public String advisorHandle;
		public String firstName;
		public String lastName;
		public String advisorContactNumber1;
		public String advisorContactNumber2;
	}

	public class MemberAddress {
		public String contactName;
		public String addressLine1;
		public String addressLine2;
		public String city;
		public String state;
		public String zipCode;
		public String country;
		public String email;
		public String businessPhone;
		public String homePhone;
		public String otherPhone;
		public String fax;
		public Integer addressType;
		public Integer contactType;
		public Boolean useAsMailingAddress;
	}

	public class EstimatedNetWorth {
		public String currencySecurityHandle;
		public String currencyCode;
		public Double Amount;
	}

	
	public static EnvApi_Client parse(String json) {
		return (EnvApi_Client) System.JSON.deserialize(json, EnvApi_Client.class);
	}
}