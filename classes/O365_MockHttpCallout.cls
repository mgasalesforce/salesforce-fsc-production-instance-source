@isTest

global class O365_MockHttpCallout implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
        
		// API call return
		res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        res.setBody('{"@odata.context":"https://graph.microsoft.com/v1.0/$metadata#users(EPLoganBaker%40merceradvisors.com)/calendarView(subject,start,end,showAs)","value":[{"id":"AAMkAGMwYmZjM2FiLTY5NTYtNDdiYi1hMGZhLTUwNzU0NTk4MjQzOQBGAAAAAADcvEwJxM7UT4eJWUcfSH1yBwBP85Z5aKiUQ4ywQ76Qmv7pAAAAAAENAAAwStZ7wtqeTJfrTsG8vTpwAAG4dypoAAA=","subject":"EP-IM - Lisa Siddall","showAs":"busy","start":{"dateTime":"2018-08-27T08:00:00.0000000","timeZone":"America/Denver"},"end":{"dateTime":"2018-08-27T10:00:00.0000000","timeZone":"America/Denver"}}]}');   
       
        return res;
		
    }
}