public without sharing class controller_ContactSearch
{
    public List<Contact> contacts { get; set; }
    public String name { get; set; }
    public String mailingstate { get; set; }

    public controller_ContactSearch()
    {
        contacts = new List<Contact>();
    }

    public PageReference searchContacts()
    {
        string likeName = '%'+name+'%';
        string likeMailingState = '%'+MailingState+'%';
        contacts = [select Id
                          ,Name
                          ,MailingState 
                     from Contact 
                     where Name like :likeName or MailingState like :likeMailingState];
                     //where Name like '%:name%'];
                    //where Name = :name];
        return null;
    }
}