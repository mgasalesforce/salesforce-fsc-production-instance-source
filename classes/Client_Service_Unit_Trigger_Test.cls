@isTest
public class Client_Service_Unit_Trigger_Test {
    @isTest static void test_CSU_Trigger(){
        //create Branch, CSU, Adv/Asso user
        Branch__c testB = new Branch__c(Name='test Branch');
        insert testB;
        
        Profile csuProfile = [SELECT Id FROM Profile WHERE Name='MA CSU']; 
        
        List<Client_Service_Unit__c> csuList = new List<Client_Service_Unit__c>();
        List<User> csuMembers = new List<User>();
        Integer x = 5; //test size
        
        for(Integer i=0;i<x;i++){
            
            User adv = new User(Alias = 'adv'+i, Email='adv'+i+'@testorg.com', 
                              EmailEncodingKey='UTF-8', FirstName='Testx', LastName='Advisor'+i, LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = csuProfile.Id, 
                              TimeZoneSidKey='America/Los_Angeles', UserName='adv'+i+'@testorg.com');
            
            User asso = new User(Alias = 'asso'+i, Email='asso'+i+'@testorg.com', 
                              EmailEncodingKey='UTF-8', FirstName='Testx', LastName='Associate'+i, LanguageLocaleKey='en_US', 
                              LocaleSidKey='en_US', ProfileId = csuProfile.Id, 
                              TimeZoneSidKey='America/Los_Angeles', UserName='asso'+i+'@testorg.com');
            
            csuMembers.add(adv);
            csuMembers.add(asso);
        }
        insert csuMembers;
        List<User> csuMembersInserted = [SELECT Id,username,name FROM User WHERE username like '%@testorg.com' ORDER BY name];
        System.debug('-------------- CSU Inserted: '+csuMembersInserted.size());
        System.debug('-------------- create CSU');
        for(Integer i=0;i<x;i++){
            
            Client_Service_Unit__c csu = new Client_Service_Unit__c(Name=csuMembersInserted[i].name, Branch__c=testB.id, Status__c = 'Active',
                                                                    Client_Advisor__c=csuMembersInserted[i].id, Client_Associate__c=csuMembersInserted[5+i].id);
            csuList.add(csu);
            
            System.debug('**** Adv: '+csuMembersInserted[i].name);
            System.debug('**** Asso: '+csuMembersInserted[x+i].name);
            System.debug('**** CSU : '+csu.name);
        }
        
        Test.startTest();
        insert csuList;
        
        for(Integer i=0; i<x-1 ; i++){
            csuList[i].Client_Associate__c = csuList[i+1].Client_Associate__c;
        }
        update csuList;
        Delete csuList;
        Undelete csuList;
        
        for(Integer i=0; i<x ; i++){
            csuList[i].Status__c='Inactive';
        }
        update csuList;
        
        for(Integer i=0; i<x ; i++){
            csuList[i].Status__c='Active';
        }
        update csuList;
        
        for(Integer i=0; i<x ; i++){
            csuList[i].Client_Associate__c = NULL;
        }
        update csuList;
        
        Test.stopTest();
        
        List<Group> csuGroups = [SELECT id FROM Group WHERE Name like 'CSU Testx Advisor%'];
        System.assertEquals(x, csuGroups.size());
    }
}