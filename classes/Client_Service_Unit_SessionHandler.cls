/**
 * @File Name          : Client_Service_Unit_SessionHandler.apxc
 * @Description        : Trigger Session handler class for Client Service Unit object to create relavant CSU public group/queue - handles future methods for setup object DML
 * @Author             : soobin.kittredge@merceradvisors.com
 * @Group              : Mercer Advisors
 * @Last Modified By   : soobin.kittredge@merceradvisors.com
 * @Last Modified On   : 8/2/2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author                    Modification
 *==============================================================================
 * 1.0        8/2/2019    soobin.kittredge@merceradvisors.com     Initial draft
**/

public class Client_Service_Unit_SessionHandler {
    @future
    public static void createQueueSobj(Set<id> groupIds) {
        List<QueueSobject> newQueueSobject = new List<QueueSobject>();
        String clipoff;
        for (Id queueId : groupIds) {
            newQueueSobject.add(new QueueSObject(SobjectType='Case',QueueId=queueId));
        }
        //---------system.debug('** New Queue SObj: '+newQueueSobject);
        try{
            insert newQueueSobject;
        }
        catch(exception e){
            //---------system.debug('------ Exception: '+e);
        }   
    }  
    
    @future
    public static void groupMemberDml(Set<id> groupIds, Set<Id> csuIds){
        //---------System.debug('------- Group size: '+groupids.size());
        //---------System.debug('------- CSU size: '+csuIds.size());
        
        List<Group> csuGroup = [SELECT Name, id FROM Group WHERE Id in :groupIds];
        List<Client_Service_Unit__c> csu2Process = [SELECT Id, Name, Client_Advisor__c, Client_Associate__c FROM Client_Service_Unit__c WHERE Id In :csuIds];
        
        Map<String, Group> csuGroupMap = new Map<String, Group>();
        for(Group csuGroup2Map: csuGroup){
            csuGroupMap.put(csuGroup2Map.Name,csuGroup2Map);
            //---------System.debug('Group Key: '+csuGroup2Map.Name);
        }       
        
        List<GroupMember> newGms = new List<GroupMember>();
        
        for(Client_Service_Unit__c csu: csu2Process){
            Group g = csuGroupMap.get('CSU '+csu.Name);
            //---------System.debug('**** Group name: CSU '+csu.Name);
            //---------System.debug('**** Group name: '+g.Name);
            if(g <> null){
                if(csu.Client_Advisor__c <> NULL){
                    //---------System.debug('**** Advisor: '+csu.Client_Advisor__c);
                    GroupMember gmAdvisor = new GroupMember(GroupId = g.Id, UserOrGroupId = csu.Client_Advisor__c);
                    newGms.add(gmAdvisor);
                }
                if(csu.Client_Associate__c <> NULL){
                    //---------System.debug('**** Associate: '+csu.Client_Associate__c);
                    GroupMember gmAssociate = new GroupMember(GroupId = g.Id, UserOrGroupId = csu.Client_Associate__c);
                    newGms.add(gmAssociate);
                }
            }
        }
        
        if(newGms.size() > 0) insert newGms;
    }
    
    @future
    public static void groupMemberUpdateDml(Map<id, id> GroupIdGroupmemberId){
        //---------System.debug('**** new member '+GroupIdGroupmemberId.size());
        List<GroupMember> newGroupMembers = new List<GroupMember>();
        List<Group> groupToAddMember = [SELECT id FROM Group WHERE Id In :GroupIdGroupmemberId.keyset()];
        
        for(Group g: groupToAddMember){
            if(GroupIdGroupmemberId.get(g.id)<> NULL){
                GroupMember gm = new GroupMember(GroupId = g.Id, 
                                                 UserOrGroupId = GroupIdGroupmemberId.get(g.id));
                newGroupMembers.add(gm);
            }
        }
        
        if(newGroupMembers.size()>0) insert newGroupMembers;
    }
    
    @future
    public static void groupMemberDeleteDml(Set<Id> groupMember2delete){
        //---------System.debug('**** delete groupmember '+groupMember2delete.size());
        List<GroupMember> gmDelete = [SELECT Id from GroupMember where Id In :groupMember2delete];
        
        if(gmDelete.size()>0) delete gmDelete;
    }
}