/**
* ─────────────────────────────────────────────────────────────────────────────────────────────────┐
* Invocable methods for updating OwnerId of Non-Primary Person Accounts in a Household.
* 
* Person Accounts related to a Household in the AccountContactRelations table need to have their
* OwnerId value updated as the Client Service Unit change automation is firing.  This code provides
* a CPU efficent solution for passing the Household's CSU's Owner to the Person Accounts which are
* NOT the Primary__c but are still in the PrimaryGroup__c.
* ──────────────────────────────────────────────────────────────────────────────────────────────────
* @author: John Crapuchettes - john@zennify.com
* @created: 2019-10-31
* @modifiedBy: John Crapuchettes - john@zennify.com
* @modifiedDate: 2019-10-31
* ─────────────────────────────────────────────────────────────────────────────────────────────────┘
*/

public class NonPrimaryPersonOwnerUpdateInvocable {
    //Declare Invocable
    @InvocableMethod(label='Non Primary Person Owner Update Invocable' description='Updates the Owner of Non-Primary Person Accounts related to a Household')
    public static void updatePersonOwnerByHousehold(List<Id> HouseholdIds)
    {
        // Get Household from AccountId. Should be one.
        List<Account> HouseholdList = [SELECT ID, Name, OwnerId FROM Account WHERE ID IN :HouseholdIds LIMIT 1];
        system.debug('Household List: ' + HouseholdList);
        
        // Get the Owner from the Household.
        Id NewOwnerId = NULL;
        if(HouseholdList.size() > 0)
        {
            NewOwnerId = HouseholdList[0].OwnerId;
            system.debug('NewOwnerID: ' + NewOwnerId);
        }
        
        // Get the Non-Primary Member but in the Primary Group Person Accounts related to the Household via ACRs
        List<AccountContactRelation> NonPrimaryPersonList = 
            [SELECT ID, AccountId, ContactId FROM AccountContactRelation WHERE
             AccountId IN :HouseholdIds AND FinServ__Primary__c = FALSE AND FinServ__PrimaryGroup__c = TRUE];
        system.debug('Non Primary Person List: ' + NonPrimaryPersonList);
        
        // Get the list of ContactIds into a Set
        Set<Id> ContactIds = new Set<Id>();
        for(AccountContactRelation acr: NonPrimaryPersonList)
        {
            ContactIds.add(acr.ContactId);
        }
        system.debug('Contact Ids:' + ContactIds);
        
        // Bulk update variable
        List<Account> personUpdate = new List<Account>();
        
        // Query the Person Accounts which have PersonContactId from the ContactIds
        List<Account> personGet = [SELECT ID, OwnerId FROM Account WHERE Recordtype.DeveloperName = 'PersonAccount' AND PersonContactId IN :ContactIds];
        system.debug('Person Get List: ' + personGet);
        
        // Check to see if we get results
        if(personGet.size() > 0)
        {
            // Iterate around the Get list of Person Accounts.
            for(Account a: personGet)
            {
                // Update OwnerId from the Household.
                a.OwnerId = NewOwnerId;
                system.debug('OwnerId: ' + a.OwnerId);
                personUpdate.add(a);
            }
            
            if(personUpdate.size() > 0)
            {
                system.debug('Person Update List: ' + personUpdate);
                update personUpdate;
            }
        }
        return;
    }
}