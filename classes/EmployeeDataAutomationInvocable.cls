/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Invocable methods and supporting class structure for updating related records Employee Data.
 * 
 * Allows for the querying and updating of child records of a given account so that the Account's
 * Employee Data ie: Customer Type and Employee are passed on to the related records.  Overcomes the
 * limitations of flows where the iterations can not exceed 2000 records.  Utilizes the custom
 * metadata to narrow down the records types for each object while adding for better maintenance down
 * the road.
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author: John Crapuchettes - john@zennify.com
 * @created: 2019-09-17
 * @modifiedBy: John Crapuchettes - john@zennify.com
 * @modifiedDate: 2019-09-17
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

public class EmployeeDataAutomationInvocable {
    @InvocableMethod(label='Update Related By Account' description='Updates Related Objects Employee Data by the Account')
    public static void updateRelatedByAccount(List<Id> accountIDs)
    {
        // Verify the input list contains values.
        if(accountIDs.size() > 0)
        {           
            // Select the Accounts using the supplied Ids.
            List<Account> Accounts = [SELECT ID, Name, RecordType.Name, Customer_Type__c, Employee__c FROM Account WHERE ID =: accountIDs];
            system.debug('Accounts: ' + Accounts.size());
            
            // Get Custom Metadata for Case Record Types
            List<Employee_Data_RecordType__mdt> mdtCases = [SELECT ID, Label, Object_Name__c, Record_Type_Developer_Name__c FROM Employee_Data_RecordType__mdt WHERE Object_Name__c = 'Case'];
            system.debug('Cases MDT: ' + mdtCases.size());
            // Put the record types into a list
            List<String> caseRecordTypeDevNames = new List<String>();
            for(Employee_Data_RecordType__mdt c : mdtCases)
            {
                system.debug('*MDT record: ' + c.Object_Name__c + ' ' + c.Record_Type_Developer_Name__c);
                caseRecordTypeDevNames.add(c.Record_Type_Developer_Name__c);
            }
            
            // Get Custom Metadata for Opportunity Record Types
            List<Employee_Data_RecordType__mdt> mdtOpps = [SELECT ID, Label, Object_Name__c, Record_Type_Developer_Name__c FROM Employee_Data_RecordType__mdt WHERE Object_Name__c = 'Opportunity'];
            system.debug('Opps MDT: ' + mdtOpps.size());
            // Put the record types into a list
            List<String> oppsRecordTypeDevNames = new List<String>();
            for(Employee_Data_RecordType__mdt o : mdtOpps)
            {
                system.debug('*MDT record: ' + o.Object_Name__c + ' ' + o.Record_Type_Developer_Name__c);
                oppsRecordTypeDevNames.add(o.Record_Type_Developer_Name__c);
            }
            
            // Get Custom Metadata for Financial Account Record Types
            List<Employee_Data_RecordType__mdt> mdtFinAccts = [SELECT ID, Label, Object_Name__c, Record_Type_Developer_Name__c FROM Employee_Data_RecordType__mdt WHERE Object_Name__c = 'FinServ__FinancialAccount__c'];
            system.debug('FinAccts MDT: ' + mdtFinAccts.size());
            // Put the record types into a list
            List<String> finAcctsRecordTypeDevNames = new List<String>();
            for(Employee_Data_RecordType__mdt f : mdtFinAccts)
            {
                system.debug('*MDT record: ' + f.Object_Name__c + ' ' + f.Record_Type_Developer_Name__c);
                finAcctsRecordTypeDevNames.add(f.Record_Type_Developer_Name__c);
            }
            
            // Verify the Accounts found by ID is not blank.
            if(Accounts.size() > 0)
            {
                // All account employee data values should be the same so just need to grab the values to be updated from the first.
                string sCustomerType = Accounts[0].Customer_Type__c;
                ID idEmployee = Accounts[0].Employee__c;
                
                // Get records by Object matching the Account and applicable record types
                List<Case> selectCases = [SELECT ID, AccountId, RecordType.DeveloperName, Customer_Type__c, Employee__c FROM Case WHERE AccountId = :accountIDs AND RecordType.DeveloperName = :caseRecordTypeDevNames];
                List<Opportunity> selectOpps = [SELECT ID, AccountId, RecordType.DeveloperName, Customer_Type__c, Employee__c FROM Opportunity WHERE AccountId = :accountIDs AND RecordType.DeveloperName = :oppsRecordTypeDevNames];
                List<FinServ__FinancialAccount__c> selectFinAccts = [SELECT ID, FinServ__PrimaryOwner__c, FinServ__Household__c, RecordType.DeveloperName, Customer_Type__c, Employee__c FROM FinServ__FinancialAccount__c WHERE FinServ__PrimaryOwner__c = :accountIDs AND RecordType.DeveloperName = :finAcctsRecordTypeDevNames];
                
                system.debug('Select Cases: ' + selectCases.size());
                system.debug('Select Opps: ' + selectOpps.size());
                system.debug('Select FinAccts: ' + selectFinAccts.size());
                
                // Verify we are going to update something
                if(selectCases.size() > 0 || selectOpps.size() > 0 || selectFinAccts.size() > 0)
                {
                    // Create bulkified lists
                    List<Case> updateCases = new List<Case>();
                    List<Opportunity> updateOpps = new List<Opportunity>();
                    List<FinServ__FinancialAccount__c> updateFinAccts = new List<FinServ__FinancialAccount__c>();
                    
                    // Update related Case values
                    for(Case c: selectCases)
                    {
                        c.Customer_Type__c = sCustomerType;
                        c.Employee__c = idEmployee;
                        updateCases.add(c);
                        system.debug('*Case: ' + c.Id + ', ' + c.RecordType.DeveloperName + ', ' + c.Customer_Type__c + ', ' + c.Employee__c);
                    }
                    
                    // Update related Opportunity values
                    for(Opportunity o: selectOpps)
                    {
                        o.Customer_Type__c = sCustomerType;
                        o.Employee__c = idEmployee;
                        updateOpps.add(o);
                        system.debug('*Opp: ' + o.Id + ', ' + o.RecordType.DeveloperName + ', ' + o.Customer_Type__c + ', ' + o.Employee__c);
                    }
                    
                    // Update related Financial Account values
                    for(FinServ__FinancialAccount__c f: selectFinAccts)
                    {
                        f.Customer_Type__c = sCustomerType;
                        f.Employee__c = idEmployee;
                        updateFinAccts.add(f);
                        system.debug('*FinAcct: ' + f.Id + ', ' + f.RecordType.DeveloperName + ', ' + f.Customer_Type__c + ', ' + f.Employee__c);
                    }
                    
                    system.debug('Update Cases: ' + updateCases.size());
                    system.debug('Update Opps: ' + updateOpps.size());
                    system.debug('Update FinAccts: ' + updateFinAccts.size());
                    
                    // Bulk insert
                    if(updateCases.size() > 0)
                    {
                        update updateCases;
                    }
                    if (updateOpps.size() > 0)
                    {
                        update updateOpps;
                    }
                    if (updateFinAccts.size() > 0)
                    {
                        update updateFinAccts;
                    }
                }
            }
        }
    }
}