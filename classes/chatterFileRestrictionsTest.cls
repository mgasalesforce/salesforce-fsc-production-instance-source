@isTest
public class chatterFileRestrictionsTest {
    
    static testMethod void unitTestInsert(){
    //id abc = 'EXE' ;
    RecordType rtMkting = [SELECT ID FROM RecordType WHERE DeveloperName = 'Branch_Operations' LIMIT 1];
    Case cse = new Case();
    cse.RecordTypeId = rtMkting.Id;
    cse.Status = 'New';
    cse.Category__c = 'Prospect';
    cse.Reason = 'Email Blast';
    cse.Subject = 'test';
    cse.Description = 'test';
    Insert cse;       
    Test.startTest();

    ContentVersion contentVersion_1 = new ContentVersion(
        Title = 'Content',
        PathOnClient = 'Content.html',
        VersionData = Blob.valueOf('Test Content'),
        IsMajorVersion = true
    );
    insert contentVersion_1;//create the the content version record 

    ContentVersion contentVersion_2 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersion_1.Id LIMIT 1];//query the contentdocumentID
    List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];


    ContentDocumentLink newcontent = new ContentDocumentLink();
    newcontent.ContentDocumentId = contentVersion_2.ContentDocumentId;
    newcontent.LinkedEntityId = cse.Id;
    newcontent.ShareType =  'v';

    insert newcontent;//create ContentDocumentLink record

    FeedItem feed = new FeedItem (
        parentid = cse.id,
        type = 'ContentPost',
        RelatedRecordId = contentVersion_1.id,
        Body = 'Hello'
    );
    //then create feedItem record
    try {
        insert feed;    
    } catch (exception e) {
        System.assert(e.getMessage().contains('You can not add this type of file. Upload only photos.'));
    }
    Test.stopTest();      
}

}