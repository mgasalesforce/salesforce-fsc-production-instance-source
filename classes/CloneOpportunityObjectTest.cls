@IsTest(SeeAllData=true)
public class CloneOpportunityObjectTest 
{
    static testMethod void testcloneMultipleRecords() 
    {
        
        Account a = new Account();
        a.Name = 'TestName';
        a.Phone = '2222';
        a.BillingCity = 'TestCity';
        a.BillingState = 'MI';
        insert a;
        
        //Then create a primary contact
        Contact c = new Contact();
        c.FirstName = 'Paul';
        c.LastName  = 'Test';
        c.AccountId = a.id;
        c.MailingStreet = '298 S. Ringo Street';
        c.MailingCity = 'Little Rock';
        c.MailingState = 'AR';
        c.MailingPostalCode = '72201'; 
        insert c;
        System.debug('created primary contact');
        
        String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
        
        Account personacc = new Account();
        personacc.firstname = 'testfirst';
        personacc.lastname = 'testlast';
        personacc.BillingStreet = '123 street';
        personacc.recordtypeid = RecTypeId;
        insert personacc;
        
        //Create another contact
        Contact ci = new Contact();
        ci.FirstName = 'Bob';
        ci.LastName  = 'Test';
        ci.AccountId = a.id;
        ci.MailingStreet = '298 S. Ringo Street';
        ci.MailingCity = 'Little Rock';
        ci.MailingState = 'AR';
        ci.MailingPostalCode = '72201'; 
        insert ci;
        System.debug('created primary contact');
        
        Opportunity o = new Opportunity();
        o.Name = 'TestName';
        o.AccountID = a.Id;
        o.closedate = system.today();
        o.stagename = 'New';
        insert o; 
        
        
        //Now update the OCR for the primary contact
        OpportunityContactRole ocr = new OpportunityContactRole();
        ocr.ContactId = c.Id;
        ocr.OpportunityId = o.Id;
        ocr.IsPrimary = TRUE;
        ocr.Role = 'Decision Maker';
        insert ocr;
        System.debug('created opportunity contact role for primary');
        
        //Now update the OCR for another contact
        OpportunityContactRole ocr1 = new OpportunityContactRole();         
        ocr1.ContactId = ci.Id;
        ocr1.OpportunityId = o.Id;
        ocr1.IsPrimary = FALSE;
        ocr1.Role = 'Decision Maker';
        insert ocr1;
        System.debug('created opportunity contact role for another contact');
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create your product
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        //create your opportunity line item.  This assumes you already have an opportunity created, called opp
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = o.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 500
        );
        insert oli;
        
        String FinAccRecTypeId= [select Id from RecordType where (name='Bank Account') and (SobjectType='FinServ__FinancialAccount__c')].Id;
        
        FinServ__FinancialAccount__c finacc = new FinServ__FinancialAccount__c(name='test finacc', FinServ__PrimaryOwner__c=personacc.id, recordtypeid=FinAccRecTypeId);
        insert finacc;
        
        Opportunity_Financial_Account__c ofa = new Opportunity_Financial_Account__c(
            Opportunity__c = o.id,  
            Financial_Account__c = finacc.id
        );
        insert ofa;
        
        Test.startTest();
        //Test Method cloneMultipleRecords
        string oppids = '["' + string.valueof(o.id) + '"]';
        string retOppId = CloneOpportunityObject.cloneMultipleRecords(oppids);
        system.debug('retOppId:'+retOppId);
        
        //Test Method cloneRecord
        id retOppId2 = CloneOpportunityObject.cloneRecord(o.id);
        system.debug('retOppId:'+retOppId2);
        Test.stopTest();
    }
    
    
}