@isTest

global class Orion_MockHttpCallout implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
		HttpResponse res = new HttpResponse();
        
		// API call return
		res.setHeader('Content-Type', 'application/json');
        res.setStatusCode(200);
        res.setBody('{"access_token":"eyJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTUxMiIsInR5cCI6IkpXVCJ9.eyJBX1VEIjoiMjg3MDEzNiIsIkFfRUQiOiI1IiwiUEEiOiIxNzQ5IiwiVUNQIjoiRmFsc2UiLCJNTiI6IkRNWi1NRC1PQVNXRUIwMyIsIm5iZiI6MTU2ODg0NTY5OCwiZXhwIjoxNTY4ODgxNjk4LCJpc3MiOiJPcmlvbiIsImF1ZCI6Imh0dHA6Ly9zZXNzaW9uLm9yaW9uIn0.fFC2QOeX7a7NLJ7kznsjY0Rostw_6LjXLiXHWENSI88oNeZVVjLMd31IgH9UYtaH-E-cXng0zyYQ-PrN9tsnTA","expires_in":-36000.0,"refresh_token":"6IGtiQaNBGgHs0fa4cDcVB76wCxBpEBNGs65oSOFq5h1feHYTMpXuPGJmaocG3XCki71BIXhtzCviK8kuxrKr19q57zPEIJUHNlZ"}');   
              
        return res;
    }
}