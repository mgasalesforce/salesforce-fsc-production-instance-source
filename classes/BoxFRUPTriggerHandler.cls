/**
*Author: Kevin Rusk
*Date: 6/29/2017
*Updated: Praveen Kanumuri
*Date: 8/15/2019
**/

public class BoxFRUPTriggerHandler {
        
    /**
    *@Description: Push the File ID from the FRUP object to the Household(Account)
    *@param:  list<box__FRUP__c> the FRUP Records.
    *@return: None
    **/
    public static void updateInsertFileIDField(list<box__FRUP__c> FRUPs)
    {
        //map of Households to update
        map<id,account> accounts = new map<id,account>();
        System.debug('accounts = new map<id,account>()');
        
        for(box__FRUP__c thisFRUP : FRUPs)
        {
            IF (thisFRUP.box__Object_Name__c == 'Account' &&
                thisFRUP.box__Record_ID__c != NULL){
                    System.debug('FRUP Record ID: ' + thisFRUP.box__Record_ID__c);
                    System.debug('FRUP Folder ID: ' + thisFRUP.box__Folder_ID__c);
                
                    Account relatedAccount = new Account(id=thisFRUP.box__Record_ID__c, Box_Folder_ID__c = thisFRUP.box__Folder_ID__c);
                    //relatedAccount.Box_Folder_ID__c = thisFRUP.box__Folder_ID__c;
                    System.debug('relatedAccount Folder ID: ' + relatedAccount.Box_Folder_ID__c);
                    
                    accounts.put(thisFRUP.box__Record_ID__c,relatedAccount);
                    
            }
            
        }
        
        //update the Households.
        database.update(accounts.values(),false);
        
    }

}