public class chatterFileRestrictions {
    public static void preventFileTypes (List<FeedItem> newFeedItems){
        
        set<String> setAllowedTypes = new set<String> {'TIFF','JPG','JPEG','GIF', 'PNG'};
        
        try{
            Map<Id, FeedItem> contentversionIdToFeedItemMap = new Map<Id, FeedItem>();
            for(FeedItem fi : newFeedItems){
                if(fi.type == 'ContentPost')
                    contentversionIdToFeedItemMap.put(fi.RelatedRecordId, fi);
            }
            
            for(ContentVersion cv : [SELECT Id, FileExtension,FileType FROM ContentVersion where Id IN :contentversionIdToFeedItemMap.keySet()])
            {
                system.debug('File Type: ' + cv.FileType);
                system.debug('File Extension: ' + cv.FileExtension);
                if(!setAllowedTypes.Contains(cv.FileType)) 
                {
                    contentversionIdToFeedItemMap.get(cv.Id).addError('You can not add this type of file. Upload only photos.');
                }
            }
        }
        catch(exception e){                
        }          
    }
}