/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class RecurringBusinessProcessController {
    @RemoteAction
    global static FSTR.RecurringBusinessProcessController.CRUDResponse searchLookup(FSTR.RecurringBusinessProcessController.QueryObj qobj) {
        return null;
    }
global class CRUDResponse {
}
global class QueryObj {
    global QueryObj() {

    }
}
}
