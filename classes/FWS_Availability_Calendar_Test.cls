@isTest
private class FWS_Availability_Calendar_Test {
    @isTest static void test_FWS_Availability_Calendar() {
        
        PageReference pageRef = Page.FWS_Availability_Calendar;
        Test.setCurrentPage(pageRef);
        
        Test.setMock(HttpCalloutMock.class, new O365_MockHttpCallout());
        
        FWS_Availability_Calendar_Controller EPAcontroller = new FWS_Availability_Calendar_Controller();
        
        Date today_d = Date.today();
        Date monday = today_d.toStartOfWeek() + 1;
        
        
        EPAcontroller.setWeekDates(monday);
        
        EPAcontroller.redirectToOauth();
        EPAcontroller.requestToken();
        EPAcontroller.requestGraph();
        
        EPAcontroller.getTargetWeekSet();
        EPAcontroller.getIMAvailableItems();
        EPAcontroller.getTimezoneSet();
        String x = EPAcontroller.eventCode2literal(101);
        EPAcontroller.finalMeetingReqInfo = '     2019-12-1114:001 available meeting';
        EPAcontroller.showConfirmation();
        EPAcontroller.closeConfirmation();
        EPAcontroller.sendReqEmail();
        System.assertEquals('2019-', EPAcontroller.calendarYear);
        System.assertEquals('2019-12-11', EPAcontroller.finalMeetingReqInfo_date);
        
        EPAcontroller.calSearchResp ='{"@odata.context":"https://graph.microsoft.com/v1.0/$metadata#users(EPLoganBaker%40merceradvisors.com)/calendarView(subject,start,end,showAs)","value":[{"id":"AAMkAGMwYmZjM2FiLTY5NTYtNDdiYi1hMGZhLTUwNzU0NTk4MjQzOQBGAAAAAADcvEwJxM7UT4eJWUcfSH1yBwBP85Z5aKiUQ4ywQ76Qmv7pAAAAAAENAAAwStZ7wtqeTJfrTsG8vTpwAAG4dypoAAA=","subject":"EP-IM - Available","showAs":"busy","start":{"dateTime":"2018-08-27T08:00:00.0000000","timeZone":"America/Denver"},"end":{"dateTime":"2018-08-27T10:00:00.0000000","timeZone":"America/Denver"}}]}';  
        String calSearchResp_test = '{"@odata.context":"https://graph.microsoft.com/v1.0/$metadata#users(EPLoganBaker%40merceradvisors.com)/calendarView(subject,start,end,showAs)","value":[{"id":"AAMkAGMwYmZjM2FiLTY5NTYtNDdiYi1hMGZhLTUwNzU0NTk4MjQzOQBGAAAAAADcvEwJxM7UT4eJWUcfSH1yBwBP85Z5aKiUQ4ywQ76Qmv7pAAAAAAENAAAwStZ7wtqeTJfrTsG8vTpwAAG4dypoAAA=","subject":"EP-IM - Available","showAs":"busy","start":{"dateTime":"2018-08-27T08:00:00.0000000","timeZone":"America/Denver"},"end":{"dateTime":"2018-08-27T10:00:00.0000000","timeZone":"America/Denver"}}]}';  
        Map<String, Object> calTimeblockDeserial = (Map<String, Object>)Json.deserializeUntyped(calSearchResp_test);
        EPAcontroller.timeblocks = (List<object>)calTimeblockDeserial.get('value');
        
        List<object> timeblocks_test =  (List<object>)calTimeblockDeserial.get('value');
        EPAcontroller.obj2liststr(timeblocks_test);
        EPAcontroller.countDailyAvailability();
        EPAcontroller.timeblocks2timeblock();    
        String eventliteral = EPAcontroller.eventCode2literal(200);
        System.assertEquals('2 meeting sessions available ', eventliteral);
        EPAcontroller.targetWeek = '2';
        EPAcontroller.updateTargetWeek();
        EPAcontroller.showMeetingInfoReq();
    }
}