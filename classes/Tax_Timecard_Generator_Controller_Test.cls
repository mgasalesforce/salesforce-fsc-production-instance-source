/**
 * @File Name          : Tax_Timecard_Generator_Controller_Test.apxc
 * @Description        : 
 * @Author             : soobin.kittredge@merceradvisors.com
 * @Group              : 
 * @Last Modified By   : soobin.kittredge@merceradvisors.com
 * @Last Modified On   : 8/22/2019, 5:11:14 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/22/2019, 5:11:14 PM   soobin.kittredge@merceradvisors.com     Initial Version
**/
@isTest
public class Tax_Timecard_Generator_Controller_Test {
    public static testMethod void test_TimecardGenerator_Controller(){
        PageReference pageref = Page.Tax_Timecard_Generator;
        Test.setCurrentPage(pageref);
        
        Tax_Timecard_Generator_Controller tgcont = new Tax_Timecard_Generator_Controller();
        
        Test.startTest();
        
        Account accx = new Account(Name='test HH 2');
        insert accx;
        Account accxins = [SELECT Id, Name from Account where Name = 'test HH 2'];
        Opportunity newop= new Opportunity(name='1', Accountid = accxins.id, StageName='New', CloseDate=date.newInstance(2019, 9, 15));

        insert newop;
        
        tgcont.opptypeEP();
        tgcont.addSelectedOpp();
        
        tgcont.selectedOppsList = [SELECT id, name, RecordType.DeveloperName, Accountid, Account.Name, LastModifiedBy.name, StageName FROM Opportunity Limit 3];
        System.debug(LoggingLevel.INFO, '****Opplist Size: ' + tgcont.selectedOppsList.size());
        
        tgcont.initTcCreation();
        tgcont.applyAllTC();
        tgcont.createTimecard();
        
        Test.stopTest();
        
    }
}