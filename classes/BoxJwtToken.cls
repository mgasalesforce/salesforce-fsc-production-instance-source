// @description Get JWT Signature and Box Access Token 
// @author Praveen Kanumuri
// @date 8-04-2019
public class BoxJwtToken {
     
    public class Configuration {
        public String jwtUsername {get; set;}
        public String jwtConnectedAppConsumerKey {get; set;}
        public String publicKeyId {get; set;}
        public String privateKey {get; set;}
        public String jwtHostname {get; set;}
        public String jwtJti {get; set;}
        public String box_sub_type {get; set;}
        
    }
     
    private class Header {
        String alg;
        String typ;
        String kid;
        Header(String alg, String typ, String kid) {
            this.alg = alg;
            this.typ = typ;
            this.kid = kid;
        }
    }
     
    private class Body {
        String iss;
        String sub;//
        String box_sub_type;// 
        String exp;
        String aud;
        String jti;
        

        Body(String iss, String sub, String box_sub_type, String aud, String jti, String exp) {
            this.iss = iss;
            this.sub = sub;
            this.box_sub_type= box_sub_type;
            //this.prn = prn;
            this.aud= aud;
            this.jti= jti;
            this.exp= exp;
        }
    }
     
    private class JwtException extends Exception {
    }
     
    private Configuration config;
     
    public BoxJwtToken(Configuration config) {
         
        this.config = config;
    }
     
    public String requestAccessToken() {
 
  //  string strQuery;  
  //if (!runningInASandbox()) {
      //strQuery = 'select ClientId__c,ClientSecret__c from Box_JWT_Authentication__mdt where label = \'BoxJwtAuthProduction\'';   
  //}
  
  List<Box_JWT_Authentication__mdt> boxAuthMdt = [select 
                                          ClientId__c,ClientSecret__c
                                      from 
                                          Box_JWT_Authentication__mdt
                                       where label = 'BoxJwtAuthFinal'   
                                      ];
                                      
         Map<String, String> m = new Map<String, String>();
         m.put('grant_type', 'urn:ietf:params:oauth:grant-type:jwt-bearer');
         m.put('assertion', createToken());
         m.put('client_id',boxAuthMdt[0].ClientId__c);
         m.put('client_secret',boxAuthMdt[0].ClientSecret__c);         
     
         HttpRequest req = new HttpRequest();
         req.setHeader('Content-Type','application/x-www-form-urlencoded');
         req.setEndpoint('https://' + config.jwtHostname +'/oauth2/token');
         req.setMethod('POST');
         req.setTimeout(60 * 1000);
         req.setBody(formEncode(m));
               
         HttpResponse res = new Http().send(req);    
         system.debug('response body:'+res.getBody()); 
     
         if (res.getStatusCode() >= 200 && res.getStatusCode() < 300) {
             return extractJsonField(res.getBody(), 'access_token');
         } else {
             system.debug('res.getStatusCode():'+res.getStatusCode());
             throw new JwtException(res.getBody());
         }
     
    }
     
    public String formEncode(Map<String, String> m) {
         
         String s = '';
         for (String key : m.keySet()) {
            if (s.length() > 0) {
                s += '&';
            }
            s += key + '=' + EncodingUtil.urlEncode(m.get(key), 'UTF-8');
         }
         return s;
    }
     
    private String extractJsonField(String body, String field) {
         
        JSONParser parser = JSON.createParser(body);
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME
                    && parser.getText() == field) {
                parser.nextToken();
                return parser.getText();
            }
        }
        throw new JwtException(field + ' not found in response ' + body);
    }
     
    public String createToken() {
         
        String alg = 'RS256';
        String typ = 'JWT';

        List<Box_JWT_Authentication__mdt> boxAuthMdt = [select 
                                          jwtPublicKeyId__c,boxSubType__c,jwtJti__c
                                      from 
                                          Box_JWT_Authentication__mdt
                                      where label = 'BoxJwtAuthFinal'    
                                      ];
                                    
        String kid = boxAuthMdt[0].jwtPublicKeyId__c;
                                               
        String iss = config.jwtConnectedAppConsumerKey;
        String sub = config.jwtUsername;
        String box_sub_type = config.box_sub_type;
        String aud = 'https://' + config.jwtHostname +'/oauth2/token';  //'https://' + config.jwtHostname;
        String jti = generateRandomString(16); //config.jwtJti;  //'https://' + config.jwtHostname;
        //String iat = String.valueOf(System.currentTimeMillis() /1000);
        String exp = String.valueOf((System.currentTimeMillis()+60000) /1000);
        
        
        //String scope = 'signature impersonation';
         
        String headerJson = JSON.serialize(new Header(alg,typ,kid));
        //String bodyJson =  JSON.serialize(new Body(iss, sub, box_sub_type, aud, jti, exp));
        
        String bodyJson =  '{"sub":"'+ sub  + '","jti":"' + jti ;
        bodyJson  = bodyJson  + '","iss":"' + iss + '","exp":' + exp + ',"box_sub_type":"' + box_sub_type + '","aud":"' + aud  + '"}';   
        
         
        system.debug('headerJson:'+headerJson);
        system.debug('bodyJson: '+bodyJson);

            
        String token = base64UrlSafe(Blob.valueOf(headerJson))
                + '.' + base64UrlSafe(Blob.valueOf(bodyJson));
    Blob privateKey = EncodingUtil.base64Decode(config.privateKey);
        Blob signature = Crypto.sign('rsa-sha256', Blob.valueOf(token), privateKey);
    
  
        token += '.' + base64UrlSafe(signature);
         
        system.debug('token: '+token); 
         
    return token;

    }
     
    private String base64UrlSafe(Blob b) {
         
        //return EncodingUtil.base64Encode(b).replace('+', '-').replace('/', '_');
    String output = encodingUtil.base64Encode(b);
        output = output.replace('+', '-');
        output = output.replace('/', '_');
        while ( output.endsWith('=')){
            output = output.subString(0,output.length()-1);
        }
        return output;
  }  

  public void mDebugUntruncated(String sMsg) {
    for (Integer i = 0; i < sMsg.length(); i=i+300) {
        Integer iEffectiveEnd = (i+300 > (sMsg.length()-1) ? sMsg.length()-1 : i+300);
        System.debug(sMsg.substring(i,iEffectiveEnd));
      }
  }
  
  public String generateRandomString(Integer len) {
    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
    String randStr = '';
    while (randStr.length() < len) {
       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
       randStr += chars.substring(idx, idx+1);
    }
    return randStr; 
  }

  public Boolean runningInASandbox {
    get {
        if (runningInASandbox == null) {
            runningInASandbox = [SELECT IsSandbox FROM Organization LIMIT 1].IsSandbox;
        }
        return runningInASandbox;
    }
    set;
 }

}