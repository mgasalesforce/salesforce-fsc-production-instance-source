/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ForceReevaluateSrvc {
    global ForceReevaluateSrvc() {

    }
    @Future(callout=false)
    global static void forceReevaluateFuture(Set<Id> processObjectIds) {

    }
    @InvocableMethod
    global static void forceReevaluateInvocable(List<Id> processObjectIds) {

    }
    global static void forceReevaluateNonFuture(Set<Id> processObjectIds) {

    }
    global static void forceReevaluatePreFuture(Set<Id> processObjectIds) {

    }
}
