@isTest
public class OpptLineItemSort_Test {
    public static testMethod void test_OpptLineItemSort(){
        
        Account accx = new Account(Name='test HH 2');
        insert accx;
        Account accxins = [SELECT Id, Name from Account where Name = 'test HH 2'];
        Opportunity newop= new Opportunity(name='1', Accountid = accxins.id, StageName='New', CloseDate=date.newInstance(2019, 9, 15));

        insert newop;
        
        Id pricebookId = Test.getStandardPricebookId();
        
        //Create your product
        Product2 prod = new Product2(
            Name = 'Product X',
            ProductCode = 'Pro-X',
            isActive = true
        );
        insert prod;
        
        //Create your pricebook entry
        PricebookEntry pbEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = prod.Id,
            UnitPrice = 100.00,
            IsActive = true
        );
        insert pbEntry;
        
        //create your opportunity line item.  This assumes you already have an opportunity created, called opp
        OpportunityLineItem oli = new OpportunityLineItem(
            OpportunityId = newop.Id,
            Quantity = 5,
            PricebookEntryId = pbEntry.Id,
            TotalPrice = 5 * pbEntry.UnitPrice
        );
        insert oli;
        
        Test.startTest();
        PageReference pageRef = Page.Opportunity_Invoice;
        pageRef.getParameters().put('recordId',newop.id);
        Test.setCurrentPage(pageRef);
        Apexpages.StandardController stdcont = new Apexpages.StandardController(newop);
        OpptLineItemSort olts = new OpptLineItemSort(stdcont);
        List<OpportunityLineItem> opplistitems = olts.getLineItems();
        Test.stopTest();
    }
}