/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class ProcessComposerExecution {
    global ProcessComposerExecution() {

    }
    global static void CheckObjectBeforeDelete(List<SObject> ObjectToDelete) {

    }
    global static void CreateStepsOnUpdate(List<SObject> oldRec, Map<Id,SObject> newRec) {

    }
    global static void CreateSteps(List<SObject> newRec) {

    }
    global static void DeleteBusinessProcesses(Schema.SObjectType objT, Set<Id> deletedRecords) {

    }
    global static void DeleteCases(Schema.SObjectType objT, Set<Id> deletedRecords) {

    }
    global static void DeleteCustomLookupTaskSteps(Schema.SObjectType objT, Set<Id> deletedRecords) {

    }
    global static void DeleteProcessTasks(Schema.SObjectType objT, Set<Id> deletedRecords) {

    }
    global static void DeleteQueuedSteps(Schema.SObjectType objT, Set<Id> deletedRecords) {

    }
    global static void DetermineBusiness(List<SObject> newTrigger) {

    }
    global static void DetermineDefinition(List<SObject> newTrigger) {

    }
    global static void PopulateParentObjectName(List<SObject> newTrigger) {

    }
    global static List<SObject> StepProcessObjects(List<SObject> objs) {
        return null;
    }
    global static Map<Id,SObject> StepProcessObjects(Map<Id,SObject> objs) {
        return null;
    }
    global static void UnDeleteBusinessProcesses(Schema.SObjectType objT, Set<Id> undeletedRecords) {

    }
    global static void UnDeleteCases(Schema.SObjectType objT, Set<Id> undeletedRecords) {

    }
    global static void UnDeleteCustomLookupTaskSteps(Schema.SObjectType objT, Set<Id> undeletedRecords) {

    }
    global static void UnDeleteProcessTasks(Schema.SObjectType objT, Set<Id> undeletedRecords) {

    }
    global static void UnDeleteQueuedSteps(Schema.SObjectType objT, Set<Id> undeletedRecords) {

    }
    global static void UpdateCompletedProcessObjects(List<SObject> objs) {

    }
    global static void UpdateCustomStepCompletedDate(List<SObject> newStepsList, Map<Id,SObject> oldStepsMap) {

    }
    global static void UpdateStep(List<SObject> oldObj, List<SObject> UpdatedObj) {

    }
}
