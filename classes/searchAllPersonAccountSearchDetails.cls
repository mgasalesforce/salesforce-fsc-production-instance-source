public without sharing class searchAllPersonAccountSearchDetails {
    @InvocableMethod (label='searchAllPersonAccountSearchDetails' description= 'searchAllPersonAccountSearchDetails')
    public static List<List<Person_Account_Search_Details__c>> searchAllAccountContactRelations(List<String> lstPersonContactIds) {

       string PersonContactId = lstPersonContactIds[0];
       system.debug('PersonContactId:'+PersonContactId);
       List<FeedItem> feedItemList = new List<FeedItem>();
       
       string  strQueryAcr = 'Select ContactId, Roles, FinServ__Primary__c, FinServ__PrimaryGroup__c, ';
       strQueryAcr = strQueryAcr + ' Account.Client_Service_Unit__c, Account.Client_Associate__c, Account.Client_Advisor__c, ';
       strQueryAcr = strQueryAcr + ' Contact.Name, Contact.Phone, Contact.Account.BillingCity, Contact.Account.BillingState, ';
       strQueryAcr = strQueryAcr + ' Contact.Account.id,Account.Name, Account.id, ';
       strQueryAcr = strQueryAcr + ' Account.Client_Service_Unit__r.Name, Account.Client_Service_Unit__r.Client_Advisor__r.Name, ';
       strQueryAcr = strQueryAcr + ' Account.Client_Service_Unit__r.Client_Associate__r.Name';
       strQueryAcr = strQueryAcr + ' from AccountContactRelation ';
       system.debug('strQueryAcr:'+strQueryAcr);

       string strQueryWhere = 'where isActive = True and ContactId = \'' + PersonContactId + '\'';

    system.debug('strQueryAcr:'+strQueryAcr);
    system.debug('strQueryWhere:'+strQueryWhere);


      List<AccountContactRelation> accountContactDetails = database.query(strQueryAcr+strQueryWhere);

        List<Person_Account_Search_Details__c> lstObj  = new List<Person_Account_Search_Details__c>();
        for(AccountContactRelation acr : accountContactDetails){
            Person_Account_Search_Details__c obj = new Person_Account_Search_Details__c();
            obj.Name__c = acr.Contact.Name; // String.valueOf(objCase.get('id'));
            obj.Phone__c = acr.Contact.Phone;      
            obj.Roles__c = acr.Roles;
            obj.Primary_Household__c = acr.FinServ__PrimaryGroup__c;
            obj.Primary_Member__c =  acr.FinServ__Primary__c;
            obj.City__c =  acr.Contact.Account.BillingCity;
            obj.State__c = acr.Contact.Account.BillingState;
            obj.Cleint_Advisor__c = acr.Account.Client_Service_Unit__r.Client_Advisor__r.Name;
            obj.Client_Service_Unit__c = acr.Account.Client_Service_Unit__r.Name;
            obj.Client_Associate_Name__c = acr.Account.Client_Service_Unit__r.Client_Associate__r.Name;
            obj.Household__c = acr.Account.Name;
                  
            lstObj.add(obj);

            /*
            FeedItem postHousehold = new FeedItem();
            postHousehold.createdById = UserInfo.getUserId();
            postHousehold.ParentId = acr.Account.id;
            postHousehold.Type = 'TextPost';                
            postHousehold.Body = UserInfo.getName() + ' searched for and opened the Account record.';
            feedItemList.add(postHousehold);*/            
        }

      //if(lstObj.size()>0)
            //{     
          //  insert lstObj; 
      //}      

      //Post to Chatter - insert Feed Item        
      if(feedItemList.size()>0)
            {
                insert feedItemList;
            }           


      List<List<Person_Account_Search_Details__c>> lstPasd = new List<List<Person_Account_Search_Details__c>>();
      lstPasd.add(lstObj);
      return lstPasd;

    }
}