@isTest
private class FinancialAccountTriggerTest {
    private static testMethod void Test_FinancialAccount () {
        
        String RecTypeId= [select Id from RecordType where (Name='Person Account') and (SobjectType='Account')].Id;
        
        Account personacc = new Account();
        personacc.firstname = 'testfirst';
        personacc.lastname = 'testlast';
        personacc.BillingStreet = '123 street';
        personacc.recordtypeid = RecTypeId;
        insert personacc;
        
        String FinAccRecTypeId= [select Id from RecordType where (name='Bank Account') and (SobjectType='FinServ__FinancialAccount__c')].Id;
        
        FinServ__FinancialAccount__c fw = new FinServ__FinancialAccount__c(name='test finacc', FinServ__PrimaryOwner__c=personacc.id, recordtypeid=FinAccRecTypeId);
        insert fw;
        System.assertNotEquals(null, fw.id);
        update fw;
        delete fw;
    }
}