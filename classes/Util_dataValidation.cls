/**
 * @File Name          : Util_dataValidation.apxc
 * @Description        : 
 * @Author             : soobin.kittredge@merceradvisors.com
 * @Group              : 
 * @Last Modified By   : soobin.kittredge@merceradvisors.com
 * @Last Modified On   : 9/5/2019, 2:44:36 PM
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0    8/29/2019, 4:49:31 PM   soobin.kittredge@merceradvisors.com     Initial Version
**/
public class Util_dataValidation {
    public static String State2Abbr(String stateFullName){
        String stateAbb;
        if(stateFullName.length() <> 2){
            
            switch on stateFullName{
                when 'Alabama' {stateAbb = 'AL';}
                when 'Alaska' {stateAbb = 'AK';}
                when 'Arizona' {stateAbb = 'AZ';}
                when 'Arkansas' {stateAbb = 'AR';}
                when 'California' {stateAbb = 'CA';}
                when 'Colorado' {stateAbb = 'CO';}
                when 'Connecticut' {stateAbb = 'CT';}
                when 'Delaware' {stateAbb = 'DE';}
                when 'Florida' {stateAbb = 'FL';}
                when 'Georgia' {stateAbb = 'GA';}
                when 'Hawaii' {stateAbb = 'HI';}
                when 'Idaho' {stateAbb = 'ID';}
                when 'Illinois' {stateAbb = 'IL';}
                when 'Indiana' {stateAbb = 'IN';}
                when 'Iowa' {stateAbb = 'IA';}
                when 'Kansas' {stateAbb = 'KS';}
                when 'Kentucky' {stateAbb = 'KY';}
                when 'Louisiana' {stateAbb = 'LA';}
                when 'Maine' {stateAbb = 'ME';}
                when 'Maryland' {stateAbb = 'MD';}
                when 'Massachusetts' {stateAbb = 'MA';}
                when 'Michigan' {stateAbb = 'MI';}
                when 'Minnesota' {stateAbb = 'MN';}
                when 'Mississippi' {stateAbb = 'MS';}
                when 'Missouri' {stateAbb = 'MO';}
                when 'Montana' {stateAbb = 'MT';}
                when 'Nebraska' {stateAbb = 'NE';}
                when 'Nevada' {stateAbb = 'NV';}
                when 'New Hampshire' {stateAbb = 'NH';}
                when 'New Jersey' {stateAbb = 'NJ';}
                when 'New Mexico' {stateAbb = 'NM';}
                when 'New York' {stateAbb = 'NY';}
                when 'North Carolina' {stateAbb = 'NC';}
                when 'North Dakota' {stateAbb = 'ND';}
                when 'Ohio' {stateAbb = 'OH';}
                when 'Oklahoma' {stateAbb = 'OK';}
                when 'Oregon' {stateAbb = 'OR';}
                when 'Pennsylvania' {stateAbb = 'PA';}
                when 'Rhode Island' {stateAbb = 'RI';}
                when 'South Carolina' {stateAbb = 'SC';}
                when 'South Dakota' {stateAbb = 'SD';}
                when 'Tennessee' {stateAbb = 'TN';}
                when 'Texas' {stateAbb = 'TX';}
                when 'Utah' {stateAbb = 'UT';}
                when 'Vermont' {stateAbb = 'VT';}
                when 'Virginia' {stateAbb = 'VA';}
                when 'Washington' {stateAbb = 'WA';}
                when 'West Virginia' {stateAbb = 'WV';}
                when 'Wisconsin' {stateAbb = 'WI';}
                when 'Wyoming' {stateAbb = 'WY';}
                when 'Ontario' {stateAbb = 'ON';}
                when 'Quebec' {stateAbb = 'QC';}
                when 'QuéBec' {stateAbb = 'QC';}
                when 'British Columbia' {stateAbb = 'BC';}
                when 'Alberta' {stateAbb = 'AB';}
                when 'Manitoba' {stateAbb = 'MB';}
                when 'Saskatchewan' {stateAbb = 'SK';}
                when 'Nova Scotia' {stateAbb = 'NS';}
                when 'New Brunswick' {stateAbb = 'NB';}
                when 'Newfoundland And Labrador' {stateAbb = 'NL';}
                when 'Prince Edward Island' {stateAbb = 'PE';}
                when 'Northwest Territories' {stateAbb = 'NT';}
                when 'Yukon' {stateAbb = 'YT';}
                when 'Nunavut' {stateAbb =  'NU';}
                when else{stateAbb = 'NA';}
            }
        } else {
            stateAbb = 'NA';
        }
        return stateAbb;
    }
    
    public static String Gender2Char(String genderFull){
        String genderChar;
        switch on genderFull{
            when 'Male' 	{genderChar = 'M';}
            when 'Female' 	{genderChar = 'F';}
            when else 		{genderChar = '';}
        }
        return genderChar;
    }
}