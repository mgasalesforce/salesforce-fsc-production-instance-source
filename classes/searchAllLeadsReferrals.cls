// @description Invocable Apex class from Flow (/flow/Search_All_Person_Accounts_Leads_Referrals_Flow) to return All Leads meeting search criteria bypassign sharing model 
// @author Praveen Kanumuri
// @date 7-25-2019

global without sharing class searchAllLeadsReferrals {
   @InvocableMethod(label='searchAllLeadsReferrals' description='searchAllLeadsReferrals')
   public static List<List<Lead>> getLeadsReferrals(List<String> lstSearchJsonString) {
    string strName = '';
    string strCity = '';
    string strPhone = '';
    string strStatus = 'All';
    system.debug('jsonstring:'+lstSearchJsonString[0]);
    system.JSONParser jp = JSON.createParser(lstSearchJsonString[0]);
        while(jp.nextToken()!=null)
        {
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='name')
            {
              jp.nextToken();
                strName = jp.getText();    
            }   
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='city')
            {
              jp.nextToken();                 
                strCity = jp.getText();                
            }   
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='phone')
            {
              jp.nextToken();                 
                strPhone = jp.getText();                
            }   
            if(jp.getCurrentToken()==System.JSONToken.FIELD_NAME && jp.getText()=='status')
            {
              jp.nextToken();                 
                strStatus = jp.getText();                
            }   
        }    
    
      //system.debug('strName:'+strName);
      //system.debug('strCity:'+strCity);
       
       
       string  strQueryLeads = 'SELECT Id, ';
       strQueryLeads = strQueryLeads + 'name, ';
       strQueryLeads = strQueryLeads + 'City, ';
       strQueryLeads = strQueryLeads + 'State, ';
       strQueryLeads = strQueryLeads + 'Phone, ';
       strQueryLeads = strQueryLeads + 'OwnerId, ';
       strQueryLeads = strQueryLeads + 'Status, ';
       strQueryLeads = strQueryLeads + 'LeadSource, ';
       strQueryLeads = strQueryLeads + 'OwnerName__c ';
       strQueryLeads = strQueryLeads + 'FROM Lead ';
       strQueryLeads = strQueryLeads + 'WHERE isConverted = False and Name like \'%' + strName + '%\'';

       if(strPhone!=null && strCity.trim() <> '')
       {
           strQueryLeads = strQueryLeads + ' and Phone like \'%' + strPhone + '%\'';
       }

      //system.debug('strQueryLeads:'+strQueryLeads);

      //
      List<Lead> Leads = database.query(strQueryLeads);
      List<List<Lead>> lstLeads = new List<List<Lead>>();
       lstLeads.add(Leads);
      return lstLeads;
   }
}