// @description Test Class for Code Coverage for PostPersonAccountSearchToChatter 
// @author Praveen Kanumuri
// @date 8-06-2019

@IsTest(SeeAllData=true)
public class PostLeadSearchToChatterTest 
{
  static testMethod void testPostLeadSearchToChatter() 
  {
  
  
              Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 

              User u = new User(Alias = 'testabc', Email='standarduser@testorg.com', 
              EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
              LocaleSidKey='en_US', ProfileId = p.Id, 
              TimeZoneSidKey='America/Los_Angeles', UserName='test4a6@test4a6.com');  

              insert u;

            Lead l = new Lead(Company = 'Test Lead',
                      LastName = 'LeadLastName',
                      FirstName = 'LeadFirstName',
                      LeadSource = 'Call-in',
                      Status='Closed: Not Converted',
                      OwnerId = u.id);
            insert l;
                
                List<Id> lstLeadId = new List<Id>();
                lstLeadId.add(l.id);

                List<string> lststr = PostLeadSearchToChatter.PostPersonAccountSearchToChatter(lstLeadId);

                System.assertEquals(1, lststr.size());


 
  }

        
}