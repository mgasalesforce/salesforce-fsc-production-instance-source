/**
* OpptLineItemSort
* @author Soobin Kittredge @ Mercer Advisors
* @date 01/07/2018
* @description Custom extension controller for sorting line items properly on visualforce pages
*  
* Revision History:
* 01/07/2018  Soobin K. - Initial Draft.
* 01/19/2018  Soobin K. - Update - selective line items will be included
* 08/19/2018  Soobin K. - Moved to FSC (this header comments needs to be updated)
*/
public class OpptLineItemSort {
    
    public Opportunity opp;
    public Id oppId;
    public List<OpportunityLineItem> lineItems;
    transient public Double currnetinv {get; set;}
    
    public OpptLineItemSort(ApexPages.StandardController stdCont){
        this.opp = (Opportunity)stdCont.getRecord();
        oppId = opp.id;
        currnetinv=0;
    }
    
    public List<OpportunityLineItem> getLineItems(){
        
        lineItems = [SELECT Opportunity.id, Include_in_Invoice__c, Service_Hours__c, ServiceDate, Description, Quantity, UnitPrice, TotalPrice, PricebookEntry.Name, PricebookEntry.Product2.Name, PricebookEntry.Product2.Family
                     FROM OpportunityLineItem
                     WHERE Opportunity.id =: oppId AND Include_in_Invoice__c = TRUE
                     ORDER BY ServiceDate asc
                    ];
        for(OpportunityLineItem olit : lineItems){
            currnetinv = currnetinv + olit.TotalPrice;
        }
        return lineItems;
    }
}