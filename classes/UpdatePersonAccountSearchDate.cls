global without sharing class UpdatePersonAccountSearchDate {
   @InvocableMethod(label='UpdatePersonAccountSearchDate' description='UpdatePersonAccountSearchDate')
   public static List<string> UpdatePersonAccountSearchDate(List<id> lstAccountId) {
    id strId = lstAccountId[0];    
       system.debug('strId:'+strId);
       List<FeedItem> feedItemList = new List<FeedItem>();
        List<id> personContactId = new list<id>();
    
         if(!lstAccountId.isEmpty()){

            //Post to PersonAccount 
            FeedItem postPersonAccount = new FeedItem();
			postPersonAccount.createdById = UserInfo.getUserId();
			postPersonAccount.ParentId = strId;
			postPersonAccount.Type = 'TextPost';				
            postPersonAccount.Body = UserInfo.getName() + ' searched for and opened the Contact record.';
			feedItemList.add(postPersonAccount);

            //This update first process builder that posts @mention to Account Owner 
            List<Account> lstAccount = new List<Account>([Select ID,Account_Search_Date__c, PersonContactId FROM Account where ID = :lstAccountId[0]]);
            List<Account> lstUpdateAcc = new List<Account>();
            system.debug('lstAccount Count:'+ lstAccount.size()); 
            for(Account acc : lstAccount){
                acc.Account_Search_Date__c = system.now();
                lstUpdateAcc.add(acc);
                personContactId.add(acc.PersonContactId);
            }
            if(!lstUpdateAcc.isEmpty()){
                update lstUpdateAcc;
            }

            string  strQueryAcr = 'Select Account.id ';
            strQueryAcr = strQueryAcr + ' from AccountContactRelation ';
            strQueryAcr = strQueryAcr + 'where isActive = True and ContactId in : ' + PersonContactId;

            system.debug('strQueryAcr:'+strQueryAcr);

            //Post to Household
            List<AccountContactRelation> accountContactDetails = database.query(strQueryAcr);
	        for(AccountContactRelation acr : accountContactDetails){
                  
                  FeedItem postHousehold = new FeedItem();
			      postHousehold.createdById = UserInfo.getUserId();
			      postHousehold.ParentId = acr.Account.id;
			      postHousehold.Type = 'TextPost';				
                  postHousehold.Body = UserInfo.getName() + ' searched for and opened the Account record.';
			      feedItemList.add(postHousehold);            
		    }


            
             //Post to Chatter - insert Feed Item        
        	if(feedItemList.size()>0)
			{
				insert feedItemList;
			}    	




        }    
       
       		
			
       
		return lstAccountId;
	}
}