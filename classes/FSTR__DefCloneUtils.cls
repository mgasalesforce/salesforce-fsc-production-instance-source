/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class DefCloneUtils {
    global DefCloneUtils() {

    }
    global static String ExportDefinitions(Set<Id> definitions) {
        return null;
    }
    global static FSTR.DefCloneUtils.ExportWrapper ImportDefinitionsToWrapper(String importString) {
        return null;
    }
    global static Map<Id,FSTR__PCE_Definition__c> ImportDefinitions(FSTR.DefCloneUtils.ExportWrapper wrap) {
        return null;
    }
    global static Map<Id,FSTR__PCE_Definition__c> ImportDefinitions(String importString) {
        return null;
    }
global class ExportWrapper {
    global List<FSTR__PCE_Step_Approval_Item__c> approvalItems {
        get;
        set;
    }
    global List<FSTR__PCE_Step_Checklist_Item__c> checklistItems {
        get;
        set;
    }
    global Map<Id,List<FSTR__PCE_Definition_Criteria__c>> defCriteria {
        get;
        set;
    }
    global List<FSTR__PCE_Definition__c> defs {
        get;
        set;
    }
    global Map<Id,List<FSTR__PCE_Stage__c>> defStages {
        get;
        set;
    }
    global List<FSTR__PCE_Step_Field_Dependency__c> fieldDeps {
        get;
        set;
    }
    global List<FSTR__PCE_Step_Field_Update__c> fieldUpdates {
        get;
        set;
    }
    global List<FSTR__PCE_Step_Field_Validation__c> fieldValidations {
        get;
        set;
    }
    global Map<Id,List<FSTR__PCE_Definition_Initiator_Criteria__c>> initCriteria {
        get;
        set;
    }
    global List<FSTR__PCE_Definition_Initiator__c> initiators {
        get;
        set;
    }
    global List<FSTR__PCE_Step_Loop_Back_Criteria__c> loopBackCriteria {
        get;
        set;
    }
    global Map<Id,List<FSTR__PCE_Step__c>> orderedChildSteps {
        get;
        set;
    }
    global List<FSTR__PCE_Step__c> orderedSteps {
        get;
        set;
    }
    global Map<Id,QueueSobject> queues {
        get;
        set;
    }
    global List<FSTR__Step_Event__c> stepEvents {
        get;
        set;
    }
    global ExportWrapper() {

    }
}
}
