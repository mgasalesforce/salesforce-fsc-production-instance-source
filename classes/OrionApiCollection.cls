/**
* @File Name          : OrionApiCollection.apxc
* @Description        : 
* @Author             : soobin.kittredge@merceradvisors.com
* @Group              : 
* @Last Modified By   : soobin.kittredge@merceradvisors.com
* @Last Modified On   : 9/4/2019, 12:06:16 PM
* @Modification Log   : 
* Ver       Date            Author                  Modification
* 1.0    9/3/2019   soobin.kittredge@merceradvisors.com     Initial Version
**/
public class OrionApiCollection {/** Variable List ------------------------------------------------ */
    private static String orion_usnm           {get; set;}
    private static String orion_pw             {get; set;}
    public static String orion_token           {get; set;}
    private static String orion_refresh_token  {get; set;}
    public static Datetime orion_expiration    {get; set;}
    public static String orion_clientid        {get; set;}
    public static String orion_clientsecret    {get; set;}
    public static String ORION_ENDPOINT        = 'https://api.orionadvisor.com/api';
    
    public static OrionAuthCredential__c orionAuthCred {get; set;}
    //public Api_BasicAuth__mdt orionauth         {get; set;}
    
    public Boolean orion_authorized         {get; set;}
    public static String ErrorMsg                  {get; set;}
    public static Boolean IsorionAuthCredUpdated   {get; set;}
    
    public String orion_ClientHouseholdId   {get; set;}
    
    public OrionApi_Client clientObj        {get; set;}
    public static String accountVerboseStr			{get; set;}
    public static FinServ__FinancialAccount__c TargetFinAcc {get; set;}
    
    public static Blob cryptKey                    {get; set;}
    
    
    /** main Method ------------------------------------------------  */
    public OrionApiCollection(){
        OrionApiCollection_Init();
    }
    
    /** METHODS ----------------------------------------------------- */
    
    public void OrionApiCollection_Init(){
        getOrionAuthCred();
        
        if(!orionAuthorized()){
            if(orion_refresh_token <> NULL) {
                //refreshOrionAuth();
            } else {
                authOrionInit();
            }
        } 
    }
    
    private Boolean orionAuthorized(){
        Boolean Authorized;
        if(orion_token == NULL || datetime.now() >= orion_expiration){
            Authorized = false;
            //--------System.debug('CHECK ORION AUTH --> FALSE');
        } else {
            Authorized = true;
            //--------System.debug('CHECK ORION AUTH --> TRUE');
        }
        return Authorized;
    }
    
    public static void getOrionAuthCred(){
        orionAuthCred = [SELECT  usnm__c, pw__c, token__c, tokenExt__c, refreshToken__c, expiration__c, clientId__C, clientSecret__c
                         FROM OrionAuthCredential__c];
        if(orionAuthCred <> NULL){ 
            //--------System.debug('orionAuthCredential__c found: ' + String.valueOf(orionAuthCred.Id));
            orion_usnm            = orionAuthCred.usnm__c;
            orion_pw              = orionAuthCred.pw__c; 
            orion_clientid        = orionAuthCred.clientId__C;  
            orion_clientsecret    = orionAuthCred.clientSecret__c;  
            if(orionAuthCred.token__c <> NULL && orionAuthCred.refreshToken__c <> NULL){
                //createCryptKey(orionAuthCred.refreshToken__c);
                //orion_refresh_token   = orionAuthCred.refreshToken__c; 
                //orion_token           = orionAuthCred.token__c + decrypt(orionAuthCred.tokenExt__c);
                //orion_expiration      = orionAuthCred.expiration__c; 
            }
            
        } else {
            ErrorMsg = 'No orionAuthCredential__c found';
            //--------System.debug('ERROR: ' + ErrorMsg);
        }
    }
    
    // CRYPTO METHODS --------------------------------------------------------
    /*
    private static void createCryptKey(String strKey){
        //--------SYstem.debug('KEY CREATED');
        cryptKey = Blob.valueOf(strKey.left(16));
    }
    private static String encrypt(String rawData){
        //--------System.debug('ENCRYPTING ---------');
        //--------System.debug('--------' + rawData);
        String encryptedData;
        rawData = rawData.replace('.', '++PERIOD++');
        rawData = rawData.replace('_', '++UNDERSCORE++');
        rawData = rawData.replace('-', '++DASH++');
        Blob encryptInProg = Crypto.encryptWithManagedIV('AES128', cryptKey, EncodingUtil.base64Decode(rawData));
        encryptedData = EncodingUtil.base64Encode(encryptInProg);
        //--------System.debug('--------' + encryptedData);
        return encryptedData;
    }
    private static String decrypt(String rawData){
        //--------System.debug('DECRYPTING ---------');
        String decryptedData;
        Blob encryptInProg = Crypto.decryptWithManagedIV('AES128', cryptKey, EncodingUtil.base64Decode(rawData));
        decryptedData = EncodingUtil.base64Encode(encryptInProg);
        decryptedData = decryptedData.replace('++PERIOD++', '.');
        decryptedData = decryptedData.replace('++UNDERSCORE++', '_');
        decryptedData = decryptedData.replace('++DASH++', '-');
        decryptedData = decryptedData.removeEnd('=');
        //--------System.debug('--------' + rawData);
        //--------System.debug('--------' + decryptedData);
        return decryptedData;
    }
    // CRYPTO METHODS : END --------------------------------------------------------
    */
    private static void authOrionInit(){
        HTTP http = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(ORION_ENDPOINT+'/v1/Security/Token');
        Blob authHeader = Blob.valueOf(orion_usnm + ':' + orion_pw);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(authHeader);
        request.setHeader('Client_id', orion_clientid);
        request.setHeader('Client_secret', orion_clientsecret);
        request.setHeader('Authorization', authorizationHeader);
        request.setMethod('GET');
        HTTPResponse response = http.send(request);
        
        //--------System.debug(LoggingLevel.INFO, '**** TOKEN - status code: ' + response.getStatusCode());
        //--------System.debug(LoggingLevel.INFO, '**** TOKEN - body: ' + response.getBody());
        
        if(response.getStatusCode() == 200){ // auth success
            updateOrionAuthCred(response.getBody());
        } else {                            // auth failed
            ErrorMsg = 'API Call Failed. Status Code: ' + String.valueOf(response.getStatusCode()) + ' Status: ' + response.getBody();
            //--------System.debug(ErrorMsg);
        }
    }
    /*
    private void refreshOrionAuth(){    
        HTTP http = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(ORION_ENDPOINT+'/v1/Security/Token');
        request.setHeader('Client_id', orion_clientid);
        request.setHeader('Client_secret', orion_clientsecret);
        request.setHeader('Authorization', 'Bearer '+orion_refresh_token);
        request.setMethod('GET');
        HTTPResponse response = http.send(request);
        
        //--------System.debug(LoggingLevel.INFO, '**** REF TOKEN - status code: ' + response.getStatusCode());
        //--------System.debug(LoggingLevel.INFO, '**** REF TOKEN - body: ' + response.getBody());
        
        if(response.getStatusCode() == 200){ // auth success
            //updateOrionAuthCred(response.getBody());
            
        } else if(response.getStatusCode() == 401){ //Refresh token is rotten
            //--------System.debug('Error -- Status '+response.getStatusCode()+'. Calling authOrionInit()');
            authOrionInit();
            
        } else {                            // auth failed
            ErrorMsg = 'API Call Failed. Status Code: ' + String.valueOf(response.getStatusCode()) + ' Status: ' + response.getBody();
            //--------System.debug(ErrorMsg);
        }
    }
    */
    
    private static void updateOrionAuthCred(String respBody){
        Map<String, Object> authBody = (Map<String, Object>)JSON.deserializeUntyped(respBody);             
        
        String rawToken = String.valueOf(authBody.get('access_token'));
        
        orionAuthCred.token__c = rawToken.left(255);
        orionAuthCred.refreshToken__c = String.valueOf(authBody.get('refresh_token'));
        orionAuthCred.expiration__c = datetime.now().addSeconds(Integer.valueOf(authBody.get('expires_in')));
        
        //createCryptKey(String.valueOf(authBody.get('refresh_token')));
        //orionAuthCred.tokenExt__c = encrypt(rawToken.substring(255));
        
        
        orion_token         = rawToken;
        orion_refresh_token = orionAuthCred.refreshToken__c;
        orion_expiration    = orionAuthCred.expiration__c;
        
        IsorionAuthCredUpdated = true;
    }    

    
    
    public OrionApi_Client updateClientObject(OrionApi_Client oldClientObject, Id householdId){
        OrionApi_Client clientObject = oldClientObject;
        
        AccountContactRelation targetClient = [SELECT AccountId,ContactId,FinServ__Primary__c,PersonAccountid__c, 
                                               Account.name, Account.FinServ__Status__c, Account.Orion_Household_ID__c, Account.Orion_Rep_ID__c,
                                               Contact.account.Salutation,Contact.account.lastName,Contact.account.firstName,Contact.account.PersonHomePhone,Contact.account.PersonEmail,
                                               Contact.account.BillingStreet,Contact.account.BillingCity,Contact.account.BillingState,Contact.account.BillingPostalCode,
                                               Contact.account.billingCountry,Contact.account.PersonMobilePhone,Contact.account.Work_Phone__pc,Contact.account.PersonOtherPhone,Contact.account.name,
                                               Contact.account.Job_Title__pc,Contact.account.FinServ__Gender__pc,Contact.account.PersonBirthdate 
                                               FROM AccountContactRelation 
                                               WHERE FinServ__Primary__c = true AND IsActive = true AND AccountID =: householdId LIMIT 1];
        if(targetClient == NULL){
            ErrorMsg = 'ERROR: NO CLIENT FOUND/NO PRIMARY MEMBER FOUND';
            //--------System.debug(ErrorMsg+'----');
            return clientObject;
        } else if(targetClient.Account.Orion_Household_ID__c == NULL){
            ErrorMsg = 'ERROR: TARGET HOUSEHOLD NOT AN ORION HOUSEHOLD';
            //--------System.debug(ErrorMsg+'----');
            return clientObject;
        } else {
            clientObject.name                = targetClient.Account.Name;
            clientObject.reportName          = targetClient.Account.Name; //Marketing Mailing Name field?
            clientObject.isActive            = targetClient.Account.FinServ__Status__c == 'Active'? true: false;
            clientObject.representativeId    = targetClient.Account.Orion_Rep_ID__c == null? 1 :            Integer.valueOf(targetClient.Account.Orion_Rep_ID__c);
            clientObject.id                  = Integer.valueOf(targetClient.Account.Orion_Household_ID__c);
            clientObject.salutation          = targetClient.Contact.Account.Salutation == null? null :              targetClient.Contact.Account.Salutation;
            clientObject.lastName            = targetClient.Contact.Account.lastName == null? null :                targetClient.Contact.Account.lastName;
            clientObject.firstName           = targetClient.Contact.Account.firstName == null? null :               targetClient.Contact.Account.firstName;
            clientObject.homePhone           = targetClient.Contact.Account.PersonHomePhone == null? null :         targetClient.Contact.Account.PersonHomePhone;
            clientObject.email               = targetClient.Contact.Account.PersonEmail == null? null :             targetClient.Contact.Account.PersonEmail;
            clientObject.address1            = targetClient.Contact.Account.BillingStreet == null? null :           targetClient.Contact.Account.BillingStreet.split('\n')[0].replace('\n','');
            clientObject.address2            = targetClient.Contact.Account.BillingStreet.indexOf('\n') > 0?        targetClient.Contact.Account.BillingStreet.substring(targetClient.Contact.Account.BillingStreet.indexOf('\n')+1).replace('\n',', ') : null;
            clientObject.city                = targetClient.Contact.Account.BillingCity == null? null :             targetClient.Contact.Account.BillingCity;
            clientObject.state               = targetClient.Contact.Account.BillingState == null? null :            Util_dataValidation.State2Abbr(targetClient.Contact.Account.BillingState);
            clientObject.zip                 = targetClient.Contact.Account.BillingPostalCode == null? null :       targetClient.Contact.Account.BillingPostalCode;
            clientObject.country             = targetClient.Contact.Account.billingCountry == null? null :          targetClient.Contact.Account.billingCountry;
            clientObject.mobilePhone         = targetClient.Contact.Account.PersonMobilePhone == null? null :       targetClient.Contact.Account.PersonMobilePhone;
            clientObject.businessPhone       = targetClient.Contact.Account.Work_Phone__pc == null? null :          targetClient.Contact.Account.Work_Phone__pc;
            clientObject.otherPhone          = targetClient.Contact.Account.PersonOtherPhone == null? null :        targetClient.Contact.Account.PersonOtherPhone;
            clientObject.jobTitle            = targetClient.Contact.Account.Job_Title__pc == null? null :           targetClient.Contact.Account.Job_Title__pc;
            clientObject.gender              = targetClient.Contact.Account.FinServ__Gender__pc == null? null :     Util_dataValidation.Gender2Char(targetClient.Contact.Account.FinServ__Gender__pc);
            clientObject.address3            = null;
            clientObject.dob                 = targetClient.Contact.Account.PersonBirthdate == null? null:          String.valueOf(targetClient.Contact.Account.PersonBirthdate);
        }
        
        return clientObject;
    }
    
    
    //Orion - get client
    public Boolean getClient(Integer orionClientId){
        Boolean gotClient=false;
        
        HTTP http = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(ORION_ENDPOINT+'/v1/Portfolio/Clients/'+ orionClientId );
        request.setHeader('Authorization', 'Session '+ orion_token);
        request.setMethod('GET');
        HTTPResponse response = http.send(request);
        
        //--------System.debug(LoggingLevel.INFO, '**** CLIENT - status code: ' + response.getStatusCode());
        //--------System.debug(LoggingLevel.INFO, '**** CLIENT - body: ' + response.getBody());
        
        if(response.getStatusCode() == 200){
            String respBody = response.getBody();
            clientObj = OrionApi_Client.parse(respBody);
            //--------System.debug('SUCCESS!!!');
            gotClient = true;
        } else {
            //--------System.debug('FAILED !!!!!!!');
        }
        
        return gotClient;
    }
    
    //Orion - update Client
    public void updateClient(Id householdId, Integer OrionHouseholdId){
        
        //get initial client data first
        if(getClient(OrionHouseholdId)){
            //--------System.debug('*****old CLIENT : '+JSON.serialize(clientObj));
            OrionApi_Client updatedClientObj = updateClientObject(clientObj, householdId);
            
            //String updated client info
            String reqBody = JSON.serialize(updatedClientObj);
            //--------System.debug('*****CLIENT : '+JSON.serialize(updatedClientObj));
            
            //update OrionApi_Client object
            
            HTTP http = new HTTP();
            HTTPRequest request = new HTTPRequest();
            request.setEndpoint('https://api.orionadvisor.com/api/v1/Portfolio/Clients/'+ OrionHouseholdId);
            request.setMethod('PUT');
            request.setHeader('Authorization', 'Session '+ orion_token);
            request.setHeader('Content-Type', 'application/json');
            request.setBody(reqBody);
            request.setTimeout(120000);
            HTTPResponse response = http.send(request);
            
            //--------System.debug(LoggingLevel.INFO, '**** CLIENT UPDATE - status code: ' + response.getStatusCode());
            //--------System.debug(LoggingLevel.INFO, '**** CLIENT UPDATE - body: ' + response.getBody());
            
            if(response.getStatusCode() == 200){
                //--------System.debug('SUCCESS!!!');
                update_DML();
            } else {
                //--------System.debug('FAILED !!!!!!!');
            }
        } else {
            ErrorMsg = 'Client not found.';
            return;
        }
        
        
    }
    
    //Orion - get Account Verdose  (depreciated)
    public static Boolean getAccountVerbose(Integer orionAccountId){
        Boolean gotAccountVerbose = false;
        
        HTTP http = new HTTP();
        HTTPRequest request = new HTTPRequest();
        request.setEndpoint(ORION_ENDPOINT+'/v1/Portfolio/Accounts/Verbose/'+ orionAccountId+'?expand=4' );
        request.setHeader('Authorization', 'Session '+ orion_token);
        request.setMethod('GET');
        HTTPResponse response = http.send(request);
        
        //--------System.debug(LoggingLevel.INFO, '**** CLIENT - status code: ' + response.getStatusCode());
        //--------System.debug(LoggingLevel.INFO, '**** CLIENT - body: ' + response.getBody());
        
        if(response.getStatusCode() == 200){
            String respBody = response.getBody();
            String accountModelingInfo = respBody.substring(respBody.indexOf('modeling')-1,respBody.indexOf('"notes"'));
            accountVerboseStr = '{"id":'+orionAccountId+','+accountModelingInfo+'"entityOptions":null}';
            System.debug('SUCCESS!!!'+accountVerboseStr);
            gotAccountVerbose = true;
        } else {
            //--------System.debug('FAILED !!!!!!!');
        }
        
        
        return gotAccountVerbose;
    }
    // (depreciated)
    public static void updateaccountVerboseStr(Integer orionAccountId){
        TargetFinAcc = new FinServ__FinancialAccount__c();
        TargetFinAcc = [SELECT id, Orion_Financial_Account_ID__c, Capital_Gains_Reinvestment__c, Dividend_Reinvestment__c, Trading_Blocked__c, Trading_Instructions__c
                                               FROM FinServ__FinancialAccount__c
                                               WHERE Orion_Financial_Account_ID__c =: String.valueOf(orionAccountId) limit 1];
        
        if(accountVerboseStr <> null && accountVerboseStr.indexOf('"entityOptions":null')>0 && TargetFinAcc <> null){
            String entityOptionStr = '"entityOptions": [{"value": "'+ TargetFinAcc.Dividend_Reinvestment__c +'","name": "Dividends Reinvested","entityId": '+ orionAccountId +',"entity": "Account","code": "1:69DIVIDE"},{"value": "'+ TargetFinAcc.Capital_Gains_Reinvestment__c +'","name": "Capital Gains Reinvested","entityId": '+ orionAccountId +',"entity": "Account","code": "69CAPITALG"}]';
        	accountVerboseStr = accountVerboseStr.replace('"entityOptions":null', entityOptionStr);
            //--------System.debug('Updated----------'+accountVerboseStr);
        }
        if(accountVerboseStr <> null && accountVerboseStr.indexOf('"modelingInfo"')>0 && TargetFinAcc <> null){
            if(accountVerboseStr.indexOf('"isTradingBlocked":'+!TargetFinAcc.Trading_Blocked__c)>0){
                String tradingBlockStr = '"isTradingBlocked":'+ TargetFinAcc.Trading_Blocked__c ;
                accountVerboseStr = accountVerboseStr.replace('"isTradingBlocked":'+!TargetFinAcc.Trading_Blocked__c, tradingBlockStr);
                //--------System.debug('Updated----------'+accountVerboseStr);
            }
            if(accountVerboseStr.indexOf('"tradingInstructions"')>0 && TargetFinAcc <> null){
                String tradingInstructionsStr = '"tradingInstructions":'+TargetFinAcc.Trading_Instructions__c;
                
                String accountVerboseStr_tiStr = accountVerboseStr.substring(accountVerboseStr.indexOf('"tradingInstructions"'));
                accountVerboseStr_tiStr = accountVerboseStr_tiStr.left(accountVerboseStr_tiStr.indexOf(','));
                //--------System.debug('accountVerboseStr_tiStr: '+accountVerboseStr_tiStr);
                accountVerboseStr = accountVerboseStr.replace(accountVerboseStr_tiStr,tradingInstructionsStr);
                //--------System.debug('accountVerboseStr: '+accountVerboseStr);
            }
        }
    }
    
    //Orion - Update account (depreciated)
    @auraEnabled
    public static void updateAccountVerbose(Integer orionAccountId){
        getOrionAuthCred();
        authOrionInit();
        if(getAccountVerbose(orionAccountId)){
            updateaccountVerboseStr(orionAccountId);
            
            HTTP http = new HTTP();
            HTTPRequest request = new HTTPRequest();
            request.setEndpoint('https://api.orionadvisor.com/api/v1/Portfolio/Accounts/Verbose/'+ orionAccountId);
            request.setMethod('PUT');
            request.setHeader('Authorization', 'Session '+ orion_token);
            request.setHeader('Content-Type', 'application/json');
            request.setBody(accountVerboseStr);
            HTTPResponse response = http.send(request);
            
            //--------System.debug(LoggingLevel.INFO, '**** Account UPDATE - status code: ' + response.getStatusCode());
            //--------System.debug(LoggingLevel.INFO, '**** Account UPDATE - body: ' + response.getBody());
            
            if(response.getStatusCode() == 200){
                //--------System.debug('SUCCESS!!!');
            } else {
                //--------System.debug('FAILED !!!!!!!');
            }
        } else {
            ErrorMsg = 'Financial Account not found.';
            return;
        }
        update_DML();
    }
    
    //Orion - Update account (invocable) 
    @InvocableMethod(label='Update FA in Orion' description='Send Callout to Orion to update target financial accounts.')
    public static void updateAccountVerbose_Inv(List<Id> financialAccountIds){
        Set<Id> finAccIdSet = new Set<Id>(financialAccountIds);
        updateAccountVerbose_Bulk(finAccIdSet);
    }
    
    //Orion - Update account (Bulk)
    @future(callout=true)
    public static void updateAccountVerbose_Bulk(Set<Id> financialAccountIds){
        Boolean orionPushFailed = false;
        List<Sync_Error__c> syncErrorList = new List<Sync_Error__c>();
        List<FinServ__FinancialAccount__c> TargetFinAccList = [SELECT id, Orion_Financial_Account_ID__c, Capital_Gains_Reinvestment__c, Dividend_Reinvestment__c, Trading_Blocked__c, Trading_Instructions__c, Include_in_aggregate__c,Pay_Method_Storage__c
                        FROM FinServ__FinancialAccount__c
                        WHERE Id in: financialAccountIds];
        
        getOrionAuthCred();
        authOrionInit();
        
        for(FinServ__FinancialAccount__c financialAccount: TargetFinAccList){
            Integer orionAccountId = financialAccount.Orion_Financial_Account_ID__c <> null? Integer.valueOf(financialAccount.Orion_Financial_Account_ID__c) : 0;
            String AccVerbStr = getAccountVerbose_Bulk(orionAccountId);
            if(AccVerbStr.length()>1){
                AccVerbStr = updateaccountVerboseStr_Bulk(AccVerbStr, financialAccount);
                
                HTTP http = new HTTP();
                HTTPRequest request = new HTTPRequest();
                request.setEndpoint('https://api.orionadvisor.com/api/v1/Portfolio/Accounts/Verbose/'+ orionAccountId);
                request.setMethod('PUT');
                request.setHeader('Authorization', 'Session '+ orion_token);
                request.setHeader('Content-Type', 'application/json');
                request.setBody(AccVerbStr);
                request.setTimeout(120000);
                HTTPResponse response = http.send(request);
                
                //--------System.debug(LoggingLevel.INFO, '**** Account UPDATE - status code: ' + response.getStatusCode());
                //--------System.debug(LoggingLevel.INFO, '**** Account UPDATE - body: ' + response.getBody());
                
                if(response.getStatusCode() == 200){
                    //--------System.debug('SUCCESS!!!');
                } else {
                    //--------System.debug('FAILED !!!!!!!');
                    ErrorMsg = 'Request------\nRequest endpoint:' + request.getEndpoint() + '\nRequest body: '+request.getBody()+'Response----\nResponse Status Code: '+ response.getStatusCode() + '\nResponse Body:' + response.getBody();
                    syncErrorList.add(finAccSyncError(financialAccount.Id,ErrorMsg));
                    orionPushFailed = true;
                }
            } else {
                ErrorMsg = 'Financial Account not found in Orion.';
                syncErrorList.add(finAccSyncError(financialAccount.Id,ErrorMsg));
                orionPushFailed = true;
            }
        }
        if(orionPushFailed) insert_DML_List(syncErrorList);
    }
    
    private static Sync_Error__c finAccSyncError(Id RecId, String errorMessage){
        Sync_Error__c syncError = new Sync_Error__c(SyncErrorMessage__c = errorMessage,
                                                   SyncErrorTimestamp__c = Datetime.now(),
                                                   RecordID__c = RecId);
        return syncError;
    }
    
    
    public static String getAccountVerbose_Bulk(Integer orionAccountId){
        String AccVerbStr='';
        if(orionAccountId <> null && orionAccountId > 1){
            
            HTTP http = new HTTP();
            HTTPRequest request = new HTTPRequest();
            request.setEndpoint(ORION_ENDPOINT+'/v1/Portfolio/Accounts/Verbose/'+ orionAccountId+'?expand=1&expand=4' );
            request.setHeader('Authorization', 'Session '+ orion_token);
            request.setMethod('GET');
            request.setTimeout(120000);
            HTTPResponse response = http.send(request);
            
            //--------System.debug(LoggingLevel.INFO, '**** CLIENT - status code: ' + response.getStatusCode());
            //--------System.debug(LoggingLevel.INFO, '**** CLIENT - body: ' + response.getBody());
            
            if(response.getStatusCode() == 200){
                String respBody = response.getBody();
                String billingInfo = respBody.substring(respBody.indexOf('"billing"'),respBody.indexOf('"portfolio"'));
                String accountModelingInfo = respBody.substring(respBody.indexOf('"modeling'),respBody.indexOf('"notes"'));
                AccVerbStr = '{"id":'+orionAccountId+','+accountModelingInfo+billingInfo+'"entityOptions":null}';
                System.debug('SUCCESS!!!'+AccVerbStr);
            } else {
                //--------System.debug('FAILED !!!!!!!');
            }
        }
        return AccVerbStr;
    }
    
    
    public static String updateaccountVerboseStr_Bulk(String AccVerbStr, FinServ__FinancialAccount__c finAccRec){
        
        if(AccVerbStr <> null && AccVerbStr.indexOf('"entityOptions":null')>0 && finAccRec <> null){
            String entityOptionStr = '"entityOptions": [{"value": "'+ finAccRec.Dividend_Reinvestment__c +'","name": "Dividends Reinvested","entityId": '+ finAccRec.Orion_Financial_Account_ID__c +',"entity": "Account","code": "1:69DIVIDE"},{"value": "'+ finAccRec.Capital_Gains_Reinvestment__c +'","name": "Capital Gains Reinvested","entityId": '+ finAccRec.Orion_Financial_Account_ID__c +',"entity": "Account","code": "69CAPITALG"}]';
        	AccVerbStr = AccVerbStr.replace('"entityOptions":null', entityOptionStr);
            //--------System.debug('Updated----------'+AccVerbStr);
        }
        if(finAccRec <> null && AccVerbStr <> null && AccVerbStr.indexOf('"modelingInfo"')>0){
            if(AccVerbStr.indexOf('"isTradingBlocked":'+!finAccRec.Trading_Blocked__c)>0){
                String tradingBlockStr = '"isTradingBlocked":'+ finAccRec.Trading_Blocked__c ;
                AccVerbStr = AccVerbStr.replace('"isTradingBlocked":'+!finAccRec.Trading_Blocked__c, tradingBlockStr);
                //--------System.debug('Updated----------'+AccVerbStr);
            }
            if(AccVerbStr.indexOf('"tradingInstructions"')>0){
                String tradingInstructionsStr = '"tradingInstructions":"'+finAccRec.Trading_Instructions__c+'"';
                
                String accountVerboseStr_tiStr = AccVerbStr.substring(AccVerbStr.indexOf('"tradingInstructions"'));
                accountVerboseStr_tiStr = accountVerboseStr_tiStr.left(accountVerboseStr_tiStr.indexOf(','));
                //--------System.debug('accountVerboseStr_tiStr: '+accountVerboseStr_tiStr);
                AccVerbStr = AccVerbStr.replace(accountVerboseStr_tiStr,tradingInstructionsStr);
                //--------System.debug('AccVerbStr: '+AccVerbStr);
            }
        }
        
        if(AccVerbStr <> null && finAccRec <> null && AccVerbStr.indexOf('"billing"')>0){
            if(AccVerbStr.indexOf('"includeInAggregate":'+!finAccRec.Include_in_aggregate__c)>0){
                String includeInAggregateStr = '"includeInAggregate":'+finAccRec.Include_in_aggregate__c;
                AccVerbStr = AccVerbStr.replace('"includeInAggregate":'+!finAccRec.Include_in_aggregate__c, includeInAggregateStr);
            }
            /* for future Pay Method update callout
            if(AccVerbStr.indexOf('"payMethodId"')>0){
                String payMethodIdStr = '"payMethodId":'+finAccRec.Pay_Method_Storage__c;
                String accountVerboseStr_pmiStr = AccVerbStr.substring(AccVerbStr.indexOf('"payMethodId"'));
                accountVerboseStr_pmiStr = accountVerboseStr_pmiStr.left(accountVerboseStr_pmiStr.indexOf(','));
                String accountVerboseStr_pmStr = AccVerbStr.substring(AccVerbStr.indexOf('"payMethod"'));
                accountVerboseStr_pmStr = accountVerboseStr_pmStr.left(accountVerboseStr_pmStr.indexOf(',')+1);
                AccVerbStr = AccVerbStr.replace(accountVerboseStr_pmiStr, payMethodIdStr);
                AccVerbStr = AccVerbStr.replace(accountVerboseStr_pmStr, '');
            }
			*/
        }
        
        return AccVerbStr;
    }
    
    private static void update_DML(){
        //if(IsorionAuthCredUpdated <> null && IsorionAuthCredUpdated) update orionAuthCred;
    }
    
    private static void insert_DML_List(List<sObject> objList){
        Insert objList;
    }
}