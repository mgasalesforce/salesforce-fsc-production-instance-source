global without sharing class PersonAccountSearch_PostToChatter {
	public static void PostToChatter(List<AccountContactRelation> acrList) 
	{
     for(AccountContactRelation ah:acrList) 
        {
                    
            ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
            ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
            ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
            ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

            messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

            mentionSegmentInput.id = ah.Account.Client_Advisor__c; 
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            
	        textSegmentInput.text = '  ' + ah.Contact.Name+ ' called in and ' + UserInfo.getName() + ' provided with their CSU Contact information ';
            messageBodyInput.messageSegments.add(textSegmentInput);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = ah.Account.id; 

            ConnectApi.FeedElement feedElementHouseholdAccount = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
            
        }        		

	}

    @future(callout=true)
    public static void mention(string uid,string recordId, string postText){
        	//INVOKE CHATTER REST API FOR MENTION
        	String salesforceHost = System.Url.getSalesforceBaseURL().toExternalForm();
        	String url =  salesforceHost + '/services/data/v45.0/chatter/feeds/record/' + recordId + '/feed-items';
        	HttpRequest req = new HttpRequest();
        	req.setMethod('POST');
        	req.setEndpoint(url);
        	string ChatterPostText=postText;
        	req.setHeader('Content-type', 'application/json');
        	req.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
        	req.setBody('{ "body" : { "messageSegments" : [ { "type": "mention", "id" : "' + uid + '" }, { "type": "text",  "text" : "' + ' ' + ChatterPostText +  '" } ] } }');
        	Http http = new Http();
        	HTTPResponse res = http.send(req);           
    } 
   

	public static void insertFeedItem (List<AccountContactRelation> acrList) 
	{

	List<FeedItem> listFeedItem = new List<FeedItem>();

	string sMention;

     for(AccountContactRelation ah:acrList) 
        {
                                
			FeedItem postHousehold = new FeedItem();
			postHousehold.createdById = UserInfo.getUserId();
			postHousehold.ParentId = ah.Account.id;
			postHousehold.Type = 'TextPost';	
			postHousehold.Body = '  ' + ah.Contact.Name+ ' called in and ' + UserInfo.getName() + ' provided with their CSU Contact information ';
			listFeedItem.add(postHousehold);

			FeedItem postPersonAccount = new FeedItem();
			postPersonAccount.createdById = UserInfo.getUserId();
			postPersonAccount.ParentId = ah.ContactId;
			postPersonAccount.Type = 'TextPost';			
			postPersonAccount.Body = '  ' + ah.Contact.Name+ ' called in and ' + UserInfo.getName() + ' provided with their CSU Contact information ';
			listFeedItem.add(postPersonAccount);

			sMention = ah.Account.Client_Advisor__c;

        }    

		if(listFeedItem.size()>0)
		{
			insert listFeedItem;
		}    		

		//for (FeedItem item:listFeedItem)
		//{
			//String sMention = item.RelatedRecordId;
		//	ConnectApi.Comment fic = ConnectApiHelper.postCommentWithMentions(null, item.Id, sMention);
			//mention(item.CreatedById,item.id, item.body);
		//}

	}



}