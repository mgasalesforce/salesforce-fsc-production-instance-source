/*
This file is generated and isn't the actual source code for this
managed global class.
This read-only file shows the class's global constructors,
methods, variables, and properties.
To enable code to compile, all methods return null.
*/
global class BuildRecurringWrapper {
    global SObject captureObject {
        get;
        set;
    }
    global FSTR__Recurring_Business_Process__c rbpRecord {
        get;
        set;
    }
    global BuildRecurringWrapper(FSTR__Recurring_Business_Process__c r, SObject s) {

    }
}
