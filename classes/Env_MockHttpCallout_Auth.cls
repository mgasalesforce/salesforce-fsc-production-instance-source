@isTest

global class Env_MockHttpCallout_Auth implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HttpResponse res = new HttpResponse();
        
        // API call return
        res.setStatusCode(200);
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Token', 'eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJFTlYiLCJzdWIiOiIyMzk1MDk6MjplbnZlc3RuZXQiLCJyb2xlIjoiYWR2aXNvciIsInBsYXRmb3JtTW9kZSI6ZmFsc2UsInJ0aSI6Ijk4ZDkzMDc3ZTc0Mzg4YWFjMWVlZTgzYjY2YzAzM2RlIiwianRpIjoiNTc2ZTYyNDMtYzZjMS00OWU3LWI0YWQtM2VhMDdmNTc3NDc1IiwiaWF0IjoxNTY4ODY1MDI4LCJleHAiOjE1Njg4Njg2Mjh9.YpRsUIDDc9IxfkpzzcmMfmJfsRpINqJ-ti3aFjxuX8tB27kHZEMJcIYoULkWyQ9mTfKU5sTu4jtWvfq5Q5CVp5T8lVrNYwbcKShrdnuhvJS1pKzCUzY4gi5Ighoe1bqHO_g8xSXlz2t-sYmJ4-IzPqKx5EWDK8ru1AKANb0Au8AeZe_jUA9h8FZsDggZ_iu6zTmZ2xF-un6OMrfnclXjh2UerLICVz3uuY7fRy0KRswk4ze_6FVCqvw_UqkJdt-dyuc471BAK4o5XqxNwngy41LU8rJtcqJ8p2E5cM2nNr7iH1WtfAsHuyY413e73xhEWTsnHDDFyylRfL_kmmZSpQ');
        res.setHeader('Refresh-Token', 'MAarV0KwuvZzn9xfqS79HyM4Ype4fwSJ');
        res.setBody('{"userHandle":"1~2~239509","firstName":"Soobin\'s","lastName":"Advisor"}');   
       
        return res;
    }
}