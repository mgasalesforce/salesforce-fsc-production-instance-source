public class QueryRecordType {
    public static Map<String, String> recordTypeMap {get; set;}
    
    @AuraEnabled
    public static Map<String, String> GetRecordTypeMap(String sobjectName){
        System.debug('Init setrectypemap');
        recordTypeMap = new Map<String, String>();
        List<RecordType> listRec = new List<RecordType>();
        for(RecordType rec : [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType =: sobjectName]) {
            recordTypeMap.put(rec.DeveloperName, rec.Id);
        System.debug('Init setrectypemap--'+rec.DeveloperName+':'+ rec.Id);
        }
        return recordTypeMap;
    }
}