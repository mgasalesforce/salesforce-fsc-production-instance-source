// @description Test Class for Code Coverage for searchAllAccountContactRelations 
// @author Praveen Kanumuri
// @date 8-06-2019

@IsTest(SeeAllData=true)
public class searchAllAccountContactRelationsTest 
{
  static testMethod void testsearchAllAccountContactRelations() 
  {
  

                Branch__c b = new Branch__c();
                b.Name = 'TestBranch';
                b.Branch_Manager__c = UserInfo.getUserId();
                insert b;

                Client_Service_Unit__c csu = new Client_Service_Unit__c();
                csu.branch__c = b.id;
                csu.Client_Advisor__c = UserInfo.getUserId();
                csu.Client_Associate__c = UserInfo.getUserId();
                csu.Name = 'CSU';
                insert csu;

                Id HhRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Household').getRecordTypeId();

                Account ah = new Account();
                ah.Name = 'TestHousehold';
                ah.Phone = '1111';
                ah.BillingCity = '1111xxx';
                ah.BillingState = 'GA';
                ah.recordTypeId = HhRecordTypeId;
                ah.FinServ__Status__c = 'Active';
                ah.Client_Service_Unit__c = csu.id;
                insert ah;

                Id RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Person Account').getRecordTypeId();

                Account a = new Account();
                a.FirstName = 'PersonFirstName';
                a.LastName = 'PersonLastName';
                a.Phone = '2222';
                a.BillingCity = 'TestCity';
                a.BillingState = 'MI';
                a.recordTypeId = RecordTypeId;
                a.FinServ__Status__c = 'Active';
                a.PersonMailingStreet='test@yahoo.com';
                a.PersonMailingPostalCode='12345';
                a.PersonMailingCity='SFO';
                a.PersonEmail='test@yahoo.com';
                a.PersonHomePhone='1234567';
                a.PersonMobilePhone='12345678';

                insert a;


                contact con = [select Id from Contact where FirstName = 'PersonFirstName'];
  
                AccountContactRelation acr = new AccountContactRelation();
                acr.AccountId = ah.id; 
                acr.isActive = True;
                acr.ContactId = con.id;
                insert acr;
                
                List<Id> lstConId = new List<Id>();
                lstConId.add(con.id);

                List<List<AccountContactRelation>> lststr = searchAllAccountContactRelations.searchAllAccountContactRelations(lstConId);

                System.assertEquals(1, lststr.size());


 
  }

        
}