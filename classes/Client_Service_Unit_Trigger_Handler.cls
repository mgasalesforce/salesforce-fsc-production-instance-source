/**
 * @File Name          : Client_Service_Unit_Trigger_Handler.apxc
 * @Description        : Trigger handler class for Client Service Unit object to create relavant CSU public group/queue
 * @Author             : soobin.kittredge@merceradvisors.com
 * @Group              : Mercer Advisors
 * @Last Modified By   : soobin.kittredge@merceradvisors.com
 * @Last Modified On   : 8/2/2019
 * @Modification Log   : 
 *==============================================================================
 * Ver         Date                     Author      		      Modification
 *==============================================================================
 * 1.0  	  8/2/2019	  soobin.kittredge@merceradvisors.com     Initial draft
 * 1.1		12/20/2019	  soobin.kittredge@merceradvisors.com	  Special character handling
**/

public class Client_Service_Unit_Trigger_Handler {
	public Client_Service_Unit_Trigger_Handler(){
		//Constructor logic
    }
	
    // Trigger handler methods : Start ------------------------------------------------------------
	public void afterInsert(List<Client_Service_Unit__c> newcsu){
		//List<Client_Service_Unit__c> csuList = (List<Client_Service_Unit__c>) newcsu;
		System.debug('---- CSU Created -----------------------');
		groupCreation(newcsu);
	}
	public void afterUpdate(Map<Id,Client_Service_Unit__c> updatedcsu, Map<Id, Client_Service_Unit__c> oldcsu){
		System.debug('---- CSU Updated -----------------------');
        if(updatedcsu<>NULL)groupUpdate(updatedcsu,oldcsu);
	}
	public void afterDelete(Map<Id, Client_Service_Unit__c> oldcsu){
		System.debug('---- CSU Delete -----------------------');
        groupUpdate(NULL,oldcsu);
	}
	public void afterUndelete(Map<Id, Client_Service_Unit__c> undeletedcsu){
		System.debug('---- CSU undelete -----------------------');
        groupUpdate(undeletedcsu,NULL);
	}
    // Trigger handler methods : end ----------------------------------------------------------------

    // Group related methods : start ----------------------------------------------------------------
    public void groupCreation(List<Client_Service_Unit__c> csuList){
		List<Group> groupToCreate = new List<Group>();
        Map<String, Id> gmAdvisors = new Map<String, Id>();
        Map<String, Id> gmAssociates = new Map<String, Id>();
                
        for(Client_Service_Unit__c csu: csuList){
            system.debug('*--- CSU: '+csu.name );
            //Group fields: DeveloperName, DoesIncludeBosses, DoesSendEmailToMembers, Email, Name, Type
            String groupName = 'CSU '+csu.name ;
            Group newCsuGroup = new Group(Name = groupName,
                                    DeveloperName = groupName.deleteWhitespace().replaceAll('[^a-zA-Z0-9\\s+]', '').replaceAll('\\p{Punct}',''),
                                    DoesSendEmailToMembers = true,
                                    Type = 'Queue');
            if(csu.Status__c == 'Inactive'){
                newCsuGroup.name = newCsuGroup.name + ' (Inactive)';
            }
            System.debug('**** '+newCsuGroup.name);
            groupToCreate.add(newCsuGroup);
        }
        
        if(groupToCreate.size()>0){
            insert groupToCreate;
            Set<Id> groupCreatedIds = (new Map<Id, Group>(groupToCreate)).keyset();
            Set<Id> newCsuIds = (new Map<Id, Client_Service_Unit__c>(csuList)).keyset();
            
            //setup object passed to future method
            Client_Service_Unit_SessionHandler.createQueueSobj(groupCreatedIds);
            Client_Service_Unit_SessionHandler.groupMemberDml(groupCreatedIds,newCsuIds);
        }
    }
    
    public void groupUpdate(Map<Id, Client_Service_Unit__c> newCsuMap, Map<Id, Client_Service_Unit__c> oldCsuMap){
		List<Group> groupToUpdate = new List<Group>();
        List<String> csuGroupStatusChange = new List<String>();
        List<String> csuGroupStatusChangeActive = new List<String>();
        List<String> csuGroupMemberChange = new List<String>();
        Map<String, id> csuGroupMember2Remove = new Map<String, id>();
        Map<String, id> csuGroupMember2Add = new Map<String, id>();
        Client_Service_Unit__c oldCsu;
        Client_Service_Unit__c newCsu;
        
        if(oldCsuMap <> NULL){
            for(id csuId : oldCsuMap.keySet()){
                oldCsu = oldCsuMap.get(csuId);
                
                if(newCsuMap <> NULL) newCsu = newCsuMap.get(csuId);
                
                System.debug('****---- CSU: '+oldCsu.Client_Advisor__c+oldCsu.Status__c);
                
                if(newCsu == NULL){ 									//deleted
                    csuGroupStatusChange.add('CSU '+oldCsu.Name);
                    System.debug('---- CSU Deleted ');
                } else {
                    if(newCsu.Status__c == 'Inactive' && oldCsu.Status__c <> newCsu.Status__c){					//turn inactive
                        csuGroupStatusChange.add('CSU '+oldCsu.Name);
                        System.debug('---- CSU turned inactive ');
                    }
                    if(newCsu.Status__c == 'Active' && oldCsu.Status__c <> newCsu.Status__c){		//turn active
                        csuGroupStatusChangeActive.add('CSU ' + newCsu.Name + ' (Inactive)');
                        System.debug('---- CSU reactivated ');
                    }
                    if(oldCsu.Client_Associate__c <> newCsu.Client_Associate__c){	//change member
                    csuGroupMemberChange.add('CSU '+oldCsu.Name);
                    csuGroupMember2Remove.put(('CSU '+oldCsu.Name),oldCsu.Client_Associate__c);
                    csuGroupMember2Add.put(('CSU '+oldCsu.Name),newCsu.Client_Associate__c);
                    System.debug('---- CSU Associate changed ');
                }
                }
            } 
        } else if(newCsuMap <> NULL){
            for(id csuId : newCsuMap.keySet()){
                newCsu = newCsuMap.get(csuId);	//undeleted
                csuGroupStatusChangeActive.add('CSU ' + newCsu.Name + ' (Inactive)');
                System.debug('---- CSU Undeleted ');
            }
        }
               
        
        //soql group to make change
        if(csuGroupMemberChange.size()>0){
       		List<Group> group2ChangeMember = [SELECT Id, Name FROM Group where name in :csuGroupMemberChange];
            Set<Id> group2ChangeMemberIds = (new Map<Id, Group>(group2ChangeMember)).keyset();
            List<GroupMember> newGroupMembers = new List<GroupMember>();
            Set<Id> removeGroupMembers = new Set<Id>();
            Map<id, id> groupMemberToAddMap = new Map<Id, Id>();
            
            if(csuGroupMember2Add.size()>0){
                for(Group g: group2ChangeMember){
                    groupMemberToAddMap.put(g.id,csuGroupMember2Add.get(g.name));
                }            
            	Client_Service_Unit_SessionHandler.groupMemberUpdateDml(groupMemberToAddMap);
            }
            
            if(csuGroupMember2Remove.size()>0){
                List<GroupMember> removeGroupMemberList = [SELECT Id FROM GroupMember 
                                     			WHERE GroupId In :group2ChangeMemberIds AND UserOrGroupID in :csuGroupMember2Remove.values()];
                System.debug('*--- GM to delete '+removeGroupMemberList.size());
                for(GroupMember gm: removeGroupMemberList){
                    removeGroupMembers.add(gm.id);
                }
                if(removeGroupMembers.size()>0) Client_Service_Unit_SessionHandler.groupMemberDeleteDml(removeGroupMembers);
            }
        }
        
        if(csuGroupStatusChange.size()>0){
       		List<Group> group2ChangeStatus = [SELECT Id, Name FROM Group WHERE name in :csuGroupStatusChange];
            List<Id> group2ChangeStatusIds = new List<Id>();
            for(Group g: group2changeStatus){
                g.name = g.name + ' (Inactive)';
                groupToUpdate.add(g);
                group2ChangeStatusIds.add(g.id);
            }
            
            List<GroupMember> inactiveGroupmemberRemove = [SELECT Id FROM GroupMember WHERE GroupId In :group2ChangeStatusIds];
            Set<Id> inactiveGroupmemberRemoveIds = (new Map<Id,GroupMember>(inactiveGroupmemberRemove)).keyset();
            if(inactiveGroupmemberRemoveIds.size()>0) Client_Service_Unit_SessionHandler.groupMemberDeleteDml(inactiveGroupmemberRemoveIds);
        }
        
        if(csuGroupStatusChangeActive.size()>0){
       		List<Group> group2ChangeStatusActive = [SELECT Id, Name FROM Group where name in :csuGroupStatusChangeActive];
            for(Group g: group2ChangeStatusActive){
                if(g.name.substring(g.name.length()-11) == ' (Inactive)'){
                    g.name = g.name.substring(0,g.name.length()-11);
                    groupToUpdate.add(g);
                }
            }
            Set<Id> group2ChangeStatusActiveMemberDmlIds = (new Map<Id,Group>(group2ChangeStatusActive)).keyset();
            Set<Id> csu2ChangeStatusActiveMemberDmlIds = (new Map<Id, Client_Service_Unit__c>(newCsuMap)).keyset();
            Client_Service_Unit_SessionHandler.groupMemberDml(group2ChangeStatusActiveMemberDmlIds, csu2ChangeStatusActiveMemberDmlIds);                                                                        
        }
        
        if(groupToUpdate.size()>0) update groupToUpdate;
    }
    // Group related methods : end   ----------------------------------------------------------------
}