/**
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┐
 * Test class for invocable apex designed to clone action plan templates.
 * 
 * Provides test coverage for the action plan template invocable method.
 * ──────────────────────────────────────────────────────────────────────────────────────────────────
 * @author: John Crapuchettes - john@zennify.com
 * @created: 2019-08-28
 * @modifiedBy: John Crapuchettes - john@zennify.com
 * @modifiedDate: 2019-08-28
 * ─────────────────────────────────────────────────────────────────────────────────────────────────┘
 */

@isTest
public class CloneActionPlanTemplateInvocableTest {
    @isTest
    public static void TestClone()
    {
        //Test data creation
        
        ActionPlanTemplate apt1 = new ActionPlanTemplate();
        apt1.Name = 'Test 1';
        apt1.ActionPlanType = 'Industries';
        apt1.TargetEntityType = 'Account';
        List<User> userAdmin = [SELECT ID, Name, Profile.Name FROM User WHERE Profile.Name = 'System Administrator' AND isActive = True LIMIT 1];
        if(userAdmin.size()> 0)
        {
            apt1.OwnerId = userAdmin[0].id;
        }
        insert apt1;
        
        ActionPlanTemplateVersion aptv = [SELECT ID, Name FROM ActionPlanTemplateVersion WHERE ActionPlanTemplateID =: apt1.Id LIMIT 1];
        
        List<ActionPlanTemplateItem> apti = new List<ActionPlanTemplateItem>();
        ActionPlanTemplateItem apti1 = new ActionPlanTemplateItem();
        apti1.Name = 'Item 1';
        apti1.ItemEntityType = 'Task';
        apti1.ActionPlanTemplateVersionId = aptv.Id;
        apti.add(apti1);
        
        ActionPlanTemplateItem apti2 = new ActionPlanTemplateItem();
        apti2.Name = 'Item 2';
        apti2.ItemEntityType = 'Task';
        apti2.ActionPlanTemplateVersionId = aptv.Id;
        apti.add(apti2);
        
        insert apti;
        
        List<ActionPlanTemplateItemValue> aptiv = new List<ActionPlanTemplateItemValue>();
        ActionPlanTemplateItemValue apti1v1 = new ActionPlanTemplateItemValue();
        apti1v1.Name = 'Subject';
        apti1v1.ActionPlanTemplateItemId = apti1.id;
        apti1v1.ItemEntityFieldName = 'Task.Subject';
        apti1v1.ValueLiteral = 'Test 1';
        aptiv.add(apti1v1);
        insert apti1v1;
        
        ActionPlanTemplateItemValue apti1v2 = new ActionPlanTemplateItemValue();
        apti1v2.Name = 'Priority';
        apti1v2.ActionPlanTemplateItemId = apti1.id;
        apti1v2.ItemEntityFieldName = 'Task.Priority';
        apti1v2.ValueLiteral = 'Normal';
        aptiv.add(apti1v2);
        insert apti1v2;
      
        ActionPlanTemplateItemValue apti1v3 = new ActionPlanTemplateItemValue();
        apti1v3.Name = 'Description';
        apti1v3.ActionPlanTemplateItemId = apti1.id;
        apti1v3.ItemEntityFieldName = 'Task.Description';
        apti1v3.ValueLiteral = 'A long value';
        aptiv.add(apti1v3);
        insert apti1v3;
        
        ActionPlanTemplateItemValue apti1v4 = new ActionPlanTemplateItemValue();
        apti1v4.Name = 'ActivityDate';
        apti1v4.ActionPlanTemplateItemId = apti1.id;
        apti1v4.ItemEntityFieldName = 'Task.ActivityDate';
        apti1v4.ValueFormula = 'StartDate + 1';
        aptiv.add(apti1v4);
        insert apti1v4;
        
        ActionPlanTemplateItemValue apti2v1 = new ActionPlanTemplateItemValue();
        apti2v1.Name = 'Subject';
        apti2v1.ActionPlanTemplateItemId = apti2.id;
        apti2v1.ItemEntityFieldName = 'Task.Subject';
        apti2v1.ValueLiteral = 'Test 2';
        aptiv.add(apti2v1);
        insert apti2v1;
        
        ActionPlanTemplateItemValue apti2v2 = new ActionPlanTemplateItemValue();
        apti2v2.Name = 'Priority';
        apti2v2.ActionPlanTemplateItemId = apti2.id;
        apti2v2.ItemEntityFieldName = 'Task.Priority';
        apti2v2.ValueLiteral = 'Normal';
        aptiv.add(apti2v2);
        insert apti2v2;
        
        ActionPlanTemplateItemValue apti2v3 = new ActionPlanTemplateItemValue();
        apti2v3.Name = 'Description';
        apti2v3.ActionPlanTemplateItemId = apti2.id;
        apti2v3.ItemEntityFieldName = 'Task.Description';
        apti2v3.ValueLiteral = 'A long value';
        aptiv.add(apti2v3);
        insert apti2v3;
        
        ActionPlanTemplateItemValue apti2v4 = new ActionPlanTemplateItemValue();
        apti2v4.Name = 'ActivityDate';
        apti2v4.ActionPlanTemplateItemId = apti2.id;
        apti2v4.ItemEntityFieldName = 'Task.ActivityDate';
        apti2v4.ValueFormula = 'StartDate + 1';
        aptiv.add(apti2v4);
        insert apti2v4;
        
        //Cannot do a bulkified insert
        //insert aptiv;
        
        
        List<List<Id>> CloneIDList = new List<List<Id>>();
        List<ActionPlanTemplate> aptList = new List<ActionPlanTemplate>();
        aptList.add(apt1);
        CloneIDList = CloneActionPlanTemplateInvocable.CloneRecord(aptList);
        system.assertEquals(1, CloneIDList.size());
        List<Id> CloneID = CloneIDList[0];
        ActionPlanTemplate aptClone = [SELECT ID, Name FROM ActionPlanTemplate WHERE Id =: CloneID];
        system.assert(aptClone != null);
        system.assertEquals(apt1.Name, aptClone.Name);
        
        ActionPlanTemplateVersion aptvClone = [SELECT ID, Name, ActionPlanTemplateID FROM ActionPlanTemplateVersion WHERE ActionPlanTemplateId =: aptClone.Id LIMIT 1];
        List<ActionPlanTemplateItem> aptiClone = [SELECT ID, Name, ActionPlanTemplateVersionId FROM ActionPlanTemplateItem WHERE ActionPlanTemplateVersionId =: aptvClone.Id];
        Set<id> aptiCloneIDs = (new Map<Id,ActionPlanTemplateItem>(aptiClone)).keySet();
        List<ActionPlanTemplateItemValue> aptivClone = [SELECT ID, Name, ActionPlanTemplateItemId, ItemEntityFieldName, ValueFormula, ValueLiteral FROM ActionPlanTemplateItemValue WHERE ActionPlanTemplateItemId =: aptiCloneIDs];
        
        system.assertEquals(aptiClone.size(), apti.size());
        system.assertEquals(aptivClone.size(), aptiv.size());
    }
}