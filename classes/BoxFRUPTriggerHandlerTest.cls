@isTest
public class BoxFRUPTriggerHandlerTest {
    @isTest
    static void myFRUPTest() {
    	
    	
    	Account testAcc = new Account(Name = 'testFRUP');
    	insert testAcc;
    	
    	   	
        box__FRUP__c testFRUP = new box__FRUP__c(box__Object_Name__c = 'Account', box__Folder_ID__c = '1234567890', box__Record_ID__c = testAcc.Id);
        insert testFRUP;
        
        testAcc = [SELECT id, Box_Folder_ID__c
			FROM Account Where id = :testAcc.Id];
		
		System.assertEquals('1234567890', testAcc.Box_Folder_ID__c);
        
        testFRUP.box__Folder_ID__c = '0987654321';
        update testFRUP;
        
        testAcc = [SELECT id, Box_Folder_ID__c
			FROM Account Where id = :testAcc.Id];
		
		System.assertNotEquals('0987654321', testAcc.Box_Folder_ID__c);
    }

}