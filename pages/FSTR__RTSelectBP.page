<apex:page title="{!SUBSTITUTE(SUBSTITUTE($Label.fstr__pce_new_object_select_record_type,'{{object}}',$ObjectType.FSTR__Business_Process__c.label),'{{recType}}',$ObjectType.RecordType.label)}" standardController="FSTR__Business_Process__c" extensions="FSTR.RecordTypeSelectCtrl">
    <apex:slds />
    <style>
        table.infoTable {
            border-collapse: collapse;
        }
        table.infoTable tr {
            background:#DCDCDC;
        }
        table.infoTable thead tr th,table.infoTable thead tr td {
            width:50%;
            padding: 0.25rem;
            text-align: left;
            border: 1px solid #ccc;
        }
        div.bPageBlock.brandSecondaryBrd.apexDefaultPageBlock.secondaryPalette {
            border-top-color: rgb(234, 234, 234);
            border-color: rgb(234, 234, 234);
        }           
    </style>   
    
    <c:CongaAnalyticsVFComponent />
    
    <apex:pageMessages id="msgs"/>
    <apex:form >
        <div class="slds-scope" id="bannerWrapper" >  
            <div class="slds-page-header" role="banner">
              <div class="slds-grid">
                <div class="slds-col slds-has-flexi-truncate">
                  <div class="slds-media">
                    <div class="slds-media__figure">
                      <img class="slds-icon slds-icon--large slds-icon-standard-user" src="{!URLFOR($Asset.slds, 'assets/icons/custom/custom57_120.png')}" alt="{!SUBSTITUTE($Label.PCE_New_Object,'{{object}}',$ObjectType.Business_Process__c.label)}" />               
                    </div>
                    <div class="slds-media__body">
                        <p class="slds-text-heading--label">{!SUBSTITUTE($Label.PCE_New_Object,'{{object}}',$ObjectType.Business_Process__c.label)}</p>
                         <p class="slds-page-header__title slds-truncate slds-align-middle" title="{!SUBSTITUTE(SUBSTITUTE($Label.fstr__pce_select_object_record_type,'{{object}}',$ObjectType.FSTR__Business_Process__c.label),'{{recType}}',$ObjectType.RecordType.label)}">{!SUBSTITUTE(SUBSTITUTE($Label.fstr__pce_select_object_record_type,'{{object}}',$ObjectType.FSTR__Business_Process__c.label),'{{recType}}',$ObjectType.RecordType.label)}</p>
                        <p class="slds-text-body--small page-header__info" id="summary_line"></p>
                    </div>              
                  </div>
                </div>
                <div class="slds-col slds-no-flex slds-align-bottom" >
                  <div class="slds-button-group" role="group"> 
                        <apex:commandButton styleClass="slds-button slds-button--small slds-button--brand" action="{!btnContinue}" value="{!$Label.fstr__pce_continue}"/>
                        <apex:commandButton styleClass="slds-button slds-button--small slds-button--neutral" action="{!cancel}" value="{!$Label.fstr__pce_cancel}"/>                  
                        <div class="slds-button--last">
                            <!-- placeholder -->
                        </div>
                  </div>
                </div>
              </div>     
            </div>     
        </div>      
        <apex:pageBlock title="{!SUBSTITUTE(SUBSTITUTE($Label.fstr__pce_select_object_record_type,'{{object}}',$ObjectType.FSTR__Business_Process__c.label),'{{recType}}',$ObjectType.RecordType.label)}" id="thePageBlock">
            <apex:pageBlockSection columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!$Label.fstr__pce_choose_category}" for="def_categories"/>
                    <apex:selectList value="{!selectedCategory}" id="def_categories" size="1">
                        <apex:selectOptions value="{!categoriesOptions}"/>
                        <apex:actionSupport event="onchange" action="{!refreshPage}" reRender="thePageBlock,rtOptions,msgs"/>
                    </apex:selectList> 
                </apex:pageBlockSectionItem>
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="{!SUBSTITUTE($Label.fstr__pce_select_record_type,'{{recType}}',$ObjectType.RecordType.label)}" for="def_record_types"/>
                    <apex:selectList value="{!selectedRT}" id="def_record_types" size="1">
                        <apex:selectOptions value="{!rtOptions}"/>
                    </apex:selectList> 
                </apex:pageBlockSectionItem>                
            </apex:pageBlockSection>    
        </apex:pageBlock>
    </apex:form>
    
    <h2 class="recordTypesHeading">{!SUBSTITUTE(SUBSTITUTE($Label.fstr__pce_available_object_record_types,'{{object}}',$ObjectType.FSTR__Business_Process__c.label),'{{recTypes}}',$ObjectType.RecordType.labelplural)}</h2>
    <apex:dataTable value="{!recordTypeDescriptions}" var="rt" columnClasses="recordTypeName,recordTypeDescription" headerclass="headerRow" styleClass="infoTable" id="rtOptions">
        <apex:column style="border-left-width:1px" headerClass="recordTypeName" headerValue="{!SUBSTITUTE($Label.fstr__pce_object_name,'{{object}}',$ObjectType.RecordType.label)}">
            <apex:outputText value="{!rt.name}"/>
        </apex:column>
        <apex:column headerClass="recordTypeDescription" headerValue="{!$Label.fstr__pce_description}">
            <apex:outputText value="{!rt.description}"/>
        </apex:column>
    </apex:dataTable>
</apex:page>