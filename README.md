# fsc-production Change Log

Salesforce FSC Production instance source


--------
## SF2020 Sprint0 01/06~07/2020 [Completed]

Update

 - [SF2020-301] Tax Opp Naming convention update
     - Process builder - Opportunity Object Process updated for Tax Opportunity Naming node and action
     - Opportunity Record type label updated - Tax Preparation -> Tax Planning & Preparation
     - Opportunity_Type__c
         - New Picklist options: Tax Planning, Tax Preparation
         - Deactivated picklist options: Basic, Advanced
     - Tax_Timecard_Generator_Controller.cls updated
         - line 73: updated to oppTypeLabel = 'FWS – Tax Planning & Preparation';
 - [SF2020-305] New custom report type: Contact with Related Account
     - New custom report type
     - New report as template
 - [SF2020-303] Task/Event type for Tax activities
     - New Task Types: FWS - Tax Prep, FWS - Tax Onboarding
     - New Event Type: FWS - Tax KPI
 - [SF2020-295] Campaign page layout - add 'Active' field on page
 - [SF2020-307] Tax Opportunity Sales Process reordered/updated
 - [SF2020-302] Opportunity_nickname__c to be visible
     - Tax Opportunity Compact Layout updated
     - Account - Household, Household (Admin), Institution page layout updated (related list)
     - Person account page layout updated (related list)
     
Data Patch

 - [SF2020-301] Update existing Tax opportunity - Opportunity_type__c = 'Tax Planning'

--------
## Salesforce Patch 12/31/2019 [Completed]

Update

 - Financial Planning Module
     - Name changed to Text from auto number
     - FPM related objects edit access to System administrator profile
 - Opportunity page layout assignment updated - MA CSU / RVP: FWS read only layout, MA Full access - FWS layout
 - MA Full Access: permissions to 'Edit Events'

--------
## Salesforce Patch 12/30/2019 [Completed]

Update

 - Opportunity process builder - 'Channel Partner Information' node criteria updated (ReferredByContact is changed OR isnew)
 - FinServ\_\_financialAccount\_\_c.Orion\_Push\_Failed\_\_c removed - unused field, replaced by Sync\_Error\_\_c object (OrionApiCollection, ApiCollection\_Test updated to remove the field)

--------
## Salesforce Patch 12/27/2019 [Completed]

Update

 - Financial Account Page layout - Envestnet/Orion page layout updated : Related list updated
 - Permission set - Business intelligence Access updated : Interactive Client Survey access granted


--------
## Salesforce Patch 12/26/2019 [Completed]

Update

 - Client Service Unit Page layout updated
 - Client Service Unit FLS updated:
      - Total Client Households
      - Required Client Meetings per Month
      - Required Client Meetings per Quarter
      - Required Client Meetings per Week
 - Opportunity - Tax Preparation layout updated
 - Account - Household Admin page layout updated: related list updated (interactive client survey, Gift and entertainment added)
 

Data Patch

 - OpportunityLineItem transfer for Karen Sakas Household (Could be more to be transferred based on future request)

--------
## Salesforce Patch 12/23/2019 [Completed]

Update

 - [ITSD-2059] NLC case process
     - Account.Type
         - Activated value: Pending NLC, NLC
     - Case Object process builder
        - NLC case = new - Status = Inactive, Type = Pending NLC, Pending NLC date = today()
        - NLC case = closed - Status = Inactive, Type = NLC, NLC date = today()
 - Case - Envestnet NLC Billing Disclosure : added Helptext (I understand Envestnet accounts require additional steps to initiate a Final Bill. I have taken those additional steps or plan to do so immediately after creating this case.)
 - household layout update: Status, NLC date, Pending NLC date, NLC reason - read only
 - Account validation rule - Inactive\_Client : deactivated

Data Patch

 - [ITSD-2059] NLC cases submitted since 11/1 - Update target household Type

--------
## Salesforce Patch 12/20/2019 [Completed]

Security Update

 - Palm Beach Gardens - now under Northeast Managing Director
 - New Public group: Hackensack-Palm Beach Garden Group
     - Palm Beach Gardens Support Group
     - Hackensack Support Group
 - New sharing setting
     - Account
         - Hackensack-Palm Beach Garden Group Sharing
     - Financial Account
         - Hackensack-Palm Beach Garden Group Sharing
 - Profile upates:
     - View setup and configuration has been removed: MA Corporate, MA CSU, MA FWS, MA Full Access, MA Investment Ops, MA RVP, MA Trust
     - Moderate Chatter has been removed: MA CSU, MA RVP
     
 Update

 - CSU Process builder - remove auto-naming the CSU
 - CSU Trigger - ignore special characters in name
 - New contact formula fields- Marketing\_Salutation\_Person\_Account\_\_c, Client\_Communication\_Person\_Account\_\_c
 - Updated report type; Accounts with related contacts
 - Contact (Business) page layout updated: Description added to the page layout.
 
Data Patch

 - [ITSD-2154] Box collaborator sync from Salesforce

--------
## Salesforce Patch 12/19/2019 [Completed]

Update

 - Opportunity - Stage: Prospect Cloesd - Activated/Updated to be Closed Won, Probability 100%
 - opportunity process - AnyUpdateOnClosedOpp allowed (Opportunity.Name can be changed)
 - Financial account process updated for Investment Ops
     - Process builder updated: if Hold code changed to Invest AO, Trading Blocked = false. If changed to anything else, trading blocked = true
 - CSU update - invocable updated
 - OrionApiCollection updated: Financial account update/client update - to prevent early read time out 
 - [ITSD-1580] Trade Error validation rule updated: StatusMAInvestOpsOnly - ISCHANGED(Status\_\_c) was added so Accounting can add data before completing the approval
 
Data Patch

 - [ITSD-2130] Prospect email missing - matched from Account record in TDA salesforce

--------
## Salesforce Patch 12/18/2019 [Completed]

Update

 - Financial account update
    - Pay Method Storage updated: If pay method is null, get data from billing instruction
    - Orion Financial Account page layout updated
    - Process Builder - Financial account process updated:
        - exclude orion update from triggering financial account callout,
        - extra filter for updating trading instruction to prevent multiple update,
        - update previous hold code & Quarterly rebal updated�nodes- filter formula to conditions
 - Rollup By Lookup Configuration - Following RBL have been deactivated for performance improvement:
    - RBLForBankingHH
    - RBLForFARForInsuranceClientJointOwner
    - RBLForFARForInsuranceClientPrimaryOwner
    - RBLForFARForInsurancePremium
    - RBLForFARForInvestmentsClientJointOwner
    - RBLForFARForInvestmentsClientPrimaryOwner
    - RBLForFARForLastTransactionDateHH
    - RBLForFARForLastTransactionDateJointOwner
    - RBLForFARForLastTransactionDatePrimaryOwner
    - RBLForFARForTotalBankDepositsJointOwner
    - RBLForFARForTotalBankDepositsPrimaryOwner
    - RBLForFARForTotalOutstandingCreditJointOwner
    - RBLForFARForTotalOutstandingCreditPrimaryOwner
    - RBLForInsuranceHH
    - RBLForInsurancePremiumHH
    - RBLForInvestmentsHH
    - RBLForLiabilitiesHH
    - RBLForNonfinAssetsHH
    - RBLForTotalOutstandingCreditBankerHH
    - RBLForTotalRevenueBanker

Data Patch

 - [ITSD-2010] Service category update
 - [ITSD-1959]�Financial accounts under wrong HH - manually fixed


--------
## Salesforce Patch 12/17/2019 [Completed]

Update

 - EnvApiCollection.handleTranslate - code handler fixed
 - Permission set - Business Intelligent Permission : added permission for CSU object/fields
 - Financial account - Trading Instruction: Process builder updated to auto-populate Trading instruction field when hold code/Status code/billing instruction chagnes
 - Financial account - Pay Method Storage: turns Pay method text into Orion Pay method code (need to fix: need to translate Billing instruction instead)
 - Orion financial account sync process automated: any changes
    - Financial Account process builder: Trigger invocable apex when target fields are updated in Orion account with Orion FAID 
    - OrionApiCollection and related test classes (ApiCollection\_Test, Orion\_MockHttpCallout, Orion\_MockHttpCallout\_Auth, updated
    - New object: Sync\_Error\_\_c
   
Data patch

 - [ITSD-1959]�Financial accounts under wrong HH - manually fixed


--------
## Salesforce Patch 12/16/2019 [Completed]

Update

EnvApiCollection.handleTranslate - code handler fixed
Permission set - Business Intelligent Permission : added permission for CSU object/fields
Financial account - Trading Instruction: Process builder updated to auto-populate Trading instruction field when hold code/Status code/billing instruction chagnes
Financial account - Pay Method Storage: turns Pay method text into Orion Pay method code (need to fix: need to translate Billing instruction instead)
Orion financial account sync process automated: any changes
Financial Account process builder: Trigger invocable apex when target fields are updated in Orion account with Orion FAID 
OrionApiCollection and related test classes (ApiCollection\_Test, Orion\_MockHttpCallout, Orion\_MockHttpCallout\_Auth, updated
New object: Sync\_Error\_\_c
 

Data Patch

 - [ITSD-1959] Household mismatch financial account - manual check
 - [Email / Stephen Prinster] Hold code bulk update


--------
## Salesforce Patch 12/13/2019 [Completed]

Security Update

- Managing Director/Branch Managers also need role update for accessing cases/opportunities belongs to their client
	- select id, name, CaseAccessForAccountOwner, OpportunityAccessForAccountOwner from UserRole where name like '%Manager' or name like '%Director'
	- CaseAccessForAccountOwner, OpportunityAccessForAccountOwner = EDIT


Update

- Campaign - logic for auto-generating Marketing case: Do not create marketing case if name contains 'Pardot'
- ITSD-1844] MA RVP can now create/manage personal dashboards


--------
## Salesforce Patch 12/12/2019 [Completed]

Update

- [ITSD-1908] Permission Set update - Business Intelligent Permission: read access to all Opportunity fields

Data Patch

- Case : 169 cases matched with wrong Financial accounts. Correct FinAcc were rematched except for following three (Financial account not found)
- 5001U00000M2xoyQAB	924470336
- 5001U00000MetgBQAR	943405848
- 5001U00000MetQdQAJ	943405848


--------
## Salesforce Patch 12/11/2019 [Completed]

Update

- ITSD-977] Account.Financial\_Planner\_\_c field can be edited by MA CSU
- ITSD-1819] New Custom Model case - notify ISG team (Financial account object process updated)
- ITSD-1818] Opportunity new custom field - Final\_Year\_Return\_\_c (checkbox) for FWS Tax Prep opportunity

Data Patch

- ITSD-1835] Pricing Reference Data update - 5 failed (no ID)
- Duplicate Financial account name - v1 dedupe (188 duplicates merged)


--------
--------


### ** Change Logs before 2019-12-11 


--------
--------
## Salesforce Patch 12/9-10/2019 [Completed]

**Staging (Full) sandbox refreshed 11:15am MST
 
TODO: Need Fix**

- [Need Fix] New Financial Account Process - Causing error when MA CSU creates trade request
 

Update

- Flows updated to accommodate OTBD ownership change - removed node for updating OTBD ownership
	- Perpetuate Account Ownership Change
	- Perpetuate Account Ownership Change isLinkedToHousehold
- Business Process update for Trade Request Process; allows licensed user to be able to change status to new without submitting the record for approval
- EnvApiCollection - need to be updated: ENV Handle -> ENV code
- [ITSD-1793] make Type required on quick action (Account action - New Task)
- [ITSD-1724] AnyChangesOnClosedOpp - Causing error on cloning
	- Validation rule updated (Not(ISNEW()) --> Createddate <> LastModifiedDate)
	- Update fieldset for cloning (Advisory Sales)
	- CloneOpportunityObject.cls updated: Set StageName = New on Advisory Sales
- [ITSD-1671] updated Financial account validation rule - Envestnet\_Account\_Info\_Section
	- removed 'IsChanged(Envestnet\_Excess\_Cash\_\_c)' portion from validation rule
	- Envestnet\_Excess\_Cash\_\_c field is formula and shouldn't be included in the validation rule anyways
- Page layout updated
	- Household Account/Household Account(Admin) : Household financial account field reordered
	- Person account : Financial account Primary Owner/Joint Owner field reordered
	- Person account lightning page - Financial account tab - single related list updated to be enhanced list
 

Data Patch

- Opportunity - LeadSource : Updated 'E*Trade/TCA' to be 'E*Trade' for data quality,


--------
## Salesforce Patch 12/6-7/2019 [Completed]

Updates

- Financial account object process builder - Reverted back due to it triggers QueryLimit on FinancialAccountTrigger when user is submitting (case submitted to Salesforce Support)
- Profile permission update
	- Opportunity Financial Account: CRU permission to following
		- MA Corporate, MA CSU, MA Full Access, MA RVP, MA Trust
	- [ITSD-1725] Financial Accounts TDA Pre-Assigned: Read/Edit permission added to following
		- MA RVP
- [ITSD-1724] Opportunity Validation Rule - No edit after closed for MA CSU and MA RVP
- [ITSD-1730] Sharing setting for Campaign updated : Private -> public read
- [ITSD-1639] Account - Initial incentive payment date is read only for following profiles: MA Corporate, MA CSU, MA RVP, MA Trust
Security Updates
- Security update: users in same branch OR included in cross-branch access group can view each others' clients (Data update)
	- Advisor roles were added to the branch support group
		- Query group to update: select id,name from group where name like '% Support Group'
		- Insert following with data (Group.Name and RoleName are for reference)
Id	Group.Name	UserOrGroupId	RoleName

- Advisor role - case access/Opportunity access
	- for all account roles 'Case access' and 'Opportunity access' are updated from no access to read/write (Metadata update)
		- Query (select id, name, CaseAccessForAccountOwner, OpportunityAccessForAccountOwner from UserRole where name like '%Advisors')
		- Reimport with updated value (Edit) on CaseAccessForAccountOwner, OpportunityAccessForAccountOwner
	- Sharing setting update (Metadata Update)
		- Account
		- Update following sharing rules: Any Branch-related sharing, cross-branch sharing, All Branch sharing, All RVP sharing, All Corporate sharing
		- Case
		- Delete following sharing rules: Any Branch-related sharing, cross-branch sharing, All Branch sharing, All RVP sharing, All Corporate sharing
		- Opportunity
		- Delete following sharing rules: Any Branch-related sharing, cross-branch sharing, All Branch sharing, All RVP sharing, All Corporate sharing
- One Time Billing Discount - update Account to be Master-detail to inherit security from Account
 
- Contact fields to create for Pardot connection: [Pending]
	- Client communciation
	- Marketing Salutation
	- Role
	- Branch
	- Client Since


--------
## Salesforce Patch 12/5/2019 [Completed]


Updates

- PMR - Billing Exception Request Page layout updated : Added 'Record Type' to page layout
- Financial account Process Builder updated - CSU population (if household<> null, and CSU is empty OR CSU is different, update CSU on financial account) 
- [ITSD-1152] SLOA related list columns updated for Envestnet/Orion financial account page layout
	- only first 4 column will be visible to Related tab, user need to click 'view all'
- [ITSD-1023] SLOA - Picklist value '.' has been added to following fields for data bulk import:
	- ACH\_SLOA\_Type\_\_c
	- ACH\_Direction\_\_c
	- Check\_SLOA\_Type\_\_c
	- Journal\_SLOA\_Type\_\_c
	- Wire\_SLOA\_Type\_\_c
- [] Opportunity Process update - disable EP Opportunity AUM Change Email Alert node (AUM recalculation sends email bomb to Jenna every time AUM recalculation happens)
- [] Opportunity - Advisory Sales page layout updated : Description was added to the page layout
- Household account/person account page layout updated - Financial account list : updated fields to be included and sort by Balance - DESC

Data patch

- [ITSD-1023] SLOA Bulk insert
- [ITSD-1644] FinAcc Pre-assigned TDA numbers inserted


--------
## Salesforce Patch 12/4/2019 [Completed]

Updates

- Profile : Integration User - Granted 'Update Records with Inactive Owners' permission
- Price modification request
	- Billing Exception Request page layout updated
	- Lightning page layout updated
	- Submit for approval - Textarea for comment has been added
- Person Account page layout: Envestnet Member code added to the page layout
- Permission Set: Business Intelligence Access - Granted 'Customize Application' permission
- [ITSD-1633] Business Intelligence Access - access to RVP field, New Permission set : View All Data - Assigned to Database Backup
- [ITSD-1624] Advisor change case access for branch manager / CSU to advisor
- PB - Account Object Process : Orion advisor change process added to CSU changed node to update Advisor in Orion
- EnvApiCollection - handleTranslate updated
- Trade Error Approval Page Layout Updated
- [ITSD-1493] Person account lightning page updated (added 'Financial Account Primary Owner' and 'Financial Account Join Owner' related list to Financial account tab), Financial Account - Field Set : Orion Financial Accounts updated
 
Data Patch

- PMR - Billing exception request : record type re-matched


--------
## Salesforce Patch 12/3/2019 [Completed]

Updates

- Role update: Move Philadelphia branch under Northeast Managing Director
- Custom report type
	- Accounts with Related Contact with CSU
	- Accounts with Financial accounts with SLOA
- Business Account Page layout : Include RVP field
- Price Modification Reques
	- Page layout/Lightning app page updated
	- Lightning Component for custom approval submission 
	- Validation rule update (Deactivate validation rule) 
- Price Modification Financial Accounts
	- Page Layout updated
	- Record lightning page created
 
Data Patch

- Channel partner business accounts - Regional Vice-President field brought back from TDA SF


--------
## Salesforce Patch 12/2/2019 [Completed]

Updates

- MA Full Access - grated permission for 'Manage public reports' and 'Manage public dashboards'
- Opportunity : Lead source - TD Ameritrade were added as an option for Advisory Sales
- Investment strategy case: 'Assigned to' not editable by MA CSU
- [ITSD-1534] Rochester Support Group - Add Rochester Advisors role (Compliance approved client sharing)
- Lead assignments [pending]

Data Patch

- Channel partner - business household corrected (~200 contacts)
 


--------
## Issues/Release/Update/Data Run: 11/19/2019 5/8PM MST [Pending]

Data patch

- FP update for CSUs: Colon/Devita/Noveck
- Advisor changes


--------
## Issues/Release/Update/Data Run: 11/18/2019 5/8PM MST [Completed]

Data Migration

- PMR - Update legacy PMR record type [Completed]
- Case - ADv Change share with CSU To and Branch Managers

Update

- Orion - Account update NEED FIX ASAP
- <Staging> PMR $$ fields update from 18,0 -> 16,2
- <Staging> <WIP> Account Access to include OTBD share & Adv change auto share


--------
## Issues/Release/Update/Data Run: 11/17/2019 5/8PM MST [Completed]

Update

- <Staging> Trade Request Process: Updated approval auto sumission criteria node
- <Staging> Account Process: Added node for copying Mercer Household ID to Mercer Hoisehold ID Override (allows search by HHID number)
- <Staging> <WIP> Account Access to include OTBD share
- <Staging> Trade Request - recall/resubmission by Inv ops. Allows stage change to 'New' by Inv Ops after approved
- <Staging> Lead setting- Default lead assingee: Mercer System Admin. Lead assignment rule - deactivated 


--------
## Issues/Release/Update/Data Run: 11/16/2019 5/8PM MST [Completed]

Data migration

- Task 'Completed' date is wrong. Migrating today
- Opportunity  - EP Completion Meeting was not migrated. Migrating today
- Account.Mercer\_Household\_Id\_\_c - Migrating Salesforce\_ID\_\_c from TDA org

Issue fixed

- [PROD] FlexiPage for Account - Household page fixed
- [Staging] Opportunity.Completion\_Meeting\_\_c renamed (duplicate/empty)


--------
## Issues/Release/Update/Data Run: 11/15/2019 5/8PM MST [Completed]

- Campaign : sharing setting update to public read
- Account - Admin Household page layout updated
 
- Additional FRUP migrated from TDA org


--------
## Issues/Release/Update/Data Run: 11/14/2019 5/8PM MST [Completed]

Issues and Update

- [ITSD-1078] Simsbury/Hartford security update: Client info are shared for all members in branch
- [Prod/UPD] Financial account page layout update - Orion account
- [Prod/UPD] MA Full Access: Access to Dashboards was provided
- <Completed> [ITSD-1120] Financial accounts - new fields
	- [NEW] Previous\_Rebalance\_Status\_\_c
	- [NEW] Status\_Code\_Storage\_\_c
	- [NEW] Rebalance\_Y\_N\_Lookup\_\_c
- <Completed> [ITSD-1120] Financial accounts - process builder update
- [NEW] Report type: Account with Related Contact - for pulling ACR in reports

 
TDA to FSC Data Transfer [Pending]

- [ITSD-929] HH-00141347


--------
## Issues/Release/Update/Data Run: 11/13/2019 5/8PM MST [Completed]

Issues

- [ITSD-823] Opportunity.Client\_Service\_unit\_\_c formula need to be updated https://fscmerceradvisors.lightning.force.com/lightning/setup/ObjectManager/Opportunity/FieldsAndRelationships/00N1U00000UJMgz/view
	- [UPD] Opportunity.Client\_Service\_unit\_\_c
- [ITSD-823] Two Assigned to field in Case
	- [UPD] Case page layouts
	- [UPD] Assigned\_To\_cases\_\_c label updated to Assigned To Case
- [ITSD-907] Case sharing between Marketing teams - should include marketing cases created by marketing team as well. Also need access to compliance case created by other marketing team members
	- [NEW] Case sharing rule: Marketing User case sharing
- [ITSD-793] Trade error object validation rule (prevent users from updating status without approval process)
- [ITSD-1011] marketing-compliance case - make FRUP for compliance case
	- [UPD] PB: Case Notification process
	- [UPD] PB: Marketing case process
	- [NEW] Flow: Create FRUP
- financial account page layout
	- orion page layout - include Date closed, trading instruction, Status(finserv)
- Compliance access to users to update 'Licensed\_\_c' field : Permission set - Compliance User Access (Created in Prod and Staging)
- [ITSD-940] Lead queue: Marketing Leads - need to remove 'Marketing' team  and 'Kristen Kestel' (completed in Prod and Stagin
- Orion - update closed account with closed date / final balance
 

TDA to FSC Data Transfer [Pending]

- [ITSD-929] HH-00141347
 

Data Update

- re-assign primary owner for manual/yodlee financial accounts, change owner, and any related cases


--------
## Release / Update / Data Run : 11/12/2019 8PM MST [Completed]

Integrations

- DBsync configured and ready to be deployed


Release

- Apex class
	- [UPD] OrionApiCollection
- Lightning Component
	- [UPD] UpdateFinancialAccount


--------
## Release / Data run : 11/11/2019 8pm MST [Completed]


- Apex Class
	- [UPD] EnvApiCollection
- Object/Field
	- [NEW] Opportunity.Mercer\_Household\_ID\_\_c
- Visualforce Page
	- [UPD] Opportunity\_Invoice
- Process Builder
	- [UPD] Opportunity Invoice Process
-- Run HH update for Stephen Lee CSU clients (Added FP)
