<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>OrionDevAuth</label>
    <protected>true</protected>
    <values>
        <field>Client_Id__c</field>
        <value xsi:type="xsd:string">1749</value>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:type="xsd:string">A942902D-9FB7-4E33-BF70-AE1A46791987</value>
    </values>
    <values>
        <field>expiration__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>pw__c</field>
        <value xsi:type="xsd:string">7AlFLXw9aXkOyx3#*DFg</value>
    </values>
    <values>
        <field>refreshToken__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>token__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>usnm__c</field>
        <value xsi:type="xsd:string">api_MercerAdvisors_IntegrationPartner</value>
    </values>
</CustomMetadata>
