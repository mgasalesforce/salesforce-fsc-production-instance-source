<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>CreatedDate</label>
    <protected>false</protected>
    <values>
        <field>GoLiveDateTime__c</field>
        <value xsi:type="xsd:dateTime">2019-11-04T07:00:00.000Z</value>
    </values>
</CustomMetadata>
