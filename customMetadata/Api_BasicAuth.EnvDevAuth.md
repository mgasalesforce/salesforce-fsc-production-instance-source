<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>EnvDevAuth</label>
    <protected>false</protected>
    <values>
        <field>Client_Id__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Client_Secret__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>expiration__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>pw__c</field>
        <value xsi:type="xsd:string">welcome12</value>
    </values>
    <values>
        <field>refreshToken__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>token__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>usnm__c</field>
        <value xsi:type="xsd:string">soobinsadvisor</value>
    </values>
</CustomMetadata>
