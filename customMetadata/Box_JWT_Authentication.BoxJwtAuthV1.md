<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BoxJwtAuthV1</label>
    <protected>false</protected>
    <values>
        <field>BoxAccountId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>BoxServiceAccountId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>BoxServiceAccount_Email__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>ClientId__c</field>
        <value xsi:type="xsd:string">5wulwmgmibr0ejbbz93foze008bjopax</value>
    </values>
    <values>
        <field>ClientSecret__c</field>
        <value xsi:type="xsd:string">O54WoaNZA3yRi7MSjc9k7UBgqvuIEMrP</value>
    </values>
    <values>
        <field>HouseholdMasterFolderStructure_FolderId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>HouseholdRootFolderStructure_FolderId__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>boxSubType__c</field>
        <value xsi:type="xsd:string">enterprise</value>
    </values>
    <values>
        <field>jwtConnectedAppConsumerKey__c</field>
        <value xsi:type="xsd:string">5wulwmgmibr0ejbbz93foze008bjopax</value>
    </values>
    <values>
        <field>jwtHostname__c</field>
        <value xsi:type="xsd:string">api.box.com</value>
    </values>
    <values>
        <field>jwtJti__c</field>
        <value xsi:type="xsd:string">M4yeY3W63TxHa9jFek85</value>
    </values>
    <values>
        <field>jwtPrivateKey__c</field>
        <value xsi:type="xsd:string">MIIEpAIBAAKCAQEAqqaQVh1kww9H/JtXU2EF+4UkOhMkYybNtJNBrmX9cgVMT+2iWP0nEi6CeLh6nEDNU/Xj384If0kDgwx5d9cum4ugTTyJ92d7a23lGrGBWG4CjAOuQrkkacOfHz82qsUPIZtrBSsvB6NPUPBYnWlumUCcGkMqdx0624delgJzlZdK9lIi95zJHswnF/RSLCxnqPc0w28IfOV/Q828OduZPkcgWSVEJk+sY2YnYsERKPXi/PQUxMFvsHemf4iZlgKtsah7KQfufHW3ghtjvXPsgfwyU1RcPN8e18QrHz4KxyArDJeezCDcK7XE1wzOVbvjuj3LC9SSYZq4b78DZF9WKQIDAQABAoIBACtqyVo/jD1ci/InC6a5Dq6Zh0gHSsC3RKrmtFVMuZji5DGJwj5l2iT0FFnmhbtveU4dR2bPorXPu8cBy33ED6Wfdp9wAaIif/IrOZj7BG+dPaSl4xYyyKLoFHn8uUnlbR4dzrK38/U7hZiQLduO0YbIO28rG4jGtQzZeiUsZn/uXbejwd/f+NC6q/J5ao8r4ErvLW4zlHoQxneDTtqSJgwGABOVwWP9eMHf/IA33ZjmCzBVeVjMHQHHpGGmBNDix/1AiLF8Rv3r33aId5fA18g5YByzKGTFpEBxX74EJcpF8vzYBvyyC1YOg3XSc98i4jB/B3PHTzKe0K7hzPMAfokCgYEA20TeW8o25u2yp2g341HFK6rxwQ7N6FKvnB2Alb02oyd7Qvt0vVrbsO15omnh3AAhTvJn3n4xyEMhLvlKNoQvXp6LBYGNAq3VGlyQXk8mTq1AnWomZ0WwXyiHPZLFuViggceS1pXP1fquXazn/NAQ1e+NwhfGQtQrmczEgHXKdWsCgYEAxzy+tZaR79CQ96Od5RhFKIrpKmsx1bzgTNZAxR5UUQn9syaiqbZAmU1RibqAW681Hj7Myuvsg3DUDdAWL2Bwsmjeydnio+uSAcvZuFLQbu0A+SK8FJglPqeRLwY5ENGXr142j1UpIs70ZzCvhjxzyuRPEI5gAXeKRmAfg+7V87sCgYEAt7DHM1CBlA53x4UopNhg/Mc1xo1XdJSop3mTul4MQryVp7SuxN80AtG/P4/CdiLpxJCQ8bT4kZOjXg3b3TX51SOuxYsVjbQBPhdjno5iQjEkW3uM9qFMMzvpqGyoFjEoLfD6k6hnuBRNNQ7gPkw0fnU0ZfCy2FOUv0DrMc78MEcCgYEAl3cvyN8aPUXDH/CsVgFYelT4o/3wWfyITQHJJdJD6YP98leZ7AmZHLKyHVHCyXDuofq+ODqZiOea3pFmfEgGBSxCyxOxAEtTqbfu4flbaikVx1FHySRNlE9RyX0inw2oEnM+aUMlzcel+FkD57pHkWR2UzDZ/eOCJLRo4SUOVIcCgYBWqYMweNaPziLznuSJDrkaVcUP/TBOIgVTojD0EEJ4TWjaP7svzuDlZ9M5iqABvQA0nWsGjQi+GtpdLhY1TqBdvtiAfkSKh8UkHIpxlh0cH/WL/70C8jzhcQaFN2ifNKDu97DvQZgdNrW+1OqRZMC2c8bD/kfxlhpukwN+kAgu9A==</value>
    </values>
    <values>
        <field>jwtPublicKeyId__c</field>
        <value xsi:type="xsd:string">214h6oly</value>
    </values>
    <values>
        <field>jwtUsername__c</field>
        <value xsi:type="xsd:string">219317767</value>
    </values>
</CustomMetadata>
