<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>BoxJwtAuthFinal</label>
    <protected>false</protected>
    <values>
        <field>BoxAccountId__c</field>
        <value xsi:type="xsd:string">363746</value>
    </values>
    <values>
        <field>BoxServiceAccountId__c</field>
        <value xsi:type="xsd:string">10500752973</value>
    </values>
    <values>
        <field>BoxServiceAccount_Email__c</field>
        <value xsi:type="xsd:string">AutomationUser_897863_qLMi0Hurt5@boxdevedition.com</value>
    </values>
    <values>
        <field>ClientId__c</field>
        <value xsi:type="xsd:string">no7tp18y0643w9twt6n26woedk5lw1av</value>
    </values>
    <values>
        <field>ClientSecret__c</field>
        <value xsi:type="xsd:string">yJe3jCbDkjYEBq5m7cSaUqsc1LJuSpoH</value>
    </values>
    <values>
        <field>HouseholdMasterFolderStructure_FolderId__c</field>
        <value xsi:type="xsd:string">83750693928</value>
    </values>
    <values>
        <field>HouseholdRootFolderStructure_FolderId__c</field>
        <value xsi:type="xsd:string">5370980549</value>
    </values>
    <values>
        <field>boxSubType__c</field>
        <value xsi:type="xsd:string">enterprise</value>
    </values>
    <values>
        <field>jwtConnectedAppConsumerKey__c</field>
        <value xsi:type="xsd:string">no7tp18y0643w9twt6n26woedk5lw1av</value>
    </values>
    <values>
        <field>jwtHostname__c</field>
        <value xsi:type="xsd:string">api.box.com</value>
    </values>
    <values>
        <field>jwtJti__c</field>
        <value xsi:type="xsd:string">M4yeY3W63TxHa9jFek85</value>
    </values>
    <values>
        <field>jwtPrivateKey__c</field>
        <value xsi:type="xsd:string">MIIEpQIBAAKCAQEA1tLl3igzE5gM4tBfyD7SXqFx4R875PeSNb69BXMQ3fsESJAZp5r5L6G70i3zZJ3ca9RUcT4lgCgKhrKtO0xVdWxgu4wY4O0jHk/cDpGSvHKmPaJ7NW41tJZgZVOtk1uFiBb+uiXYSouo+efTDA7HZSJji+ICWNUNxC2oG5MQApNtcXdoLa7EhhZQAGSV1FAMoil46lKcLtIwyup6XG1eq9pf5lA2NOSdrQVM5cc7+SgQSEwKrEkKm9l8yET8W82OQU1QIjo1NOuODk8vhV0h4vkey2ZTn/NIp6X78MPbX5e5y04Nl2ExWASuCYfl3UiqyVOSY5zg4tIuJzE2Qul2EQIDAQABAoIBAAXnS8vcHPM35XrCMS+mkk9pbmC6ZAH5z7nMYRNqQxdmk4qO8zWI67okXq3690pXkDwiXKO0CoHIF+n1tQ8BHQQaE1p0Eib0PVfS6AA6febR28Gr2mjwe2dYDpQaRZXcGQJRZ3SkH5qE1gLwHPui2FXuKQAqGNhqGnO+sXT4iKMZbyZtNKcv9OVKWi+Rr5WEjNqGhJpL/CkcNbZTOphR9U1qAQx7zsswRssExrUUsQtYkV3FwY4/yK82WbAwMw1+SF7rV1taVn7j/RjuhJWO8tNs0aCrTTjIbIYqZUNutBmXpt/SkumBZFzeU85T2D1Xe9AUYdc8sVTAibdzHHkd2fECgYEA/PRbr2azyZ4SLxLalLMMfcLOCOKs1xjZ+tb+jTtzn8/RJuLh+63T/akpGelu+apHYB6Y5xKtEFfEmS+OZrBfveGJHMw5l1K78qIIDvJ+ID9cnabaXNj2QmCKwFI3+zPiLlvHFQ6KbnAHiA8i12yXK3baAIfP1KD+l5AHz3EyYQUCgYEA2WkD+PhcDVmNp9VANdYNEH8SyMDrNoCqoDMvB31KQJVY5jd5s+sfseIXXqTetXBWI61OmN4S9gCPARX1F+qikYSfWJ73HVqDMGaBMMRnRGSxP/jG06foy+pMWn0lvbrh+BuZWFPrG0hDjjfZrOdHuIadHex/f8GhxX/f7K+N/p0CgYEA886WnF3yBe9MeYDDmzVkYcakjZ3ll8DFQOd+HCRkUnGcls1xoQIvZLtp+Saspwk/mF6FY5r1od3miPt0VnfDICJE5LPXAopkyvzvpXCGrpBDtVQ//ePEn7+rnKjiekXMyH//R8VnGgDy0Wo9W8O3hNYeUwqT/85Y9T6GuKPTOf0CgYEAzwv40YlMuRX5FsW68cpZBMXP0MoQ9wXP+OLlvd8u8JSKZiycxaB8vb5QfTzML8S89BsfR+vt6MOdbO5JTK6Shbzkt9QkCJtjZpuoGxZgNj0ipIa6a2V0CYankmGuqKKqgstd3UWGGKRYWb+MmOJlZjEkvhO1VKjF9D2UtmZ19u0CgYEA6PZYX9pCDpAzPv7flIxS4gEf+CqwUlMKrSHXYWvFXJRY6YnQAxpy6fpxUBGykTj444T8vVU2jAxksOeJeW92UeWJmpAm2cGwYWLJnz2QyvNQKSRe5LJ/ci6l2eAbJ1N1ZqEnI5lP1/p3cS4twVpbJ/sP0snEdLruDyxnc33LlNA=</value>
    </values>
    <values>
        <field>jwtPublicKeyId__c</field>
        <value xsi:type="xsd:string">8v5zrsd9</value>
    </values>
    <values>
        <field>jwtUsername__c</field>
        <value xsi:type="xsd:string">363746</value>
    </values>
</CustomMetadata>
