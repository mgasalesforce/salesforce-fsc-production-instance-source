<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Member Relationship Adult Child</label>
    <protected>false</protected>
    <values>
        <field>Salesforce_Value__c</field>
        <value xsi:type="xsd:string">Adult Child</value>
    </values>
    <values>
        <field>Target_API_Field__c</field>
        <value xsi:type="xsd:string">relation</value>
    </values>
    <values>
        <field>Target_API_Object__c</field>
        <value xsi:type="xsd:string">member</value>
    </values>
    <values>
        <field>Target_API_Value__c</field>
        <value xsi:type="xsd:string">10</value>
    </values>
</CustomMetadata>
