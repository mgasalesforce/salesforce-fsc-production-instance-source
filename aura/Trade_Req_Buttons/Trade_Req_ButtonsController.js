({
    doInit : function(component, event, helper) {
        // Trade Request record types
        //// Cash_Distribution			012180000000cwiAAA
        //// Full_Account_Liquidation	012180000000cwnAAA
        //// IPS_Change 				012180000000cwsAAA
        //// Manual_Trade				012180000000cwxAAA
        //// New_Money_IPS_Changes		012180000000cx2AAA
        var action = component.get("c.GetRecordTypeMap");
        action.setParams({
            'sobjectName': 'Trade_Request__c'
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.recordTypeDeveloperMap", response.getReturnValue());
            }
            else{
                alert("Temporary Error: There is a problem getting the Record Types.");
            }
        });
        $A.enqueueAction(action); 

    },
    finacc_New_Cash_Distribution : function (component, event, helper){
        var recordTypeDeveloperName = 'Cash_Distribution';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Trade_Request__c',
            'defaultFieldValues': {
                'Account__c':component.get("v.household"),
                'Financial_Account__c':component.get("v.financialAccount"),
                'Previous_Hold__c':component.get("v.holdCode"),
                'Existing_Model__c':component.get("v.model")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_New_Full_Account_Liquidation : function (component, event, helper){
        var recordTypeDeveloperName = 'Full_Account_Liquidation';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Trade_Request__c',
            'defaultFieldValues': {
                'Account__c':component.get("v.household"),
                'Financial_Account__c':component.get("v.financialAccount"),
                'Previous_Hold__c':component.get("v.holdCode"),
                'Existing_Model__c':component.get("v.model"),
                'Quarterly_Rebalance__c':component.get("v.quarterlyRebal")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_New_Model_Chg_New_Money_Only : function (component, event, helper){
        var recordTypeDeveloperName = 'New_Money_IPS_Changes';
        if(component.get("v.quarterlyRebalBool")){
            component.set("v.quarterlyRebal",'Yes');
        } else {
            component.set("v.quarterlyRebal",'No');
        }
        
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Trade_Request__c',
            'defaultFieldValues': {
                'Account__c':component.get("v.household"),
                'Financial_Account__c':component.get("v.financialAccount"),
                'Previous_Hold__c':component.get("v.holdCode"),
                'Existing_Model__c':component.get("v.model"),
                'Quarterly_Rebalance__c':component.get("v.quarterlyRebal")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_Model_Chg_Sells_Manual_Trade : function (component, event, helper){
        var recordTypeDeveloperName = 'IPS_Change';
        if(component.get("v.quarterlyRebalBool")){
            component.set("v.quarterlyRebal",'Yes');
        } else {
            component.set("v.quarterlyRebal",'No');
        }
        
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Trade_Request__c',
            'defaultFieldValues': {
                'Account__c':component.get("v.household"),
                'Financial_Account__c':component.get("v.financialAccount"),
                'Previous_Hold__c':component.get("v.holdCode"),
                'Existing_Model__c':component.get("v.model"),
                'Quarterly_Rebalance__c':component.get("v.quarterlyRebal")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_New_Manual_Trade : function (component, event, helper){
        var recordTypeDeveloperName = 'Manual_Trade';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Trade_Request__c',
            'defaultFieldValues': {
                'Account__c':component.get("v.household"),
                'Financial_Account__c':component.get("v.financialAccount"),
                'Previous_Hold__c':component.get("v.holdCode"),
                'Existing_Model__c':component.get("v.model")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    }
})