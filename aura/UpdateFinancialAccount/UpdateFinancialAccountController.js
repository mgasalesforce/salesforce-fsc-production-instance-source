({
    updateOrionAccount : function(component, event, helper){
        console.log("--INITIATE ACCOUNT UPDATE--");
        component.set("v.pleaseWait", true);
        console.log("pleaseWait? "+component.get('v.pleaseWait'));
        console.log("OrionFinAccId: "+component.get('v.OrionFinancialAccountId'));
        
        var orionFinAccUpdate = component.get('c.updateAccountVerbose');
        orionFinAccUpdate.setParams({
            "orionAccountId" : component.get('v.OrionFinancialAccountId')
        });
        
        orionFinAccUpdate.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            } else if(state == 'INCOMPLETE'){
                console.log("State... Incomplete?");
            } else if(state == 'ERROR'){
                console.log("state... Errored?");
                
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown error -_-?");
                    }
                }
            }
        });
        
        $A.enqueueAction(orionFinAccUpdate);
    },
    
    isRefreshed: function(component, event, helper) {
        if(component.get('v.pleaseWait')){
            location.reload();
        }
    }
})