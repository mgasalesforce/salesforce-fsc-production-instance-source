({
    doInit : function(component, event, helper) {
        /*
        alert("hasEnvCode" + component.get('v.hasEnvCode'));
        alert("hasAdvisorCode" + component.get('v.hasAdvisorCode'));
        if(!component.get('v.hasEnvCode') && component.get('v.hasAdvisorCode')){
            var initDataValidation = component.get('c.validateData');
            $A.enqueueAction(initDataValidation);
        }
        */
    },
    validateData : function(component){
        //validate family member data
        var validateFamilyMemberData = component.get('c.validateClientInfo');
        validateFamilyMemberData.setParams({
            "householdId" : component.get('v.recordId')
        });
        
        validateFamilyMemberData.setCallback(this, function(response){
            var state = response.getState();
            if(state == "SUCCESS"){
                if(response.getReturnValue()){
                	component.set('v.householdReadyForClientCreation',response.getReturnValue());
                } else {
                    alert("Data validation failed: Please review client data and try again.");
                }
            } else if (state == "INCOMPLETE"){
                console.log("Incomplete - Try again later");
            } else if (state == "ERROR") {
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message: " + 
                                 errors[0].message);
                    }
                } else {
                    console.log("Unknown error");
                }
            }
        });
        $A.enqueueAction(validateFamilyMemberData);
    },
    createClientInEnvAction : function(component, event, helper){
        console.log("--INITIATE ENV CLIENT CREATION--");
        component.set("v.pleaseWait", true);
        
        var advisorCode = '';
        advisorCode = component.get('v.advisorCode');
        console.log("RecordID: "+component.get('v.recordId'));
        console.log("AdvCode: "+component.get('v.advisorCode'));
        var envAction = component.get('c.EnvCreateClient');
        envAction.setParams({
            "householdId" : component.get('v.recordId'),
            "advisorCode" : advisorCode
        });
        
        envAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            } else if(state == 'INCOMPLETE'){
                console.log("State... Incomplete?");
            } else if(state == 'ERROR'){
                console.log("state... Errored?");
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown error -_-?");
                    }
                }
            }
        });
        
        $A.enqueueAction(envAction);
    },
    
    updateClientInEnvAction : function(component, event, helper){
        console.log("--INITIATE CLIENT UPDATE--");
        component.set("v.pleaseWait_update", true);
        console.log("pleaseWait? "+component.get('v.pleaseWait_update'));
        
        var customerCode = '';
        customerCode = component.get('v.customerCode');
        console.log("RecordID: "+component.get('v.recordId'));
        console.log("AdvCode: "+component.get('v.customerCode'));
        console.log("test - isactive="+component.get('v.isActive'));
        if(customerCode == null) {
            customerCode = 'en0011709190';
            console.log("customerCode not found -- send to test client: "+customerCode);
        }
        var envAction = component.get('c.EnvUpdateClient');
        envAction.setParams({
            "householdId" : component.get('v.recordId'),
            "customerCode" : customerCode
        });
        console.log("household id: " + component.get('v.recordId'));
        
        envAction.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                $A.get('e.force:refreshView').fire();
            } else if(state == 'INCOMPLETE'){
                console.log("State... Incomplete?");
            } else if(state == 'ERROR'){
                console.log("state... Errored?");
                
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown error -_-?");
                    }
                }
            }
        });
        
        $A.enqueueAction(envAction);
    },
    
    isRefreshed: function(component, event, helper) {
        if(component.get('v.pleaseWait_update') || component.get('v.pleaseWait')){
        	location.reload();
        }
    }
})