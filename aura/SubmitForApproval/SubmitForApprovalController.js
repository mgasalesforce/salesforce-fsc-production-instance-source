({
    doInit : function(component, event, helper) {
        /*
        TODO: make this validation configurable from app builder page?
        */
        var changeType = event.getParams().changeType;
        if(changeType === "CHANGED"){
            component.find("recordLoader").reloadRecord();
        }

        var isAllFinAcc = component.get('v.targetRecord.All_Financial_Accounts__c');
        var numOfFinAcc = component.get('v.targetRecord.PMR_Financial_Accounts_Count__c');
        var clientStatus = component.get('v.targetRecord.Client_Status__c');
        console.log("isAllFinAcc: "+isAllFinAcc);
        console.log("numOfFinAcc: "+numOfFinAcc);
        console.log("clientStatus: "+clientStatus);
        var isInvalid = isAllFinAcc != 'Yes' && numOfFinAcc == '0' && clientStatus == 'Existing Client';
        console.log("isInvalid? "+ isInvalid);
        if(isInvalid){
            component.set('v.approvalValidated',false);
            component.set('v.validationMessage','Please specify Financial accounts in \'Price Modification Financial Accounts\' section.');
        } else {
            component.set('v.approvalValidated',true);
        }
    },
    
	submitForApprovalLC : function(component, event, helper) {
        component.set("v.pleaseWait", true);
		var recId = component.get('v.recordId');
		var comment = component.get('v.approvalComment');
        var submitForApprovalEvent = component.get('c.submitForApproval');
        submitForApprovalEvent.setParams({
            "recId" : component.get('v.recordId'),
            "comment" : comment
        });
        console.log("recId: "+recId);
        console.log("comment: "+comment);
        
        submitForApprovalEvent.setCallback(this, function(response){
            var state = response.getState();
            if(state == 'SUCCESS'){
                console.log('Success!');
                $A.get('e.force:refreshView').fire();
            } else if(state == 'INCOMPLETE'){
                console.log("State... Incomplete?");
            } else if(state == 'ERROR'){
                console.log("state... Errored?");
                var errors = response.getError();
                if(errors){
                    if(errors[0] && errors[0].message){
                        console.log("Error message: " + errors[0].message);
                    } else {
                        console.log("Unknown error -_-?");
                    }
                }
            }
        });
        
        $A.enqueueAction(submitForApprovalEvent);
	},
    
    isRefreshed: function(component, event, helper) {
        if(component.get('v.pleaseWait')){
        	location.reload();
        }
    }
})