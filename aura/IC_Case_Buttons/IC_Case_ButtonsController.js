({
    doInit : function(component, event, helper) {
        var action = component.get("c.GetRecordTypeMap");
        action.setParams({
            'sobjectName': 'Case'
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if(state === "SUCCESS"){
                component.set("v.recordTypeDeveloperMap", response.getReturnValue());
            }
            else{
                alert("Temporary Error: There is a problem getting the Record Types.");
            }
        });
        $A.enqueueAction(action); 

    },
    finacc_Account_Maint_Request : function(component, event, helper){
        var recordTypeDeveloperName = 'Account_Management';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        evt.fire();
    },
    finacc_TOA_Request : function(component, event, helper){
        var recordTypeDeveloperName = 'Transfer_Request';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_FI_Request : function(component, event, helper){
        var recordTypeDeveloperName = 'Fixed_Income_Request';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_Submit_IPS : function(component, event, helper){
        var recordTypeDeveloperName = 'Account_Management';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc"),
                'Reason_Details__c':'Attached IPS'
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_Move_Money_Request : function(component, event, helper){
        var recordTypeDeveloperName = 'Cash_Management';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc"),
                'Type':'Distribution'
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_Trade_Questions : function(component, event, helper){
        var recordTypeDeveloperName = 'Trade_Question';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_Team_Notes : function(component, event, helper){
        var recordTypeDeveloperName = 'Team_Note';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    finacc_Deposit_Tracking : function(component, event, helper){
        var recordTypeDeveloperName = 'Deposit_Tracking';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_finacc"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_finacc"),
                'Status':'Check Received',
                'Subject':'Deposit Tracking'
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    tradeReq_Account_Maint_Request : function (component, event, helper){
        var recordTypeDeveloperName = 'Account_Management';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_tradereq"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_tradereq"),
                'Trade_Request__c':component.get("v.tradeRequest_tradereq"),
                'Type':'Account Maintenance'
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    tradeReq_Submit_IPS : function (component, event, helper){
        var recordTypeDeveloperName = 'Account_Management';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_tradereq"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_tradereq"),
                'Trade_Request__c':component.get("v.tradeRequest_tradereq"),
                'Reason_Details__c':'Attached IPS'
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    tradeReq_Move_Money_Request : function (component, event, helper){
        var recordTypeDeveloperName = 'Cash_Management';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_tradereq"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_tradereq"),
                'Trade_Request__c':component.get("v.tradeRequest_tradereq"),
                'Type':'Distribution'
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    tradeReq_Team_Notes : function (component, event, helper){
        var recordTypeDeveloperName = 'Team_Note';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_tradereq"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_tradereq"),
                'Trade_Request__c':component.get("v.tradeRequest_tradereq")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    },
    tradeReq_Model_Change_Note : function (component, event, helper){
        var recordTypeDeveloperName = 'Team_Note';
        var evt = $A.get("e.force:createRecord");
        evt.setParams({
            'entityApiName':'Case',
            'defaultFieldValues': {
                'AccountId':component.get("v.household_tradereq"),
                'FinServ__FinancialAccount__c':component.get("v.financialAccount_tradereq"),
                'Trade_Request__c':component.get("v.tradeRequest_tradereq")
            },
            'recordTypeId':component.get("v.recordTypeDeveloperMap")[recordTypeDeveloperName]
        });
        
        evt.fire();
    }
})