({
    doInit : function(component, event, helper) {
        var action = component.get("c.createSubFolders");
        //var ids=new Array();
        //ids.push(component.get("v.recordId"));
        //var idListJSON=JSON.stringify(ids);
        
        action.setParams({"recordId": component.get("v.recordId")});
        //action.setParams({"RecordIds": idListJSON});
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(state === "SUCCESS") {
                var sObjectEvent = $A.get("e.force:navigateToSObject");
                sObjectEvent.setParams({
                    "recordId": response.getReturnValue(),
                    "slideDevName": "detail"
                });
                sObjectEvent.fire();
            }else if (state === "ERROR"){
                console.log('Problem creating folders for account, response state: ' + state);
                var errors = response.getError();
                if(errors) {
                    cmp.set("v.errorMsg", errors[0].message);
                    var errorMsg = cmp.find('errorMsg');
                    $A.util.removeClass(errorMsg, 'slds-hide');
                    var field = cmp.find('field');
                    $A.util.addClass(field, 'slds-hide');
                }
            }
        });
        $A.enqueueAction(action);
    },
    
})