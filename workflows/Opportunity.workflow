<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>FWS_Opportunity_AUM_Changed</fullName>
        <description>[Not In Use] FWS Opportunity AUM Changed</description>
        <protected>false</protected>
        <recipients>
            <recipient>Estate_Planning_Admin_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/FWS_Opp_AUM_Changed</template>
    </alerts>
</Workflow>
