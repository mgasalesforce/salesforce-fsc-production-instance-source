<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Case_Advisor_Change_Notify_Accounting</fullName>
        <description>Case - Advisor Change : Notify Accounting</description>
        <protected>false</protected>
        <recipients>
            <recipient>iwolf@merceradvisors.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_Advisor_Change_case_completed</template>
    </alerts>
    <alerts>
        <fullName>Case_Compliance_Notify_Creator_Marketing_Gen_Case</fullName>
        <description>Case - Compliance : Notify Creator (Marketing-Gen. Case)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_case_created</template>
    </alerts>
    <alerts>
        <fullName>Case_Investment_Strategy_PATS_Analysis</fullName>
        <description>Case - Investment Strategy : PATS Analysis</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_case_created_Investment_Strategy_PATS_analysis</template>
    </alerts>
    <alerts>
        <fullName>Case_KPI_Exception_Request_Approval_needed</fullName>
        <description>Case - KPI Exception Request : Approval needed</description>
        <protected>false</protected>
        <recipients>
            <recipient>KPI_Exception_Request_Approver</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_approval_request_KPI_Exception_Request</template>
    </alerts>
    <alerts>
        <fullName>Case_Marketing_Project_Contact_Info_Update</fullName>
        <description>Case - Marketing Project : Contact Info Update</description>
        <protected>false</protected>
        <recipients>
            <recipient>Marketing_Contact_Info_Update</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_case_created</template>
    </alerts>
    <alerts>
        <fullName>Case_Marketing_Project_Marketing_Kit_Order</fullName>
        <description>Case - Marketing Project : Marketing Kit Order</description>
        <protected>false</protected>
        <recipients>
            <recipient>Marketing_Marketing_Kit_Order</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_case_created</template>
    </alerts>
    <alerts>
        <fullName>Case_Marketing_Project_Notify_Creator</fullName>
        <description>Case - Marketing Project : Notify Creator</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_New_case_created_Marketing_Project</template>
    </alerts>
    <alerts>
        <fullName>Case_NLC_Notify_NLC_Survey</fullName>
        <description>Case - NLC : Notify NLC Survey</description>
        <protected>false</protected>
        <recipients>
            <recipient>NLC_Survey</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Case_NLC_NLC_Survey</template>
    </alerts>
    <alerts>
        <fullName>New_Case_not_Worked_On</fullName>
        <description>New Case not Worked On</description>
        <protected>false</protected>
        <recipients>
            <recipient>Investment</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/SUPPORTNewassignmentnotificationSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Case_Status_Cancelled</fullName>
        <field>Status</field>
        <literalValue>Cancelled</literalValue>
        <name>Case Status = Cancelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Completed</fullName>
        <field>Status</field>
        <literalValue>Completed</literalValue>
        <name>Case Status = Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_Mgr_Approved_Sent_to_Ops</fullName>
        <field>Status</field>
        <literalValue>Mgr Approved &amp; Sent to Ops</literalValue>
        <name>Case Status = Mgr Approved &amp; Sent to Ops</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_On_Hold</fullName>
        <field>Status</field>
        <literalValue>On Hold</literalValue>
        <name>Case Status = On Hold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_WIP</fullName>
        <field>Status</field>
        <literalValue>Work in progress</literalValue>
        <name>Case Status = WIP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Mgr_Reviewed_Sent_Back_to_CSU</fullName>
        <field>Status</field>
        <literalValue>Mgr Reviewed &amp; Sent Back to CSU</literalValue>
        <name>Status = Mgr Reviewed &amp; Sent Back to CSU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Ops_Reviewed_Sent_Back_to_CSU</fullName>
        <field>Status</field>
        <literalValue>Ops Reviewed &amp; Sent Back to CSU</literalValue>
        <name>Status = Ops Reviewed &amp; Sent Back to CSU</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
