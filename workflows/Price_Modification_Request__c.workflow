<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Process_Email_Alert_PMR_Billing_Exception_Request_Final_Decision</fullName>
        <description>[Approval Process Email Alert] PMR - Billing Exception Request - Final Decision</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PMR_Billing_Exception_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Final_Decision_Email</fullName>
        <description>Final Decision Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Price_Modification_Final_Decision</template>
    </alerts>
    <alerts>
        <fullName>PMR_Notify_Accounting</fullName>
        <description>PMR Notify Accounting</description>
        <protected>false</protected>
        <recipients>
            <recipient>iwolf@merceradvisors.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/PMR_Notify_Accounting</template>
    </alerts>
    <fieldUpdates>
        <fullName>Finance_Approval_Status_Approved</fullName>
        <field>Finance_Approval_Status__c</field>
        <literalValue>Finance Approved</literalValue>
        <name>Finance Approval Status: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Approval_Status_Not_Required</fullName>
        <field>Finance_Approval_Status__c</field>
        <literalValue>Not Required</literalValue>
        <name>Finance Approval Status: Not Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Approval_Status_Rejected</fullName>
        <field>Finance_Approval_Status__c</field>
        <literalValue>Finance Rejected</literalValue>
        <name>Finance Approval Status: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Approval_Status_Required</fullName>
        <field>Finance_Approval_Status__c</field>
        <literalValue>Required</literalValue>
        <name>Finance Approval Status: Required</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Approval_Status_in_Review</fullName>
        <field>Finance_Approval_Status__c</field>
        <literalValue>In Review</literalValue>
        <name>Finance Approval Status: in Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Finance_Approval_Status_none</fullName>
        <field>Finance_Approval_Status__c</field>
        <name>Finance Approval Status: none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Approval_Rejected</fullName>
        <field>Sales_Approval_Status__c</field>
        <literalValue>Sales Rejected</literalValue>
        <name>Sales Approval Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Approval_Status_Approved</fullName>
        <field>Sales_Approval_Status__c</field>
        <literalValue>Sales Approved</literalValue>
        <name>Sales Approval Status: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Approval_Status_In_Review</fullName>
        <field>Sales_Approval_Status__c</field>
        <literalValue>In Review</literalValue>
        <name>Sales Approval Status: In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Approval_Status_Rejected</fullName>
        <field>Sales_Approval_Status__c</field>
        <literalValue>Sales Rejected</literalValue>
        <name>Sales Approval Status: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Approval_Status_none</fullName>
        <field>Sales_Approval_Status__c</field>
        <name>Sales Approval Status: none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Sales Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Approval_Status_Approved</fullName>
        <field>Service_Approval__c</field>
        <literalValue>Service Approved</literalValue>
        <name>Service Approval Status: Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Approval_Status_In_Review</fullName>
        <field>Service_Approval__c</field>
        <literalValue>In Review</literalValue>
        <name>Service Approval Status: In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Approval_Status_Rejected</fullName>
        <field>Service_Approval__c</field>
        <literalValue>Service Rejected</literalValue>
        <name>Service Approval Status: Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Approval_Status_none</fullName>
        <field>Service_Approval__c</field>
        <name>Service Approval Status: none</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Status Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Not_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Not Submitted</literalValue>
        <name>Status Not Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Status Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_Update_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Status Update Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Status_to_Submitted</fullName>
        <field>Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Status to Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_Status_Not_Submitted</fullName>
        <field>Submitted_Status__c</field>
        <literalValue>Not Submitted</literalValue>
        <name>Submitted Status: Not Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_Status_Submitted</fullName>
        <field>Submitted_Status__c</field>
        <literalValue>Submitted</literalValue>
        <name>Submitted Status: Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
