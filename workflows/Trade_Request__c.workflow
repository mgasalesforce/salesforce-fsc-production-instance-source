<?xml version="1.0" encoding="utf-8"?><Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Final_Decision_Email</fullName>
        <description>Final Decision Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Trade_Request_Final_Decision</template>
    </alerts>
    <fieldUpdates>
        <fullName>Trade_Request_Advisor_Approved</fullName>
        <field>Status__c</field>
        <literalValue>Advisor Approved</literalValue>
        <name>Trade Request Advisor Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trade_Request_Advisor_Rejected</fullName>
        <field>Status__c</field>
        <literalValue>Advisor Rejected</literalValue>
        <name>Trade Request Advisor Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Trade_Request_Pending_Advisor_Approval</fullName>
        <field>Status__c</field>
        <literalValue>Pending Advisor Approval</literalValue>
        <name>Trade Request Pending Advisor Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
